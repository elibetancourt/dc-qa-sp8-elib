/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-02-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   10-29-2020   Elizabeth Betancourt Herrera   Initial Version
**/
public without sharing class dcPublicGroupUtils {
    
    /* devuelve public group id  de agenia mayorista*/
    public static String getGroupAgencyMayorista(){
        List<Account> accountList = [SELECT Id, PublicGroupId__c FROM account WHERE PublicGroupId__c != '' AND PublicGroupId__c != null AND Type='Mayorista' ] ;
        if(accountList.size()>0) return accountList[0].PublicGroupId__c;
        return '';
    }

    /* devuelve un mapa de agencia y public group id  */
    public static Map<String, String> getAgencyGroupMap(String agencyId){
        Map<String,String> agencyGropuIdMap = new Map<String, String>();

        if(!(Schema.sObjectType.account.isAccessible() &&
                Schema.sObjectType.account.fields.Id.isAccessible() &&
                Schema.sObjectType.account.fields.PublicGroupId__c.isAccessible() )) {
                DCException.throwPermiException('dcEmployeeTriggerHandler.getAgencyGroupMap');
            }
        List<account> accountList;
        
        if(String.isEmpty(agencyId)){
             accountList = [SELECT Id, PublicGroupId__c FROM account WHERE PublicGroupId__c != '' AND PublicGroupId__c != null] ;
         } else {
            accountList = [SELECT Id, PublicGroupId__c FROM account WHERE PublicGroupId__c != '' AND PublicGroupId__c != null AND Id=:agencyId ] ;
         } 
         
         for(account acc: accountList){
            agencyGropuIdMap.put(acc.Id, acc.PublicGroupId__c);
        }
        return agencyGropuIdMap;
    }

    /* devuelve un mapa de Agency -> User */
    public static Map<String, List<String>> getUserGroupMap(List<Id> userIdList){
        Map<String,List<String>> userGroupIdMap = new Map<String, List<String>>();

        if(!(Schema.sObjectType.GroupMember.isAccessible() &&
                Schema.sObjectType.GroupMember.fields.Id.isAccessible() &&
                Schema.sObjectType.GroupMember.fields.UserOrGroupId.isAccessible() &&
                Schema.sObjectType.GroupMember.fields.GroupId.isAccessible())) {
                DCException.throwPermiException('dcEmployeeTriggerHandler.getAgencyGroupMap');
            }

        List<String> groupList;
        List<GroupMember> groupMemberList = [SELECT Id, UserOrGroupId, GroupId  FROM GroupMember WHERE UserOrGroupId IN :userIdList ORDER BY UserOrGroupId] ;
        for (String userId: userIdList){
                userGroupIdMap.put(userId, new List<String>());
        }
       
        if (groupMemberList.size()>0){
            String userId = groupMemberList[0].UserOrGroupId;
            groupList = new List<String>();

            for(integer i=0; i <groupMemberList.size() ;i++){
                if (userId==groupMemberList[i].UserOrGroupId){
                    groupList.add(groupMemberList[i].GroupId);
                }
                else{
                    userGroupIdMap.put(userId, groupList);
                    groupList = new List<String>();
                }              
            }
            if(!userGroupIdMap.containsKey(userId)){
                userGroupIdMap.put(userId, groupList);
            }

        }

        return userGroupIdMap;
    }

}