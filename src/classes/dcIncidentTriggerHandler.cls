/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-02-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   11-02-2020   Elizabeth Betancourt Herrera   Initial Version
**/
public without sharing class dcIncidentTriggerHandler {
        Boolean isBefore;
        Boolean isAfter;
        Boolean isInsert;
        Boolean isDelete;
        Boolean isUpdate;
        List<Incident__c> newList {get; set;}
        List<Incident__c> oldList {get; set;}
        Map<Id, Incident__c> oldMap {get; set;}
    
        public dcIncidentTriggerHandler() {
            newList = trigger.new;
            oldList = trigger.old;
            oldMap = (Map<Id, Incident__c>)trigger.oldMap;
            this.isBefore = trigger.isBefore;
            this.isAfter = trigger.isAfter;
            this.isInsert = trigger.isInsert;
            this.isUpdate = trigger.isUpdate;
            this.isDelete = trigger.isDelete;
        }
        
        public void IncidentTriggerActions (){
        
            if(isAfter && isInsert) {
                //share account whith user in public group
                shareRecord();
            }                           
        }
    
        public void shareRecord (){
            
           Map<String, List<sObject>> groupIncidentList = getGroupListToShare();
           System.debug('Lista Incident a compartir') ;
           for(String groupId: groupIncidentList.keySet()){
               System.debug('groupId --> '+groupId) ;
               System.debug('total a compratir --> '+groupIncidentList.get(groupId).size()) ;
               dcSharingManager.shareRecords(groupIncidentList.get(groupId), 
                                             'Incident__Share', 'Edit', groupId, 'Manual');
           }
        }
    
        public Map<String, List<sObject>> getGroupListToShare (){
            Map<String, String> agencyGroupMap;
            System.debug('list size --> '+newList.size()) ;
            if(newList.size() == 1){
                agencyGroupMap = dcPublicGroupUtils.getAgencyGroupMap(newList[0].Agency__c);
            } else {
                agencyGroupMap = dcPublicGroupUtils.getAgencyGroupMap('');
            }
    
            Map<String, List<sObject>> groupIncidentList = new Map<String, List<sObject>>();
            
            for( Incident__c newIncident: newList){
                List<sObject> IncidentToShareList = new List<sObject>{newIncident};
                if(agencyGroupMap != null && agencyGroupMap.containsKey(newIncident.Agency__c)){
                    String groupId = agencyGroupMap.get(newIncident.Agency__c);
                    groupIncidentList.put(groupId, IncidentToShareList);
                }   
            }
    
            return groupIncidentList;
        }

}