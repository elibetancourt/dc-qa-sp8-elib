/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-03-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   10-19-2020   Elizabeth Betancourt Herrera   Initial Version
**/
public with sharing class dcSearchProcedureResult {
    
    @AuraEnabled
	public List<dcContactProcedureSearch> procedureResults {get; set;}
	@AuraEnabled
    public Boolean emptyList {get; set;}
	@AuraEnabled
	public Integer count {get; set;}
	@AuraEnabled
	public Boolean noResult {get; set;}
	@AuraEnabled
	public Boolean errorShow {get; set;}
	@AuraEnabled
	public String errorTitle {get; set;}
	@AuraEnabled
	public String errorMessage {get; set;}
	@AuraEnabled
    public dcSearchProcedureToBatch searchParams; 
    
    public dcSearchProcedureResult(dcSearchProcedureToBatch searchProcedure) {
        procedureResults = new List<dcContactProcedureSearch>();
		errorShow = false;
		emptyList = true;
		noResult = true;
		errorTitle = '';
		errorMessage = ''; 
		searchParams = searchProcedure;
    }

    public dcSearchProcedureResult(List<dcContactProcedureSearch> searchListResult,dcSearchProcedureToBatch searchProcedure) {
		procedureResults = searchListResult;
		errorShow = false;
		emptyList = procedureResults.size() == 0;
		noResult = procedureResults.size() == 0;
		errorTitle = '';
		errorMessage = ''; 
		searchParams = searchProcedure;
	}	
}