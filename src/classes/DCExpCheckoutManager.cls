/**
 * @description       : Class for Checkout Management in Express Checkout Process
 * @author            : Silvia Velazquez
 * @group             : 
 * @last modified on  : 10-06-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author             Modification
 * 1.0   10-06-2020   Silvia Velazquez   Initial Version
**/
public with sharing class DCExpCheckoutManager {
/**
    * @description Apply payment to Procedure
    * @author Silvia Velazquez | 10-06-2020 
    * @param Procedure__c p 
    * @return Boolean 
    **/
    public Boolean applyPayment(Procedure__c p){
        Decimal amount = (p.Agency_Commission__c != null) ? p.Agency_Commission__c : 0;
        List<SObject> records = new List<SObject>();
        
        if(amount > 0){
            Transaction__c creditTransaction = new Transaction__c(Agency__c = p.Agency__c,
                                                                GL_Account__c = DCExpCheckoutConstants.OPERATIONS_ACCOUNT,
                                                                Description__c = 'Transacción automática para la aplicación de un pago a traves de Express Checkout.',
                                                                Date__c = dateTime.now(),
                                                                Monto__c = amount,
                                                                Procedure__c = p.Id);
            records.add(creditTransaction);
        }
        
        Transaction__c creditEscrowTransaction = new Transaction__c(Agency__c = p.Agency__c,
                                                                    GL_Account__c = DCExpCheckoutConstants.ESCROW_ACCOUNT,
                                                                    Description__c = 'Transacción automática para la aplicación de un pago a traves de Express Checkout.',
                                                                    Date__c = dateTime.now(),
                                                                    Monto__c = p.Consulate_Payment__c,
                                                                    Procedure__c = p.Id);
        records.add(creditEscrowTransaction);

        p.Agency_Payment_Date__c = Date.today();
        records.add(p);

        records.sort(); //Sorting by Sobject Type to prevent Error: "System.TypeException: Cannot have more than 10 chunks in a single operation."
        try {
            //upsert records;
            CRUDEnforce.dmlUpsert(records, 'DCExpressCheckoutManager.applyPayment');
            return true;
        } catch (DmlException e) {
            return false;
        }
    }

    /**
    * @description Get Procedure Sales Price
    * @author Silvia Velazquez | 10-06-2020 
    * @param Procedure__c p 
    * @return Decimal 
    **/
    public Decimal getSalesPrice(Procedure__c p){
        return p.Total__c;
    }
}