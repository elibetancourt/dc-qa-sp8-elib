/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 10-30-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author             Modification
 * 1.0   10-15-2020   Silvia Velazquez   Initial Version
**/
global without sharing class DCWizardConfirmationController {

    @RemoteAction
    global static ProcedureResultWrapper updateProcedureAndGoToStripe(Procedure__c procedure, 
                                        DCWizardInformationController.ValuesToConvertWrapper values){                                           
        Procedure__c p = DCWizardInformationController.generateProcedure(procedure,values);
        if(p != null){
            ProcedureResultWrapper result = new ProcedureResultWrapper();
            result.procedure = p;
            result.stripeSettings = DCWizardPaymentManager.getProcedureStripeSettings(p);
            return result;
        }

        return null;
    }

    global class ProcedureResultWrapper{
        public Procedure__c procedure;
        public DCWizardPaymentManager.StripeSettingsWrapper stripeSettings;
    }
}