/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-03-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   10-26-2020   Elizabeth Betancourt Herrera   Initial Version
**/
@isTest
public with sharing class dcTrackingTriggerHandlerTest {
    public dcTrackingTriggerHandlerTest() {

    }

    @TestSetup
    static void makeData(){
        
        Account acc = new Account(Active__c = 'Yes',Name = 'District Cuba',Phone='2023211169',Type='Mayorista',Sigla__c = 'DCW');
        insert acc;
                
        Contact c = new Contact(FirstName='Luis',LastName='Suarez',AccountId=acc.Id,Eyes_Color__c='Negros');
        insert c;
        
        Date d = date.today();
        //two records for each RecordType, one complete and one incomplete
        
        //pasaporte 1ra vez incompleto
        Id pas1raVezRT =  DCUtils.getRecTypeIdByDevName('Procedure__c', 'Pasaporte_1ra_Vez');
        Procedure__c p = new Procedure__c(Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId=pas1raVezRT,
                                          First_Name__c='Luis', Last_Name__c='Suarez', Street__c='calle 10 entre 1ra y 3ra',
                                          Eyes_Color__c='Negros', Consulate_Payment__c = 375, Total__c=500, Subtotal__c=500);
        insert p;
       

    }

    @isTest
    static void updateProcedureEnvioTest(){

        Procedure__c proc = [SELECT Id, Delivery_Fee__c, Total__c FROM Procedure__c LIMIT 1]; 
        
        
        Tracking__c tracking1 = new Tracking__c(Type__c='Tramite completado',
                                                Delivery_Service__c='FedEx 2Day',
                                                Rate__c=40,
                                                Procedure__c=proc.Id);
        Tracking__c tracking2 = new Tracking__c(Type__c='Documentación (Enviar Cliente)',
                                                Delivery_Service__c='FedEx 2Day',
                                                Rate__c=45,
                                                Procedure__c=proc.Id);
        Tracking__c tracking3 = new Tracking__c(Type__c='Documentación (Recibir Cliente)',
                                                Delivery_Service__c='FedEx 2Day',
                                                Rate__c=50,
                                                Procedure__c=proc.Id);
        
        Test.startTest();
        // valor incial antes de insertar transaccion
        System.assert( proc.Delivery_Fee__c == null ||
                       proc.Delivery_Fee__c == 0, 
                       'Delivery_Fee__c valor inicial inesperado');
        
        insert tracking1;
        proc = [SELECT Id, Total__c, Delivery_Fee__c FROM Procedure__c LIMIT 1]; 
        System.assert( proc.Delivery_Fee__c == null ||
                       proc.Delivery_Fee__c == 0, 
                       'El total del envio no varia con los tracking de tipo completado.');
                       
        insert tracking2;
        proc = [SELECT Id, Delivery_Fee__c, Total__c FROM Procedure__c LIMIT 1]; 
        System.debug('proc-->'+proc);
        System.assertEquals(45, proc.Delivery_Fee__c, 'El monto de envio del procedimiento no es correcto despues de insertar el segundo tracking.');
        
        insert tracking3;
        proc = [SELECT Id, Delivery_Fee__c, Total__c FROM Procedure__c LIMIT 1]; 
        System.debug('proc-->'+proc);
        System.assertEquals(95, proc.Delivery_Fee__c, 'El monto de envio del procedimiento no es correcto despues de insertar el tercer tracking.');
        
        /*verificar sharing manual */
        String gId = dcPublicGroupUtils.getGroupAgencyMayorista();
        List<Tracking__Share> sharedRecords = [SELECT Id, ParentId, UserOrGroupId 
                                               FROM Tracking__Share 
                                               WHERE  UserOrGroupId=:gId AND 
                                               (parentId=:tracking1.Id OR 
                                                parentId=:tracking2.Id OR
                                                parentId=:tracking3.Id)];
        System.debug(sharedRecords.size());
        System.assert(sharedRecords.size()>=3, 'los tracking no fueron compartidos con el grupo de la agencia mayorista.');

        delete tracking2;
        proc = [SELECT Id, Delivery_Fee__c, Total__c FROM Procedure__c LIMIT 1]; 
        System.debug('proc-->'+proc);
        System.assertEquals(50, proc.Delivery_Fee__c, 'El monto de envio del procedimiento no es correcto despues de eliminar el segundo tracking.');

        tracking3.Rate__c=55;
        update tracking3;
        proc = [SELECT Id, Delivery_Fee__c, Total__c FROM Procedure__c LIMIT 1]; 
        System.debug('proc-->'+proc);
        System.assertEquals(55, proc.Delivery_Fee__c, 'El monto de envio del procedimiento no es correcto despues de actualizar el tracking.');

        Test.stopTest();
    }
}