/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-03-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   09-18-2020   Elizabeth Betancourt Herrera   Initial Version
**/
global without sharing class dcProcedureTriggerHandler {
        
    Boolean isBefore;
    Boolean isAfter;
    Boolean isInsert;
    Boolean isDelete;
    Boolean isUpdate;
    List<Procedure__c> newList {get; set;}
    List<Procedure__c> oldList {get; set;}
    Map<Id, Procedure__c> oldMap {get; set;}

    public dcProcedureTriggerHandler() {
        newList = trigger.new;
        oldList = trigger.old;
        oldMap = (Map<Id, Procedure__c>)trigger.oldMap;
        this.isBefore = trigger.isBefore;
        this.isAfter = trigger.isAfter;
        this.isInsert = trigger.isInsert;
        this.isUpdate = trigger.isUpdate;
        this.isDelete = trigger.isDelete;
    }

    public void updateRollUpSummaries(){
        // update trigger
        if(newList!= null && oldMap != null) {
            //System.debug('update trigger');
            //System.debug(newList);
            updateManifestRollUpSummaries();
            updateContactExtensionLastDate();
            updateContactActiveProcedureDate();
        }
        else {
            if(newList == null && oldMap != null) {  
                //System.debug('delete trigger');
                //System.debug(newList);
                updateManifestRollUpSummaries( );
                updateContactActiveProcedureDate( );
           } 
           else {
                if (newList != null && oldMap == null) { 
                     //System.debug('insert trigger');
                     //System.debug(newList);   
                     updateContactActiveProcedureDate();      
        		}
            }
        }
    }

    public void updateContactActiveProcedureDate(){
        //System.debug('entro a update active procedure');
        //System.debug(newList);
        Map<Id, Datetime> contactToUpdate = getContactToUpdate();
        //System.debug('contact to update');
        //System.debug(contactToUpdate);
        updateActiveProcedure(contactToUpdate);  
        //System.debug('fin active procedure');
    }

    public void updateContactExtensionLastDate(){
        //System.debug('entro a update extension last date');
        //System.debug(newList);
        Map<Id, Datetime> contactToUpdate = getExtensionContactToUpdate();
        //System.debug('extension to update');
        //System.debug(contactToUpdate);
        updateExtensionDate(contactToUpdate);                                                
    }

    private Map<Id, Datetime> getContactToUpdate(){
        Map<Id, Datetime> contactToUpdate = new Map<Id,Datetime>();

        Id primeraRT = DCUtils.getRecTypeIdByDevName('Procedure__c', 'Pasaporte_1ra_Vez');
        Id prorrogaRT = DCUtils.getRecTypeIdByDevName('Procedure__c', 'Prorroga');
        Id renovRT = DCUtils.getRecTypeIdByDevName('Procedure__c', 'Renovacion_de_Pasaporte');
        
        if (newList != null) {
            for(Procedure__c procedure: newList){  
                if( (procedure.Origin__c == 'Interno') &&
                    (procedure.RecordTypeId == prorrogaRT ||
                     procedure.RecordTypeId == renovRT ||
                     procedure.RecordTypeId == primeraRT ) &&
                    (procedure.Status__c != 'Cancelado' || 
                     procedure.Status__c != 'Entregado') &&
                    procedure.CreatedDate >= Datetime.now().addDays(-90) &&
                    ( oldMap == null ||
                     procedure.Status__c != oldMap.get(procedure.Id).Status__c) &&
                    !contactToUpdate.containsKey(procedure.Customer__c) ){
                        contactToUpdate.put(procedure.Customer__c, procedure.CreatedDate);
                }
            }
        }
        else {
            for(Procedure__c procedure: oldMap.values()){  
                if( (procedure.Origin__c == 'Interno') &&
                    (procedure.RecordTypeId == prorrogaRT ||
                     procedure.RecordTypeId == renovRT ||
                     procedure.RecordTypeId == primeraRT ) &&
                    (procedure.Status__c != 'Cancelado' || 
                     procedure.Status__c != 'Entregado') &&
                     procedure.CreatedDate >= Datetime.now().addDays(-90) &&
                    !contactToUpdate.containsKey(procedure.Customer__c) ){
                        contactToUpdate.put(procedure.Customer__c, null);
                }
            }
        }
            
        return contactToUpdate;
    }

    private Map<Id, Datetime> getExtensionContactToUpdate (){
        
        Map<Id, Datetime> extensionToUpdate = new Map<Id, Datetime>();

        Id prorrogaRT = DCUtils.getRecTypeIdByDevName('Procedure__c', 'Prorroga');
        for(Procedure__c procedure: newList){  
            if( procedure.RecordTypeId == prorrogaRT &&
                procedure.Status__c != oldMap.get(procedure.Id).Status__c &&
                (procedure.Status__c=='Completado' || 
                 procedure.Status__c=='Entregado' ||
                 procedure.Status__c=='Enviado') &&
                !extensionToUpdate.containsKey(procedure.Customer__c) ){
                extensionToUpdate.put(procedure.Customer__c, procedure.CreatedDate);
            }
        }
         
        return extensionToUpdate;

    }

    private void updateExtensionDate(Map<Id, Datetime> extensionToUpdate){
        if( extensionToUpdate.size() > 0){
            
            if(!(Schema.sObjectType.Contact.isAccessible() &&
                 Schema.sObjectType.Contact.fields.Id.isAccessible() &&
                 Schema.sObjectType.Contact.fields.ValidExtensionDate__c.isAccessible())) {
                DCException.throwPermiException('dcProcedureTriggerHandler.updateExtensionDate');
            }
            List<Contact> contactListToUpdate = [SELECT Id, ValidExtensionDate__c
                                                 FROM Contact 
                                                 WHERE Id in: extensionToUpdate.keySet() Order By Id];

            for(Integer i=0; i < contactListToUpdate.size(); i++){
                System.debug(contactListToUpdate[i].Id);
                contactListToUpdate[i].ValidExtensionDate__c = (Datetime)extensionToUpdate.get(contactListToUpdate[i].Id);
                System.debug(contactListToUpdate[i].ValidExtensionDate__c);
            }

            contactListToUpdate.sort(); //Sorting by Sobject Type to prevent Error: "System.TypeException: Cannot have more than 10 chunks in a single operation."
            try {
                CRUDEnforce.dmlUpdate(contactListToUpdate, 'dcProcedureTriggerHandler.updateExtensionDate');
            } catch (DmlException e) {
                DCException.throwException(e.getMessage());
            }
        }
    }

    private void updateActiveProcedure(Map<Id, Datetime> contactToUpdate){
        
        if( contactToUpdate.size() > 0){
            
            if(!(Schema.sObjectType.Contact.isAccessible() &&
                 Schema.sObjectType.Contact.fields.Id.isAccessible() &&
                 Schema.sObjectType.Contact.fields.ActiveProcedureDate__c.isAccessible())) {
                DCException.throwPermiException('dcProcedureTriggerHandler.updateActiveProcedure');
            }
            List<Contact> contactListToUpdate = [SELECT Id, ActiveProcedureDate__c
                                                 FROM Contact 
                                                 WHERE Id in: contactToUpdate.keySet() Order By Id];

            Map<Id, Integer> nullContactListToUpdate = new Map<Id, Integer>();

            for(Integer i=0; i < contactListToUpdate.size(); i++){
                System.debug(contactListToUpdate[i].Id);
                contactListToUpdate[i].ActiveProcedureDate__c = (Datetime)contactToUpdate.get(contactListToUpdate[i].Id);
                System.debug(contactListToUpdate[i].ActiveProcedureDate__c);
                if (contactToUpdate.get(contactListToUpdate[i].Id) == null){
                    nullContactListToUpdate.put(contactListToUpdate[i].Id, i);
                }
            }
            
            List<Id> contactIdList = new List<Id>(nullContactListToUpdate.keySet());
            
            if(!(Schema.sObjectType.Procedure__c.isAccessible() &&
                 Schema.sObjectType.Procedure__c.fields.Id.isAccessible() &&
                 Schema.sObjectType.Procedure__c.fields.CreatedDate.isAccessible() &&
                 Schema.sObjectType.Procedure__c.fields.Customer__c.isAccessible())) {
                DCException.throwPermiException('dcProcedureTriggerHandler.updateActiveProcedure');
            }
            List<Procedure__c> procedureList = [SELECT Customer__c, CreatedDate FROM Procedure__c
                                                WHERE Customer__c in: contactIdList
                                                ORDER BY Customer__c, CreatedDate DESC];
             
            if ( procedureList.size() >0 ){
                Integer j=0;
                for(Integer i=0; i < contactIdList.size() ;i++){
                    if( j < procedureList.size() && contactIdList[i] == procedureList[j].Customer__c){
                        System.debug(contactIdList[i]);
                        Integer index = nullContactListToUpdate.get(contactIdList[i]) ;
                        contactListToUpdate[index].ActiveProcedureDate__c = procedureList[j].CreatedDate;
                        // posicionar j en el proximo id diferente
                        While( j < procedureList.size() && contactIdList[i] == procedureList[j].Customer__c){
                            j++;
                        }
                    }  
                }
            }      
            System.debug('lista contactos para actualizar');
            System.debug(contactListToUpdate);
            contactListToUpdate.sort(); //Sorting by Sobject Type to prevent Error: "System.TypeException: Cannot have more than 10 chunks in a single operation."
            try {
                CRUDEnforce.dmlUpdate(contactListToUpdate, 'dcProcedureTriggerHandler.updateActiveProcedure');
            } catch (DmlException e) {
                DCException.throwException(e.getMessage());
            }
        }
    }
    
    public void updateManifestRollUpSummaries ( ){      
        Map<Id, Id> manifestIdToUpdate = new Map<Id,Id>();
        //System.debug('entro a update manifest');
        //System.debug(newList);
        if (newList == null)    {
            for(Procedure__c procedure: oldMap.values()){  
                if( procedure.Manifest__c != null &&
                   !manifestIdToUpdate.containsKey(procedure.Manifest__c) ){
                       manifestIdToUpdate.put(procedure.Manifest__c, procedure.Manifest__c);
                   }
            }
        } 
        else {
            for(Procedure__c procedure: newList){
              if( procedure.Manifest__c != null &&
                  !manifestIdToUpdate.containsKey(procedure.Manifest__c) &&
                   ( oldMap == null || 
                     procedure.Manifest__c != oldMap.get(procedure.Id).Manifest__c ||
                     procedure.ConsulateManifestPayment__c != oldMap.get(procedure.Id).ConsulateManifestPayment__c
                   )
                ){
                    manifestIdToUpdate.put(procedure.Manifest__c, procedure.Manifest__c);
                }
                else if (procedure.Manifest__c == null &&
                         oldMap != null &&
                         oldMap.get(procedure.Id).Manifest__c != null &&
                         !manifestIdToUpdate.containsKey(oldMap.get(procedure.Id).Manifest__c)){
                    manifestIdToUpdate.put(oldMap.get(procedure.Id).Manifest__c, oldMap.get(procedure.Id).Manifest__c);
                }
            }
        }                                        
        
        if( manifestIdToUpdate.size() > 0){
            
            if(!(Schema.sObjectType.Manifest__c.isAccessible() &&
             Schema.sObjectType.Manifest__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Manifest__c.fields.Procedures_Count__c.isAccessible() &&
             Schema.sObjectType.Manifest__c.fields.Amount_Due_to_Consulate__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Manifest__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.ConsulateManifestPayment__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Id.isAccessible())) {
                DCException.throwPermiException('dcProcedureTriggerHandler.updateManifestRollUpSummaries');
            }
            List<Manifest__c> manifestListToUpdate = [SELECT Id, Procedures_Count__c, Amount_Due_to_Consulate__c
                                                      FROM Manifest__c 
                                                      WHERE Id in: manifestIdToUpdate.values() Order By Id];

            AggregateResult[] rollUpSummaries= [SELECT Manifest__c Manifest__c, COUNT(Id) cantProc, 
                                                       SUM(ConsulateManifestPayment__c) totalProcedure 
                                                FROM Procedure__c  
                                                WHERE Manifest__c in: manifestIdToUpdate.values() 
                                                GROUP BY Manifest__c Order By Manifest__c];
            Integer j=0;
            for(Integer i=0; i < manifestListToUpdate.size(); i++){
                System.debug(manifestListToUpdate[i].Id);
                if (j < rollUpSummaries.size() && manifestListToUpdate[i].Id == (String)rollUpSummaries[j].get('Manifest__c') ){
                    System.debug(rollUpSummaries[j].get('Manifest__c'));
                    System.debug(rollUpSummaries[j].get('cantProc'));
                    System.debug(rollUpSummaries[j].get('totalProcedure'));
                    manifestListToUpdate[i].Procedures_Count__c = (decimal)rollUpSummaries[j].get('cantProc');
                    manifestListToUpdate[i].Amount_Due_to_Consulate__c = (decimal)rollUpSummaries[j].get('totalProcedure');
                    j++;
                }
                else{
                    manifestListToUpdate[i].Procedures_Count__c = 0;
                    manifestListToUpdate[i].Amount_Due_to_Consulate__c = 0;
                }
            }

            manifestListToUpdate.sort(); //Sorting by Sobject Type to prevent Error: "System.TypeException: Cannot have more than 10 chunks in a single operation."
            try {
                CRUDEnforce.dmlUpdate(manifestListToUpdate, 'dcProcedureTriggerHandler.updateManifestRollUpSummaries');
            } catch (DmlException e) {
                DCException.throwException(e.getMessage());
            }
            
        }
    }

}