/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 10-21-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   10-19-2020   Elizabeth Betancourt Herrera   Initial Version
**/
@isTest
public with sharing class dcAddProcedureToBatchControllerTest {
    
    @TestSetup
    static void makeData(){
        Map<String, String> param = new Map<String, String>();
        Account acc = new Account(Active__c = 'Yes',Name = 'District Cuba',Phone='2023211169',Type='Mayorista',Sigla__c = 'DCW');
        insert acc;
                
        Contact c = new Contact(FirstName='Luis',LastName='Suarez',AccountId=acc.Id,Eyes_Color__c='Negros');
        insert c;
        
        Date d = date.today();
        //two records for each RecordType, one complete and one incomplete
        List<Procedure__c> procList = new List<Procedure__c>();
        //pasaporte 1ra vez incompleto
        Id pas1raVezRT =  DCUtils.getRecTypeIdByDevName('Procedure__c', 'Pasaporte_1ra_Vez');
        Procedure__c p = new Procedure__c(Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId=pas1raVezRT,
                                          First_Name__c='Luis', Last_Name__c='Suarez', Street__c='calle 10 entre 1ra y 3ra',
                                          Eyes_Color__c='Negros', Status__c ='En Proceso');
        procList.add(p);
        //pasaporte 1ra vez completo
        p = new Procedure__c( Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId=pas1raVezRT,
                              First_Name__c='Luis', Last_Name__c='Suarez', Second_Last_Name__c='Valdés', Street__c='calle 10 entre 1ra y 3ra',
                              Residence_Country__c='US',State__c='FL', City__c='Miami', Postal_Code__c='33501', 
                              Work_Name__c='Costco Call Center', Phone__c='+15657676', Title__c='Recepcionista',
                              Departure_Date__c=d, Profession__c='Recepcionista', Birthday__c=d.addDays(-7600),
							  Immigration_Status__c='Salida Ilegal', ProfessionCategory__c='Otra',Nivel_de_Escolaridad__c='Primario',
					          Mother_Name__c='Estrella',Father_Name__c='Jose Luis',Gender__c ='Masculino',
                              Birth_Province__c='Artemisa',Birth_Municipality_City__c='Artemisa',
							  Eyes_Color__c='Negros',Hair_Color__c='Negro',Skin_Color__c='Negra',Height__c=172,
						      Reference_Full_Name__c='Ana Margarita',Reference_Second_Surname__c='Alfonso',
							  References_Province__c='Artemisa',References_Municipality__c='Artemisa',
                              Birth_Certificate_Issue_At__c='La Habana', Birth_Certificate_Issue_Date__c= d.addDays(-7500),
                              Reference_Address__c='Calle 50 entre Marti y Maceo',Reference_Last_Name__c='Hurtado',
                              Residence_Address_1__c='ave. Principal entre Marti y Maceo',Status__c ='Completado',
                              Residence_Address_1_Province__c='Artemisa',Residence_Address_1_City__c='Artemisa',
                              Residence_Year_1_From__c = '1990',Residence_Year_1_To__c = '1994');
        procList.add(p);
        //renovacion de pasaporte incompleto
        Id rpRT = DCUtils.getRecTypeIdByDevName('Procedure__c', 'Renovacion_de_Pasaporte');
        p = new Procedure__c(Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId=rpRT,Numero_de_Pasaporte__c='11111',
                                          First_Name__c='Luis', Last_Name__c='Suarez', Street__c='calle 10 entre 1ra y 3ra',
                                          Eyes_Color__c='Negros', Status__c ='En Proceso');
        procList.add(p);
        //renovacion de pasaporte completo
        p = new Procedure__c(Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId=rpRT,Numero_de_Pasaporte__c='11111',
                              First_Name__c='Luis', Last_Name__c='Suarez', Second_Last_Name__c='Valdés', Street__c='calle 10 entre 1ra y 3ra',
                              Residence_Country__c='US',State__c='FL', City__c='Miami', Postal_Code__c='33501',
                              Work_Name__c='Costco Call Center', Phone__c='+15657676', Title__c='Recepcionista',
                              Departure_Date__c=d, Profession__c='Recepcionista', Birthday__c=d.addDays(-7600),
							  Immigration_Status__c='Salida Ilegal', ProfessionCategory__c='Otra',Nivel_de_Escolaridad__c='Primario',
					          Mother_Name__c='Estrella',Father_Name__c='Jose Luis',Expired_Passport_Issue_Date__c=d,
                              Birth_Province__c='Artemisa',Birth_Municipality_City__c='Artemisa',Razon_No_Disponibilidad__c='Perdida',
							  Eyes_Color__c='Negros',Hair_Color__c='Negro',Skin_Color__c='Negra',Height__c=172,
						      Reference_Full_Name__c='Ana Margarita',Reference_Second_Surname__c='Alfonso',
							  References_Province__c='Artemisa',References_Municipality__c='Artemisa',Status__c ='Completado',
                              Birth_Certificate_Issue_At__c='La Habana', Birth_Certificate_Issue_Date__c= d.addDays(-7500),
                              Reference_Address__c='Calle 50 entre Marti y Maceo',Reference_Last_Name__c='Hurtado',
                              Residence_Address_1__c='ave. Principal entre Marti y Maceo',Gender__c ='Masculino',
                              Residence_Address_1_Province__c='Artemisa',Residence_Address_1_City__c='Artemisa',
                              Residence_Year_1_From__c = '1990',Residence_Year_1_To__c = '1994');
        procList.add(p);
        insert procList;
    }

    public static dcSearchProcedureToBatch initSearch(){
        String value = initSearchProcedure('','');
        dcSearchProcedureToBatch search = dcAddProcedureToBatchController.initData(value);
        return search;
    }
    
    public static String initSearchProcedure(String status, String agency){
            dcSearchProcedureToBatch search = new dcSearchProcedureToBatch();
            search.status = status;
            search.noBatch = true;
            search.noDistributor = false;
            search.agency = agency;
            search.filter = 'Luis';
            search.dateFrom = date.today().addDays(-1);
            search.dateTo = null;
            search.procedureTypes = new List<String>{'Pasaporte 1ra Vez', 'Renovacion de Pasaporte'};
            search.pageSize = 10;
            search.offset = 0;
            search.sortBy = 'Name, CreatedDate ';
            search.isDescending = true;
            
           return JSON.Serialize(search);
    }
    
    static testMethod void initDataTest() {
        Test.startTest();

        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        String search = initSearchProcedure('En Proceso', acc.Id);
        dcSearchProcedureToBatch searchProc = dcAddProcedureToBatchController.initData(search);
        //System.debug('status: '+searchProc.status);
        System.assertNotEquals(null, searchProc, '');
        System.assertEquals('En Proceso', searchProc.status, 'Los criterios no fueron inicializadas.');
        System.assertEquals(acc.Id, searchProc.agency, 'Los criterios no fueron inicializadas.');
       
        Test.stopTest();
    }
    
    static testMethod void counTest() {
        Test.startTest(); 

        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        String search = initSearchProcedure('En Proceso', acc.Id);
        dcSearchProcedureResult result = dcAddProcedureToBatchController.count(search);
        
        System.assertNotEquals(null, result, 'No se encontraron resultados.');
        System.assertEquals(2, result.count, 'Los resultados encontrados no son correctos.');
                
        Test.stopTest();
    }
     
    static testMethod void searchTest() {
        Test.startTest();

        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        String search = initSearchProcedure('En Proceso', acc.Id);
        dcSearchProcedureResult result = dcAddProcedureToBatchController.search(search);
        
        System.assertNotEquals(null, result.procedureResults, 'No se encontraron resultados.');
        System.assertEquals(2, result.procedureResults.size(), 'Los resultados encontrados no son correctos.');

        Test.stopTest();
    }
    
    static testMethod void resetTest() {
        Test.startTest();

        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        String search = initSearchProcedure('Completado', acc.Id);
        dcSearchProcedureResult result = dcAddProcedureToBatchController.reset(search);

        System.assertNotEquals(null, result.searchParams, '');
        System.assertEquals('Completado', result.searchParams.status, 'Los criterios no fueron inicializadas.');
        System.assertEquals(acc.Id, result.searchParams.agency, 'Los criterios no fueron inicializadas.');

        Test.stopTest();
    }
}