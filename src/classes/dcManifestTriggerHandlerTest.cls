/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-02-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   11-02-2020   Elizabeth Betancourt Herrera   Initial Version
**/
@isTest
public with sharing class dcManifestTriggerHandlerTest {
    /**
    * Testing all after insert trigger
    **/
    @isTest
    static void manifestCreateTest() {
        Test.startTest();
        
        Manifest__c m = new Manifest__c(Type__c='Renovacion de Pasaporte');
        insert m;

        List<Manifest__Share> sharedRecords = [SELECT Id, ParentId, UserOrGroupId FROM Manifest__Share WHERE parentId=:m.Id];
        System.debug(sharedRecords.size());
        System.assert(sharedRecords.size()>0, 'La tarifa de envio no fue compartida con el grupo de la agencia.');
   
        Test.stopTest();
    }
}