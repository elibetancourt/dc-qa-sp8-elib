/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-02-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   11-02-2020   Elizabeth Betancourt Herrera   Initial Version
**/
public without sharing class dcNumberXmlTriggerHandler {
        Boolean isBefore;
        Boolean isAfter;
        Boolean isInsert;
        Boolean isDelete;
        Boolean isUpdate;
        List<Number_for_XML_File__c> newList {get; set;}
        List<Number_for_XML_File__c> oldList {get; set;}
        Map<Id, Number_for_XML_File__c> oldMap {get; set;}

    public dcNumberXmlTriggerHandler() {
        newList = trigger.new;
        oldList = trigger.old;
        oldMap = (Map<Id, Number_for_XML_File__c>)trigger.oldMap;
        this.isBefore = trigger.isBefore;
        this.isAfter = trigger.isAfter;
        this.isInsert = trigger.isInsert;
        this.isUpdate = trigger.isUpdate;
        this.isDelete = trigger.isDelete;
    }

    public void NumberXmlTriggerActions (){
        
        if(isAfter && isInsert) {
            //share account whith user in public group
            shareRecord();
        }                           
    }

    public void shareRecord (){
        
       Map<String, List<sObject>> groupTrackingErrorList = getGroupListToShare();
       System.debug('Lista tarifa de envio a compartir') ;
       for(String groupId: groupTrackingErrorList.keySet()){
           System.debug('groupId --> '+groupId) ;
           System.debug('total a compratir --> '+groupTrackingErrorList.get(groupId).size()) ;
           dcSharingManager.shareRecords(groupTrackingErrorList.get(groupId), 
                                         'Number_for_XML_File__Share', 'Edit', groupId, 'Manual');
       }
    }

    public Map<String, List<sObject>> getGroupListToShare (){
        
        Map<String, List<sObject>> groupMap = new Map<String, List<sObject>>();
        
        String groupIdAgencyMayorista = dcPublicGroupUtils.getGroupAgencyMayorista();
        
        if(String.isNotEmpty(groupIdAgencyMayorista)) groupMap.put(groupIdAgencyMayorista, newList);

        return groupMap;
    }  


}