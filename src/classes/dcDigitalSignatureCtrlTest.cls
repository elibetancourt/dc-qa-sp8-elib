/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 09-25-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   09-25-2020   Elizabeth Betancourt Herrera   Initial Version
**/
@isTest
public with sharing class dcDigitalSignatureCtrlTest {
    public dcDigitalSignatureCtrlTest() {

    }

    @TestSetup
    static void makeData(){
        
        Account acc = new Account(Active__c = 'Yes',Name = 'District Cuba',Phone='2023211169',Type='Mayorista',Sigla__c = 'DCW');
        insert acc;
                
        Contact c = new Contact(FirstName='Luis',LastName='Suarez',AccountId=acc.Id,Eyes_Color__c='Negros');
        insert c;
        
        Date d = date.today();
        //two records for each RecordType, one complete and one incomplete
        List<Procedure__c> procList = new List<Procedure__c>();
        //pasaporte 1ra vez incompleto
        Id pas1raVezRT =  DCUtils.getRecTypeIdByDevName('Procedure__c', 'Pasaporte_1ra_Vez');
        Procedure__c p = new Procedure__c(Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId=pas1raVezRT,
                                          First_Name__c='Luis', Last_Name__c='Suarez', Street__c='calle 10 entre 1ra y 3ra',
                                          Eyes_Color__c='Negros');
        insert p;
    }

    @isTest
    static void DigitalSigaturePartialTest(){

        Procedure__c p = [SELECT Id FROM Procedure__c LIMIT 1];

        String urlPage = '/apex/dcDigitalSignature?recordId='+p.Id;
        urlPage = urlPage + '&obn=intmix__Procedure__c&afn=intmix__Signature_Attachment_Id__c&fln=Entrega&nurl=';
        urlPage = urlPage + '/apex/dcPassportDelivery?id='+p.Id;

        PageReference pageRef = new PageReference(urlPage);
        
        Test.startTest();
        
        Test.setCurrentPage(pageRef);
        
        dcDigitalSignatureCtrl dSign = new dcDigitalSignatureCtrl();
        dsign.generatePDF();

        Attachment attachPdf = [SELECT Name, parentId FROM Attachment WHERE Name like 'entrega%' LIMIT 1];

        System.assert(attachPdf.parentId==p.Id, 'Pdf file was not generated');

        Test.stopTest();

    }
}