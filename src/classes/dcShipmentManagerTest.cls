/**
 * @File Name          : dcShipmentManagerTest.cls
 * @Description        : Test Class for dcShipmentManager
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Elizabeth Betancourt Herrera
 * @Last Modified On   : 11-04-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    21/7/2020   Silvia Velazquez     Initial Version
**/
@isTest
private class dcShipmentManagerTest {

    @isTest
    static void generateShipmentFromProcedureOK(){
        // Call the method that invokes a callout
        Map<String,String> params = TestDataFactory.getTramiteParams(true,true,true); //true value is for getting valid params
        dcShipmentManager mgr = new dcShipmentManager();
        Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new dcFedexServiceCalloutMock('SUCCESS')); //generating a success response    
        String trackingId = mgr.generateShipment(true,params.get('tramiteId'),params.get('configurationId'),null, null, null,null,null,null,null,null,'');
        Test.stopTest();
        // Verify that a fake result is returned
        System.assertEquals(true,String.isNotBlank(trackingId), ''); 
    }

    @isTest
    static void generateShipmentFromProcedureInvalidId(){
       // Call the method that invokes a callout
       Map<String,String> params = TestDataFactory.getTramiteParams(false,true,true); //true value is for getting valid params
       dcShipmentManager mgr = new dcShipmentManager();
       Test.startTest();
       // This causes a fake response to be generated
       Test.setMock(WebServiceMock.class, new dcFedexServiceCalloutMock('ERROR')); //generating an error response    
       String trackingId = mgr.generateShipment(true,params.get('tramiteId'),params.get('configurationId'),null, null, null,null,null,null,null,null,'');
       Test.stopTest();
       // Verify that a fake result is returned
       System.assertEquals(null,trackingId, '');   
    }

    @isTest
    static void generateShipmentFromProcedureInvalidAddress(){
       // Call the method that invokes a callout
       Map<String,String> params = TestDataFactory.getTramiteParams(true,false,true); //true value is for getting valid params
       dcShipmentManager mgr = new dcShipmentManager();
       Test.startTest();
       // This causes a fake response to be generated
       Test.setMock(WebServiceMock.class, new dcFedexServiceCalloutMock('ERROR')); //generating an error response    
       String trackingId = mgr.generateShipment(true,params.get('tramiteId'),params.get('configurationId'),null, null, null,null,null,null,null,null,'');
       Test.stopTest();
       // Verify that a fake result is returned
       System.assertEquals(null,trackingId, '');   
    }

    @isTest
    static void generateShipmentFromProcedureInvalidConfig(){
       // Call the method that invokes a callout
       Map<String,String> params = TestDataFactory.getTramiteParams(true,true,false); //true value is for getting valid params
       dcShipmentManager mgr = new dcShipmentManager();
       Test.startTest();
       // This causes a fake response to be generated
       Test.setMock(WebServiceMock.class, new dcFedexServiceCalloutMock('ERROR')); //generating an error response    
       String trackingId = mgr.generateShipment(true,params.get('tramiteId'),params.get('configurationId'),null, null, null,null,null,null,null,null,'');
       Test.stopTest();
       // Verify that a fake result is returned
       System.assertEquals(null,trackingId, '');   
    }

    @isTest
    static void generateShipmentFromProcedureTipoRecibir(){
        // Call the method that invokes a callout
        Map<String,String> params = TestDataFactory.getTramiteParams(true,true,true); //true value is for getting valid params
        dcShipmentManager mgr = new dcShipmentManager();
        Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new dcFedexServiceCalloutMock('SUCCESS')); //generating a success response    
        String trackingId = mgr.generateShipment(true,params.get('tramiteId'),params.get('configurationId'),null, null, null,null,null,null,null,null,'Documentación (Recibir Cliente)'); 
        
        Test.stopTest();
        // Verify that a fake result is returned
        System.debug('trackingId --> '+trackingId);
        System.assert(String.isNotEmpty(trackingId), 'El tracking no fue generado.'); 
    }

    @isTest
    static void generateShipmentFromLoteOK(){
        // Call the method that invokes a callout
        String param = TestDataFactory.getLoteParam(true); //true value is for getting valid params
        dcShipmentManager mgr = new dcShipmentManager();
        Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new dcFedexServiceCalloutMock('SUCCESS')); //generating a success response    
        String trackingId = mgr.generateShipment(false,null,null,param, 1, 'FedEx Envelope','FedEx 2Day AM',1,5,5,5,'');
        Test.stopTest();
        // Verify that a fake result is returned
        System.assertEquals(true,String.isNotBlank(trackingId), ''); 
    }

    @isTest
    static void generateShipmentFromLoteInvalidPack(){
        // Call the method that invokes a callout
        String param = TestDataFactory.getLoteParam(true); //true value is for getting valid params
        system.debug('Params: '+param);
        dcShipmentManager mgr = new dcShipmentManager();
        Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new dcFedexServiceCalloutMock('ERROR')); //generating an error response    
        String trackingId = mgr.generateShipment(false,null,null,param, 1, 'FedEx-Envelop','FedEx 2Day AM',1,5,5,5,'');
        Test.stopTest();
        // Verify that a fake result is returned
        System.assertEquals(null,trackingId, ''); 
    }

}