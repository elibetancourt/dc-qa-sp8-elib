/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-04-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   09-18-2020   Elizabeth Betancourt Herrera   Initial Version
**/
global without sharing class dcTransactionTriggerHandler {
    
    Boolean isBefore;
    Boolean isAfter;
    Boolean isInsert;
    Boolean isDelete;
    Boolean isUpdate;
    List<Transaction__c> newList {get; set;}
    List<Transaction__c> oldList {get; set;}
    Map<Id, Transaction__c> oldMap {get; set;}
    
    public dcTransactionTriggerHandler() {
        newList = trigger.new;
        oldList = trigger.old;
        oldMap = (Map<Id, Transaction__c>)trigger.oldMap;
        this.isBefore = trigger.isBefore;
        this.isAfter = trigger.isAfter;
        this.isInsert = trigger.isInsert;
        this.isUpdate = trigger.isUpdate;
        this.isDelete = trigger.isDelete;    
    }

    public void updateTotalEscrowAmount (){
        Map<Id, Id> transactionIdToUpdate = getEscrowTransactionToUpdate();                                      
        
        updateProcedureRollUpSummary(transactionIdToUpdate);                                           
    }

    private Map<Id, Id> getEscrowTransactionToUpdate (){
        Map<Id, Id> transactionIdToUpdate = new Map<Id,Id>();

        if (newList == null)    {
            for(Transaction__c trans: oldMap.values()){  
                if( trans.GL_Account__c=='Escrow' &&
                    trans.Procedure__c != null &&
                    !transactionIdToUpdate.containsKey(trans.Procedure__c) ){
                        transactionIdToUpdate.put(trans.Procedure__c, trans.Procedure__c);
                    }
            }
        } 
        else {
            for(Transaction__c trans: newList){
                if( trans.GL_Account__c=='Escrow' &&
                    trans.Procedure__c != null &&
                    !transactionIdToUpdate.containsKey(trans.Procedure__c) &&
                    ( oldMap == null ||
                      trans.Monto__c != oldMap.get(trans.Id).Monto__c ||
                      trans.Procedure__c != oldMap.get(trans.Id).Procedure__c )
                ){
                    transactionIdToUpdate.put(trans.Procedure__c, trans.Procedure__c);
                }
                else if (trans.Procedure__c == null &&
                         oldMap != null && 
                         oldMap.get(trans.Id).GL_Account__c=='Escrow' &&
                         oldMap.get(trans.Id).Procedure__c != null &&
                         !transactionIdToUpdate.containsKey(oldMap.get(trans.Id).Procedure__c)){
                    transactionIdToUpdate.put(oldMap.get(trans.Id).Procedure__c, 
                                              oldMap.get(trans.Id).Procedure__c);
                }
            }
        }
        return transactionIdToUpdate;
    }
    private void updateProcedureRollUpSummary(Map<Id, Id> transactionIdToUpdate){
        
        if( transactionIdToUpdate.size() > 0){
            
            if(!(Schema.sObjectType.Transaction__c.isAccessible() &&
                Schema.sObjectType.Transaction__c.fields.Id.isAccessible() &&
                Schema.sObjectType.Transaction__c.fields.Monto__c.isAccessible() &&
                Schema.sObjectType.Transaction__c.fields.Procedure__c.isAccessible() &&
                Schema.sObjectType.Transaction__c.fields.GL_Account__c.isAccessible() &&
                Schema.sObjectType.Procedure__c.isAccessible() &&
                Schema.sObjectType.Procedure__c.fields.Total_Payments_To_Escrow_Account__c.isAccessible() &&
                Schema.sObjectType.Procedure__c.fields.Id.isAccessible())) {
                DCException.throwPermiException('dcTransactionTriggerHandler.updateProcedureRollUpSummary');
            }

            List<Procedure__c> prodListToUpdate = [SELECT Id, Total_Payments_To_Escrow_Account__c
                                                            FROM Procedure__c 
                                                            WHERE Id in: transactionIdToUpdate.values() Order By Id];

            AggregateResult[] rollUpSummaries= [SELECT Procedure__c, SUM(Monto__c) totalEscrowAmount
                                                FROM Transaction__c  
                                                WHERE GL_Account__c = 'Escrow' 
                                                AND Procedure__c in: transactionIdToUpdate.values() 
                                                GROUP BY Procedure__c Order By Procedure__c];
            Integer j=0;
            for(Integer i=0; i < prodListToUpdate.size(); i++){
                System.debug(prodListToUpdate[i].Id);
                if (j < rollUpSummaries.size() && prodListToUpdate[i].Id == (string)rollUpSummaries[j].get('Procedure__c')){
                   System.debug(rollUpSummaries[j].get('Procedure__c'));
                   System.debug(rollUpSummaries[j].get('totalEscrowAmount'));
                   prodListToUpdate[i].Total_Payments_To_Escrow_Account__c = (decimal)rollUpSummaries[j].get('totalEscrowAmount');
                   j++;
                }
                else{
                    prodListToUpdate[i].Total_Payments_To_Escrow_Account__c = 0;
                }
            }

            prodListToUpdate.sort(); //Sorting by Sobject Type to prevent Error: "System.TypeException: Cannot have more than 10 chunks in a single operation."
            update prodListToUpdate;
            /*try {
                CRUDEnforce.dmlUpdate(prodListToUpdate, 'dcTransactionTriggerHandler.updateProcedureRollUpSummary');
            } catch (DmlException e) {
                DCException.throwException(e.getMessage());
            } */
        }
    }

    public void TransactionTriggerActions (){
        
        if(isAfter && isInsert) {
            //share account whith user in public group
            shareRecord();
        }   
        updateTotalEscrowAmount();                        
    }

    public void shareRecord (){
        
       Map<String, List<sObject>> groupTransactionList = getGroupListToShare();
       System.debug('Lista tarifa de envio a compartir');
       for(String groupId: groupTransactionList.keySet()){
           System.debug('groupId --> '+groupId) ;
           System.debug('total a compratir --> '+groupTransactionList.get(groupId).size()) ;
           dcSharingManager.shareRecords(groupTransactionList.get(groupId), 
                                         'Transaction__Share', 'Edit', groupId, 'Manual');
       }
    }

    public Map<String, List<sObject>> getGroupListToShare (){
        
        Map<String, List<sObject>> groupMap = new Map<String, List<sObject>>();
        
        String groupIdAgencyMayorista = dcPublicGroupUtils.getGroupAgencyMayorista();
        
        if(String.isNotEmpty(groupIdAgencyMayorista)) groupMap.put(groupIdAgencyMayorista, newList);

        return groupMap;
    }  
}