/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 10-29-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   10-28-2020   Elizabeth Betancourt Herrera   Initial Version
**/
public without sharing class dcPublicGroupUserHandler {
    
    public class InputParam{
        public String publicGroupId;
        public String userId;
        public String operation;
    }

    @InvocableMethod(label='Update Employee Public Group' description='Give Employee corresponding agency public group.')
    public static void updateUserPublicGroup(List<String> accountList){
        
        Map<String, String> agencyGroupMap = dcPublicGroupUtils.getAgencyGroupMap(accountList[0]);
        if (agencyGroupMap.size()==0){
            DCException.throwException('Ha ocurrido un error. No se puede completar la solicitud puesto que la agencia no tiene un grupo asociado.');
        }
        String publicGroupId = agencyGroupMap.get(accountList[0]);
        System.debug('publicGroupId-->'+publicGroupId);
        /* buscar usuarios de la agencia*/
        List<Id> userIdList = getUserList(accountList[0]);
        Map<String, List<String>> userGroupIdMap = dcPublicGroupUtils.getUserGroupMap(userIdList);
        
        addUserPublicGroup(getUserGroupToUpdate(publicGroupId, userGroupIdMap));

    }

    public static List<Id> getUserList(String agencyId){
        if(!(Schema.sObjectType.User.isAccessible() &&
            Schema.sObjectType.User.fields.Id.isAccessible() &&
            Schema.sObjectType.User_Agency_RelationShip__c.isAccessible() &&
            Schema.sObjectType.User_Agency_RelationShip__c.fields.Agency__c.isAccessible() &&
            Schema.sObjectType.User_Agency_RelationShip__c.fields.User__c.isAccessible() &&
            Schema.sObjectType.User_Agency_RelationShip__c.fields.Id.isAccessible())) {
            DCException.throwPermiException('dcEmployeeTriggerHandler.updateUserRollUpSummary');
        }

        List<User_Agency_RelationShip__c> employeeList= [SELECT User__c 
                                                         FROM User_Agency_RelationShip__c  
                                                         WHERE Agency__c =: agencyId ];
        
        List<Id> userIdList = new List<Id>();
        for(User_Agency_RelationShip__c employee:employeeList ){
            userIdList.add(employee.User__c);
        }
        return userIdList;
    }

    public static void addUserPublicGroup(List<InputParam> params) {
        
        List<GroupMember>listGroupMemberToAdd=new List<GroupMember>(); 
        List<String>listUserToDelete=new List<String>(); 
        for(InputParam param:params){
             
            GroupMember gm= new GroupMember(GroupId=param.publicGroupId, 
                                            UserOrGroupId=param.userId); 
            
            if(param.operation == 'delete'){
                listUserToDelete.add(param.userId);
            } 
            else if (param.operation == 'add'){
                listGroupMemberToAdd.add(gm);
            }
        }
        //system.debug('to add-->'+listGroupMemberToAdd);
        //system.debug('to delete-->'+listUserToDelete);
        if (listUserToDelete.size() >0){

            if(!(Schema.sObjectType.GroupMember.isAccessible() &&
                Schema.sObjectType.GroupMember.fields.Id.isAccessible() &&
                Schema.sObjectType.GroupMember.fields.UserOrGroupId.isAccessible())) {
                DCException.throwPermiException('dcEmployeeTriggerHandler.updateUserRollUpSummary');
            }

            List<GroupMember>listGroupMemberToDelete=[SELECT Id FROM GroupMember WHERE UserOrGroupId IN:listUserToDelete ];
            if(listGroupMemberToDelete.size()>0){
                CRUDEnforce.dmlDelete(listGroupMemberToDelete, 'dcPublicGroupUserHandler.addUserToPublicGroup');
            }
            //system.debug('to delete-->'+listGroupMemberToDelete);
        }
        if (listGroupMemberToAdd.size()>0){
            CRUDEnforce.dmlInsert(listGroupMemberToAdd, 'dcPublicGroupUserHandler.addUserToPublicGroup');
        }
        //system.debug('to add-->'+listGroupMemberToAdd);
        
        /* try {
            insert listGroupMember;
        }
        catch (DmlException e){
            DCException.throwException('dcPublicGroupHandler.addUserToPublicGroup Error: '+e.getDmlMessage(0));
        }*/    
    }

    public static List<InputParam> getUserGroupToUpdate(String publicGroupId, Map<String, List<String>> userGroupIdMap){
        List<InputParam> userGroupToUpdate = new List<InputParam>();
        InputParam param;
        
        for (string userId: userGroupIdMap.keySet()){
           List<String> publicGroupIdList = userGroupIdMap.get(userId);
           boolean found = false;
           for(String groupId: publicGroupIdList){
                if(groupId == publicGroupId){
                    found = true;
                } else{
                    param = new InputParam();
                    param.operation='delete';
                    param.publicGroupId=groupId;
                    param.userId=userId;
                    userGroupToUpdate.add(param);
                }
            }
              /** add to list */
            if (!found){
                param = new InputParam();
                param.operation='add';
                param.publicGroupId=publicGroupId;
                param.userId=userId;
                userGroupToUpdate.add(param);
            }
        }
        
        return userGroupToUpdate;
    }
    
}