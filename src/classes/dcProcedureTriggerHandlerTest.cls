/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 09-21-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   09-18-2020   Elizabeth Betancourt Herrera   Initial Version
**/
@isTest
public with sharing class dcProcedureTriggerHandlerTest {
    
    private static List<Procedure__c> procRenovacListToTest;
    private static List<Procedure__c> procProrrogaListToTest;

    static void makeData(){
        //create data to insert
        Account acc = new Account(Active__c = 'Yes',Name = 'District Cuba',Phone='2023211169',Type='Mayorista',Sigla__c = 'DCW');
        insert acc;
             
        List<Contact> contactList = new List<Contact> ();
        Contact c = new Contact(FirstName='Luis',LastName='Suarez',AccountId=acc.Id,Eyes_Color__c='Negros');
        contactList.add(c);
        c = new Contact(FirstName='Doris',LastName='Suarez',AccountId=acc.Id,Eyes_Color__c='Negros');
        contactList.add(c);
        insert contactList;
        
        Date d = date.today();
        List<Procedure__c> procList = new List<Procedure__c>();
        //renovacion de pasaporte 
        Id rpRT = DCUtils.getRecTypeIdByDevName('Procedure__c', 'Renovacion_de_Pasaporte');
        Procedure__c p = new Procedure__c(Agency__c=acc.Id, Customer__c=contactList[0].Id, RecordTypeId=rpRT,Numero_de_Pasaporte__c='11111',
                                          First_Name__c='Luis', Last_Name__c='Suarez', Second_Last_Name__c='Valdés', Street__c='calle 10 entre 1ra y 3ra',
                                          Residence_Country__c='US',State__c='FL', City__c='Miami', Postal_Code__c='33501',
                                          Work_Name__c='Costco Call Center', Phone__c='+15657676', Title__c='Recepcionista',
                                          Departure_Date__c=d, Profession__c='Recepcionista', Birthday__c=d.addDays(-7600),
                                          Immigration_Status__c='Salida Ilegal', ProfessionCategory__c='Otra',Nivel_de_Escolaridad__c='Primario',
                                          Mother_Name__c='Estrella',Father_Name__c='Jose Luis',Expired_Passport_Issue_Date__c=d,
                                          Birth_Province__c='Artemisa',Birth_Municipality_City__c='Artemisa',Razon_No_Disponibilidad__c='Perdida',
                                          Eyes_Color__c='Negros',Hair_Color__c='Negro',Skin_Color__c='Negra',Height__c=172,
                                          Reference_Full_Name__c='Ana Margarita',Reference_Second_Surname__c='Alfonso',
                                          References_Province__c='Artemisa',References_Municipality__c='Artemisa',Origin__c='Interno',
                                          Birth_Certificate_Issue_At__c='La Habana', Birth_Certificate_Issue_Date__c= d.addDays(-7500),
                                          Reference_Address__c='Calle 50 entre Marti y Maceo',Reference_Last_Name__c='Hurtado',
                                          Residence_Address_1__c='ave. Principal entre Marti y Maceo',Gender__c ='Masculino',
                                          Residence_Address_1_Province__c='Artemisa',Residence_Address_1_City__c='Artemisa',
                                          Residence_Year_1_From__c = '1990',Residence_Year_1_To__c = '1994', ConsulateManifestPayment__c = 50);
        procList.add(p);
        //renovacion de pasaporte c
        p = new Procedure__c(Agency__c=acc.Id, Customer__c=contactList[1].Id, RecordTypeId=rpRT,Numero_de_Pasaporte__c='11112',
                              First_Name__c='Luis', Last_Name__c='Suarez', Second_Last_Name__c='Valdés', Street__c='calle 10 entre 1ra y 3ra',
                              Residence_Country__c='US',State__c='FL', City__c='Miami', Postal_Code__c='33501',
                              Work_Name__c='Costco Call Center', Phone__c='+15657676', Title__c='Recepcionista',
                              Departure_Date__c=d, Profession__c='Recepcionista', Birthday__c=d.addDays(-7600),
							  Immigration_Status__c='Salida Ilegal', ProfessionCategory__c='Otra',Nivel_de_Escolaridad__c='Primario',
					          Mother_Name__c='Estrella',Father_Name__c='Jose Luis',Expired_Passport_Issue_Date__c=d,
                              Birth_Province__c='Artemisa',Birth_Municipality_City__c='Artemisa',Razon_No_Disponibilidad__c='Perdida',
							  Eyes_Color__c='Negros',Hair_Color__c='Negro',Skin_Color__c='Negra',Height__c=172,
						      Reference_Full_Name__c='Ana Margarita',Reference_Second_Surname__c='Alfonso',
							  References_Province__c='Artemisa',References_Municipality__c='Artemisa',Origin__c='Interno',
                              Birth_Certificate_Issue_At__c='La Habana', Birth_Certificate_Issue_Date__c= d.addDays(-7500),
                              Reference_Address__c='Calle 50 entre Marti y Maceo',Reference_Last_Name__c='Hurtado',
                              Residence_Address_1__c='ave. Principal entre Marti y Maceo',Gender__c ='Masculino',
                              Residence_Address_1_Province__c='Artemisa',Residence_Address_1_City__c='Artemisa',
                              Residence_Year_1_From__c = '1990',Residence_Year_1_To__c = '1994', ConsulateManifestPayment__c = 100);
        procList.add(p);

        insert procList;

        Manifest__c m = new Manifest__c(Type__c='Renovacion de Pasaporte');
        insert m;
    }

    @isTest
    static void updateManifestRollUpSummariesTest(){
        
        makeData();
        Id rpRT = DCUtils.getRecTypeIdByDevName('Procedure__c', 'Renovacion_de_Pasaporte');

        Manifest__c manifest = [SELECT Id, Procedures_Count__c, Amount_Due_to_Consulate__c 
                    FROM Manifest__c WHERE Type__c='Renovacion de Pasaporte' Limit 1];

        List<Procedure__c> procedureList = [SELECT Id, Manifest__c, ConsulateManifestPayment__c FROM Procedure__c WHERE RecordTypeId =: rpRT];
        Integer cant = procedureList.size();
        Decimal totalPayment = 0;
        for (Integer i=0; i < cant; i++){
            totalPayment = totalPayment + procedureList[i].ConsulateManifestPayment__c;
            procedureList[i].Manifest__c = manifest.Id;
            procedureList[i].Status__c = 'Listo';
        }   

        Test.startTest();
        
        update procedureList;
        manifest = [SELECT Id, Procedures_Count__c, Amount_Due_to_Consulate__c 
                    FROM Manifest__c WHERE Type__c='Renovacion de Pasaporte' Limit 1];
        System.assertEquals(cant, manifest.Procedures_Count__c,'RollUpSummary procedure Count after insert NOT OK');
        System.assertEquals(totalPayment, manifest.Amount_Due_to_Consulate__c,'RollUpSummary Total Payment after insert NOT OK');
        
        procedureList[0].ConsulateManifestPayment__c = procedureList[0].ConsulateManifestPayment__c + 50;
        totalPayment = totalPayment + 50;
        update procedureList[0];
        manifest = [SELECT Id, Procedures_Count__c, Amount_Due_to_Consulate__c 
                    FROM Manifest__c WHERE Type__c='Renovacion de Pasaporte' Limit 1];
        System.assertEquals(cant, manifest.Procedures_Count__c,'RollUpSummary procedure Count after update NOT OK');
        System.assertEquals(totalPayment, manifest.Amount_Due_to_Consulate__c,'RollUpSummary Total Payment after update NOT OK');
        
        totalPayment = totalPayment - procedureList[0].ConsulateManifestPayment__c;
        delete procedureList[0];
        manifest = [SELECT Id, Procedures_Count__c, Amount_Due_to_Consulate__c 
                    FROM Manifest__c WHERE Type__c='Renovacion de Pasaporte' Limit 1];
        System.assertEquals(cant-1, manifest.Procedures_Count__c,'RollUpSummary procedure Count after delete NOT OK');
        System.assertEquals(totalPayment, manifest.Amount_Due_to_Consulate__c,'RollUpSummary Total Payment after delete NOT OK');
        
        procedureList[1].Manifest__c = null;
        update procedureList[1];     
        manifest = [SELECT Id, Procedures_Count__c, Amount_Due_to_Consulate__c 
                    FROM Manifest__c WHERE Type__c='Renovacion de Pasaporte' Limit 1];
        System.assertEquals(0, manifest.Procedures_Count__c,'RollUpSummary procedure count after clean manifest NOT OK');
        System.assertEquals(0, manifest.Amount_Due_to_Consulate__c,'RollUpSummary Total Payment after clean manifest NOT OK');
        
        Test.stopTest();
    }

    @isTest
    static void updateContactRollUpTest(){
        
        makeDataforContactRollUp();

        Test.startTest();
        
        //before inserting the field ActiveProcedureDate__c must be null
        List<Contact> contactList = [SELECT Id, ActiveProcedureDate__c FROM Contact ];
        for( Contact c: contactList){
            System.debug(c.ActiveProcedureDate__c);
            System.assert(c.ActiveProcedureDate__c == null, 'La fecha del procedimiento activo no fue inicializada');
        }
        //after inserting the field ActiveProcedureDate__c should have the creation date
        insert procRenovacListToTest;
                
        contactList = [SELECT Id, ActiveProcedureDate__c FROM Contact ];
        Id rpRT = DCUtils.getRecTypeIdByDevName('Procedure__c', 'Renovacion_de_Pasaporte');
        procRenovacListToTest = [SELECT Id, CreatedDate FROM Procedure__c WHERE recordTypeId =: rpRT];
        Datetime renovacFecha =  procRenovacListToTest[0].CreatedDate;
        Datetime renovacFecha1 =  procRenovacListToTest[1].CreatedDate;
        System.debug('fecha renovacion:'+ renovacFecha);
        System.debug('fecha renovacion:'+ renovacFecha1);
        System.debug(contactList);
        
        for( Contact c: contactList){
            System.debug(c.ActiveProcedureDate__c);
            System.assert(c.ActiveProcedureDate__c != null, 'La fecha del procedimiento activo no se actualizó después de insertar');
        }
        //wait 1 secund in order to get difference between renovacFecha and prorrogaFecha
        Integer start = System.Now().millisecond();
        while(System.Now().millisecond()< start+100){ 
        }
        // after inserting another process the date ActiveProcedureDate__c must change 
        insert procProrrogaListToTest;

        rpRT = DCUtils.getRecTypeIdByDevName('Procedure__c', 'Prorroga');
        procProrrogaListToTest = [SELECT Id, CreatedDate FROM Procedure__c WHERE recordTypeId =: rpRT];
        Datetime prorrogaFecha =  procProrrogaListToTest[0].CreatedDate;
        Datetime prorrogaFecha1 =  procProrrogaListToTest[1].CreatedDate;
        System.debug('fecha prorroga:'+prorrogaFecha);
        System.debug('fecha prorroga:'+prorrogaFecha1);
        contactList = [SELECT Id, ActiveProcedureDate__c FROM Contact ];
        for( Contact c: contactList){
            System.debug(c.ActiveProcedureDate__c);
            System.assert(c.ActiveProcedureDate__c != renovacFecha && c.ActiveProcedureDate__c != renovacFecha1,
                          'La fecha del procedimiento activo no se actualizó después de insertar prorroga');
            System.assert(c.ActiveProcedureDate__c == prorrogaFecha || c.ActiveProcedureDate__c == prorrogaFecha1,
                          'La fecha del procedimiento activo no se actualizó después de insertar prorroga');
        }
        
        procProrrogaListToTest[0].Status__c='Completado';
        //When updating a process extended to completed, the ValidExtensionDate__c date must change
        update procProrrogaListToTest[0];
        Contact c = [SELECT Id, ValidExtensionDate__c FROM Contact WHERE FirstName = 'Luis' LIMIT 1];
        System.assert(c.ValidExtensionDate__c == prorrogaFecha, 'La fecha de última prorroga no se actualizó');

        //after deleting the extension, the ActiveProcedureDate__c must reflect createddate from previous procedure
        delete procProrrogaListToTest[1];
        c = [SELECT Id, ActiveProcedureDate__c FROM Contact WHERE FirstName = 'Doris' LIMIT 1];
        System.assert(c.ActiveProcedureDate__c==renovacFecha1, 'La fecha del procedimiento activo no se actualizó después de eliminar');
        //After removing the renewal, the ActiveProcedureDate__c must be null as there are no more procedures
        delete procRenovacListToTest[1];
        c = [SELECT Id, ActiveProcedureDate__c FROM Contact WHERE FirstName = 'Doris' LIMIT 1];
        System.assert(c.ActiveProcedureDate__c==null, 'La fecha del procedimiento activo no se actualizó después de eliminar');
        
        Test.stopTest();
 
    }

    static void makeDataforContactRollUp(){
        //create data to insert
        Account acc = new Account(Active__c = 'Yes',Name = 'District Cuba',Phone='2023211169',Type='Mayorista',Sigla__c = 'DCW');
        insert acc;
             
        List<Contact> contactList = new List<Contact> ();
        Contact c = new Contact(FirstName='Luis',LastName='Suarez',AccountId=acc.Id,Eyes_Color__c='Negros');
        contactList.add(c);
        c = new Contact(FirstName='Doris',LastName='Suarez',AccountId=acc.Id,Eyes_Color__c='Negros');
        contactList.add(c);
        insert contactList;
        
        Date d = date.today();
        
        procRenovacListToTest = new List<Procedure__c>();
        //renovacion de pasaporte 
        Id rpRT = DCUtils.getRecTypeIdByDevName('Procedure__c', 'Renovacion_de_Pasaporte');
        Procedure__c p = new Procedure__c(Agency__c=acc.Id, Customer__c=contactList[0].Id, RecordTypeId=rpRT,Numero_de_Pasaporte__c='11111',
                                          First_Name__c='Luis', Last_Name__c='Suarez', Second_Last_Name__c='Valdés', Street__c='calle 10 entre 1ra y 3ra',
                                          Residence_Country__c='US',State__c='FL', City__c='Miami', Postal_Code__c='33501',
                                          Work_Name__c='Costco Call Center', Phone__c='+15657676', Title__c='Recepcionista',
                                          Departure_Date__c=d, Profession__c='Recepcionista',
                                          Immigration_Status__c='Salida Ilegal', ProfessionCategory__c='Otra',Nivel_de_Escolaridad__c='Primario',
                                          Mother_Name__c='Estrella',Father_Name__c='Jose Luis',Expired_Passport_Issue_Date__c=d,
                                          Birth_Province__c='Artemisa',Birth_Municipality_City__c='Artemisa',Razon_No_Disponibilidad__c='Perdida',
                                          Eyes_Color__c='Negros',Hair_Color__c='Negro',Skin_Color__c='Negra',Height__c=172,
                                          Reference_Full_Name__c='Ana Margarita',Reference_Second_Surname__c='Alfonso',
                                          References_Province__c='Artemisa',References_Municipality__c='Artemisa',
                                          Birth_Certificate_Issue_At__c='La Habana', Birth_Certificate_Issue_Date__c= d.addDays(-7500),
                                          Reference_Address__c='Calle 50 entre Marti y Maceo',Reference_Last_Name__c='Hurtado',
                                          Residence_Address_1__c='ave. Principal entre Marti y Maceo',
                                          Residence_Address_1_Province__c='Artemisa',Residence_Address_1_City__c='Artemisa',
                                          Residence_Year_1_From__c = '1990',Residence_Year_1_To__c = '1994', ConsulateManifestPayment__c = 50);
        procRenovacListToTest.add(p);
        //renovacion de pasaporte c
        p = new Procedure__c(Agency__c=acc.Id, Customer__c=contactList[1].Id, RecordTypeId=rpRT,Numero_de_Pasaporte__c='11112',
                              First_Name__c='Doris', Last_Name__c='Suarez', Second_Last_Name__c='Valdés', Street__c='calle 10 entre 1ra y 3ra',
                              Residence_Country__c='US',State__c='FL', City__c='Miami', Postal_Code__c='33501',
                              Work_Name__c='Costco Call Center', Phone__c='+15657676', Title__c='Recepcionista',
                              Departure_Date__c=d, Profession__c='Recepcionista',
							  Immigration_Status__c='Salida Ilegal', ProfessionCategory__c='Otra',Nivel_de_Escolaridad__c='Primario',
					          Mother_Name__c='Estrella',Father_Name__c='Jose Luis',Expired_Passport_Issue_Date__c=d,
                              Birth_Province__c='Artemisa',Birth_Municipality_City__c='Artemisa',Razon_No_Disponibilidad__c='Perdida',
							  Eyes_Color__c='Negros',Hair_Color__c='Negro',Skin_Color__c='Negra',Height__c=172,
						      Reference_Full_Name__c='Ana Margarita',Reference_Second_Surname__c='Alfonso',
							  References_Province__c='Artemisa',References_Municipality__c='Artemisa',
                              Birth_Certificate_Issue_At__c='La Habana', Birth_Certificate_Issue_Date__c= d.addDays(-7500),
                              Reference_Address__c='Calle 50 entre Marti y Maceo',Reference_Last_Name__c='Hurtado',
                              Residence_Address_1__c='ave. Principal entre Marti y Maceo',
                              Residence_Address_1_Province__c='Artemisa',Residence_Address_1_City__c='Artemisa',
                              Residence_Year_1_From__c = '1990',Residence_Year_1_To__c = '1994', ConsulateManifestPayment__c = 100);
        procRenovacListToTest.add(p);
        procProrrogaListToTest = new List<Procedure__c>();
        //Prorroga 
        rpRT = DCUtils.getRecTypeIdByDevName('Procedure__c', 'Prorroga');
        p = new Procedure__c(Agency__c=acc.Id, Customer__c=contactList[0].Id, RecordTypeId=rpRT,Numero_de_Pasaporte__c='11111',
                                          First_Name__c='Luis', Last_Name__c='Suarez', Second_Last_Name__c='Valdés', Street__c='calle 10 entre 1ra y 3ra',
                                          Residence_Country__c='US',State__c='FL', City__c='Miami', Postal_Code__c='33501',
                                          Work_Name__c='Costco Call Center', Phone__c='+15657676', Title__c='Recepcionista',
                                          Departure_Date__c=d, Profession__c='Recepcionista',
                                          Immigration_Status__c='Salida Ilegal', ProfessionCategory__c='Otra',Nivel_de_Escolaridad__c='Primario',
                                          Mother_Name__c='Estrella',Father_Name__c='Jose Luis',Expired_Passport_Issue_Date__c=d,
                                          Birth_Province__c='Artemisa',Birth_Municipality_City__c='Artemisa',Razon_No_Disponibilidad__c='Perdida',
                                          Eyes_Color__c='Negros',Hair_Color__c='Negro',Skin_Color__c='Negra',Height__c=172,
                                          Reference_Full_Name__c='Ana Margarita',Reference_Second_Surname__c='Alfonso',
                                          References_Province__c='Artemisa',References_Municipality__c='Artemisa',
                                          Birth_Certificate_Issue_At__c='La Habana', Birth_Certificate_Issue_Date__c= d.addDays(-7500),
                                          Reference_Address__c='Calle 50 entre Marti y Maceo',Reference_Last_Name__c='Hurtado',
                                          Residence_Address_1__c='ave. Principal entre Marti y Maceo',
                                          Residence_Address_1_Province__c='Artemisa',Residence_Address_1_City__c='Artemisa',
                                          Residence_Year_1_From__c = '1990',Residence_Year_1_To__c = '1994', ConsulateManifestPayment__c = 50);
        procProrrogaListToTest.add(p);
        
        //Prorroga
        p = new Procedure__c(Agency__c=acc.Id, Customer__c=contactList[1].Id, RecordTypeId=rpRT,Numero_de_Pasaporte__c='11112',
                              First_Name__c='Doris', Last_Name__c='Suarez', Second_Last_Name__c='Valdés', Street__c='calle 10 entre 1ra y 3ra',
                              Residence_Country__c='US',State__c='FL', City__c='Miami', Postal_Code__c='33501',
                              Work_Name__c='Costco Call Center', Phone__c='+15657676', Title__c='Recepcionista',
                              Departure_Date__c=d, Profession__c='Recepcionista',
							  Immigration_Status__c='Salida Ilegal', ProfessionCategory__c='Otra',Nivel_de_Escolaridad__c='Primario',
					          Mother_Name__c='Estrella',Father_Name__c='Jose Luis',Expired_Passport_Issue_Date__c=d,
                              Birth_Province__c='Artemisa',Birth_Municipality_City__c='Artemisa',Razon_No_Disponibilidad__c='Perdida',
							  Eyes_Color__c='Negros',Hair_Color__c='Negro',Skin_Color__c='Negra',Height__c=172,
						      Reference_Full_Name__c='Ana Margarita',Reference_Second_Surname__c='Alfonso',
							  References_Province__c='Artemisa',References_Municipality__c='Artemisa',
                              Birth_Certificate_Issue_At__c='La Habana', Birth_Certificate_Issue_Date__c= d.addDays(-7500),
                              Reference_Address__c='Calle 50 entre Marti y Maceo',Reference_Last_Name__c='Hurtado',
                              Residence_Address_1__c='ave. Principal entre Marti y Maceo',
                              Residence_Address_1_Province__c='Artemisa',Residence_Address_1_City__c='Artemisa',
                              Residence_Year_1_From__c = '1990',Residence_Year_1_To__c = '1994', ConsulateManifestPayment__c = 100);
        procProrrogaListToTest.add(p);        
    }
}