/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 10-30-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author             Modification
 * 1.0   10-22-2020   Silvia Velazquez   Initial Version
**/
global without sharing class DCWizardPaymentManager {
    /**
    * @description Apply payment to a procedure given an Id.
    * @author Silvia Velazquez | 10-22-2020 
    * @param String procedureId 
    * @return Procedure__c 
    **/
    global Procedure__c applyPayment(String procedureId){
        DCExpCheckoutProcedureManager manager = new DCExpCheckoutProcedureManager();
        Procedure__c procedure = manager.getProcedure(procedureId);
        if(procedure != null && 
            procedure.Origin__c == DCExpCheckoutConstants.PROCEDURE_ORIGIN &&
            procedure.Online_Step__c != DCExpCheckoutConstants.ONLINE_STEP_PAYMENT && 
            (procedure.Amount_Paid__c == null || procedure.Amount_Paid__c == 0)){
                Id rType = Schema.SObjectType.Payments__c.getRecordTypeInfosByName().get(DCExpCheckoutConstants.PAYMENT_TYPE).getRecordTypeId();
                StripeSettingsWrapper stripeSettings = getProcedureStripeSettings(procedure);
                if(stripeSettings == null){
                    DCException.throwException('Error al generar el pago en la plataforma District Cuba. La configuración del precio no existe.');
                    return null;                               
                }

                Payments__c payment = new Payments__c(Procedure__c = procedure.Id,
                                                    Agencia__c = procedure.Agency__c,
                                                    RecordTypeId = rType,
                                                    Payment_Method__c = DCExpCheckoutConstants.PAYMENT_METHOD,
                                                    Amount__c = stripeSettings.productPrice);

                procedure.Online_Step__c = DCExpCheckoutConstants.ONLINE_STEP_PAYMENT;
                try {
                    System.debug('Trying to register the Payment in SF!');
                    insert payment;
                    update procedure;
                    //CRUDEnforce.dmlUpsert(new List<SObject>{payment, procedure}, 'DCWizardPaymentManager.applyPayment');
                    System.debug('Payment Registered in SF!');
                    return procedure;
                } catch (DmlException e) {
                    DCException.throwException('Error al generar el pago en la plataforma District Cuba. ' +e.getMessage());
                    return null;
                }	
        }
        return null;
    }

    global static StripeSettingsWrapper getProcedureStripeSettings(Procedure__c procedure){

        String recordTypeName = Schema.SObjectType.Procedure__c.getRecordTypeInfosById().get(procedure.RecordTypeId).getName();

        String query = 'SELECT Procedure_Type__c, Product_Key__c, Product_Price__c, Subtype__c' 
                     + ' FROM Stripe_Checkout_Setting__mdt';
        String whereStr = ' WHERE Procedure_Type__c = \'' + string.escapeSingleQuotes(recordTypeName) + '\'';

        if(procedure.Type__c != null){
            whereStr+=' AND Subtype__c =\'' + string.escapeSingleQuotes(procedure.Type__c) + '\'';
        }

        query+= whereStr;
        System.debug('Query String: '+ query);

        List<Stripe_Checkout_Setting__mdt> stripeSettings = Database.query(query);

        if (stripeSettings.size() == 0) {
            return null;
        }

        List<Express_Checkout_URL__mdt> urls = [SELECT Success_URL__c, 
                                                        Cancel_URL__c, 
                                                        Error_URL__c
                                                FROM Express_Checkout_URL__mdt
                                                WHERE MasterLabel = 'URL Address'];

        if (urls.size() == 0) {
            return null;
        }

        StripeSettingsWrapper result =  new StripeSettingsWrapper();
        result.productKey = stripeSettings[0].Product_Key__c;
        result.productPrice = stripeSettings[0].Product_Price__c;
        result.successURL = urls[0].Success_URL__c;
        result.cancelURL = urls[0].Cancel_URL__c;
        result.errorURL = urls[0].Error_URL__c;
        return result;
    }

    global class StripeSettingsWrapper{
        public String productKey;
        public Decimal productPrice;
        public String successURL;
        public String cancelURL;
        public String errorURL; 
    }

}