/**
 * @description       : Class for Procedure Management in Express Checkout Process
 * @author            : Silvia Velazquez
 * @group             : 
 * @last modified on  : 11-04-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author             Modification
 * 1.0   10-02-2020   Silvia Velazquez   Initial Version
**/
global without sharing class DCExpCheckoutProcedureManager {
/**
    * @description Retrieve a procedure by a given Id
    * @author Silvia Velazquez | 10-02-2020 
    * @param Id procedureId 
    * @return Procedure__c 
    **/
	global Procedure__c getProcedure(Id procedureId){
        List<Procedure__c> procedures = [ SELECT Id,Name, First_Name__c, Middle_Name__c, Last_Name__c, Second_Last_Name__c, Gender__c,
										Birthday__c, Father_Name__c, Mother_Name__c, Numero_de_Pasaporte__c, Skin_Color__c, Eyes_Color__c, 
										Hair_Color__c, Height__c, Profession__c, ProfessionCategory__c, Nivel_de_Escolaridad__c, Title__c,
                                        Departure_Date__c,Immigration_Status__c, Birth_Country__c, Birth_Province__c,Birth_ProvinceT__c,
                                        Phone__c, Email__c,Birth_Municipality_City__c, Birth_Municipality_CityT__c,State__c, City__c,
                                        Residence_Country__c, Postal_Code__c, Street__c,Work_State__c, Work_City__c, Work_Phone__c,
                                        Work_Email__c,Work_Country__c,Work_Name__c,Work_Postal_Code__c,Work_Street__c,Residence_Address_1__c,
                                        Residence_Address_1_Province__c,Residence_Address_1_City__c, Residence_Year_1_From__c,  
                                        Residence_Year_1_To__c,Residence_Address_2__c,Residence_Address_2_Province__c, 
                                        Residence_Address_2_City__c, Residence_Year_2_From__c,Residence_Year_2_To__c, Reference_Full_Name__c, 
										Reference_Second_Name__c,Reference_Last_Name__c,Reference_Second_Surname__c,Reference_Address__c,
                                        Reference_Phone__c, References_Province__c, Phone1__c, Phone2__c, Relationship__c, 
                                        Special_Characteristics__c, References_Municipality__c, Reference_District__c,Comments__c,
                                        Category__c, Passport_Type__c, RecordType.Name, Passport_Expiration_Date__c, Agency__c, Origin__c,
                                        Total__c, Subtotal__c, Online_Step__c, Amount_Paid__c, Marital_Status__c, Type__c, Shipment_Address__c,
                                        Shipment_City__c, Shipment_State__c, Shipment_Postal_Code__c							
										FROM Procedure__c
                                        WHERE Id = :procedureId];
        
        return (procedures.size() > 0) ? procedures[0] : null;
    }
    
    /**
	* @description Upsert procedure
	* @author Silvia Velazquez | 10-02-2020 
	* @param ProcedureWrapper p 
	* @return Procedure__c 
	**/
	global Procedure__c upsertProcedure(Procedure__c procedure, String procedureType){
        ConfigurationWrapper config = getProcedureConfiguration(procedureType);

        Contact customer = DCExpCheckoutContactManager.getContact(procedure);
        if(customer == null){
            customer = DCExpCheckoutContactManager.saveContact(procedure,config.agencyId);
        }
         
        if(customer == null){
            DCException.throwException('Error al obtener el contacto.');
            return null;
		}
 
        if(procedure.Id == null){
            procedure.Customer__c = customer.id;
            procedure.Agency__c = config.agencyId;
            procedure.Status__c = DCExpCheckoutConstants.PROCEDURE_STATUS;
            procedure.Origin__c = DCExpCheckoutConstants.PROCEDURE_ORIGIN;
            procedure.RecordTypeId = config.procedureRecordTypeId;
            procedure.Type__c = config.prorrogueType;
        }

        procedure.Total__c = config.procedureSalesPrice;
        procedure.Subtotal__c = config.procedureSalesPrice;
        procedure.Consulate_Payment__c = config.consulatePayment;
        //procedure.Agency_Commission__c = config.agencyComission;

		try {
            //CRUDEnforce.dmlUpsert(new List<Procedure__c>{procedure}, 'DCExpCheckoutProcedureManager.upsertProcedure');
            upsert procedure;
			return procedure;
		} catch (DmlException e) {
            DCException.throwException('Error al generar el trámite: ' + e.getMessage());
			return null;
		}	
    }


	/**
	* @description Get the price configuration given the Agency and the Pprocedure type
	* @author Silvia Velazquez | 10-06-2020 
	* @param String procedureType 
	* @return ConfigurationWrapper 
	**/
	global ConfigurationWrapper getProcedureConfiguration(String procedureType){
        if(!(Schema.sObjectType.Account.isAccessible() &&
				Schema.sObjectType.Account.fields.Id.isAccessible() &&
                // Schema.sObjectType.Account.fields.Sigla__c.isAccessible() &&
                Schema.sObjectType.Procedure__c.isAccessible() &&
                Schema.sObjectType.Procedure_Setup__c.isAccessible() &&
                Schema.sObjectType.Procedure_Setup__c.fields.Id.isAccessible() &&
                Schema.sObjectType.Procedure_Setup__c.fields.Sales_Price__c.isAccessible() &&
                Schema.sObjectType.Procedure_Setup__c.fields.ConsulatePayment__c.isAccessible())) {
				DCException.throwPermiException('dcExpressCheckoutProcedureManager.getAgency');
        }

        ConfigurationWrapper config = new ConfigurationWrapper();
        //Getting the Agency
		List<Account> agencies = [SELECT Id
                                    FROM Account 
                                    WHERE Name = :DCExpCheckoutConstants.DC_AGENCY];

        if(agencies.size() == 0){
            DCException.throwException('La Agencia District Cuba no esta registrada en el sistema.');
        }

        config.agencyId = agencies[0].Id;

        //Getting the Procedure RecordType
        if(!(DCExpCheckoutConstants.typesMap.containsKey(procedureType))){
           DCException.throwException('El tipo de Trámite no es válido.');
        }

        config.procedureRecordTypeName = DCExpCheckoutConstants.typesMap.get(procedureType);
        config.procedureRecordTypeId = Schema.SObjectType.Procedure__c.getRecordTypeInfosByName()
                                                                      .get(config.procedureRecordTypeName).getRecordTypeId();
        config.prorrogueType = (procedureType == DCExpCheckoutConstants.PRORROGA) 
                               ? DCExpCheckoutConstants.REGULAR_EXTENSION_TYPE
                               :((procedureType == DCExpCheckoutConstants.PRORROGA_DOBLE)
                                    ? DCExpCheckoutConstants.DOUBLE_EXTENSION_TYPE
                                    : null);

       //Getting the Procedure Price from Procedure_Setup object
       List<Procedure_Setup__c> ps = [SELECT Id, 
                                        Sales_Price__c, 
                                        ConsulatePayment__c,
                                        Agency_Commission__c 
									FROM Procedure_Setup__c
									WHERE Agency__c  = :config.agencyId AND 
										Procedure_Type__c = :config.procedureRecordTypeName AND
										Subtype__c = :config.prorrogueType
									LIMIT 1];

		if(ps.size() == 0){
			DCException.throwException('La configuración del precio para el tipo de trámite seleccionado no existe.');
		}

		config.procedureSalesPrice = ps[0].Sales_Price__c;
		config.consulatePayment = ps[0].ConsulatePayment__c;
		config.agencyComission = ps[0].Agency_Commission__c;

		return config;
/*         DCWizardPaymentManager.StripeSettingsWrapper paymentSetting = 
        DCWizardPaymentManager.getProcedureStripeSetting(config.procedureRecordTypeId,config.prorrogueType);
        if(paymentSetting == null){
            DCException.throwException('La configuración del precio para el tipo de trámite seleccionado no existe.');
            return null;
        }                        

        config.procedureSalesPrice = paymentSetting.productPrice;
        config.productKey = paymentSetting.productKey; */
	}

    global class ConfigurationWrapper{
        public String agencyId;
        public String procedureRecordTypeId;
        public String procedureRecordTypeName;
        public String prorrogueType;
        public Decimal procedureSalesPrice;
		public Decimal consulatePayment;
		public Decimal agencyComission;
    }
}