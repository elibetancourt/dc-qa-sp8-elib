/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-02-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   11-02-2020   Elizabeth Betancourt Herrera   Initial Version
**/
@isTest
public with sharing class dcDeliveryRateTriggerHandlerTest {
    
    /**
    * Testing all after insert trigger
    **/
    @isTest
    static void deliveryRateCreateTest() {
        Test.startTest();
        
        Delivery_Rates__c tarifa = new Delivery_Rates__c(Active__c=true,Name='FedEx 2Day AM',Rate__c=35,Description__c='Public rate $99.99 delivery time 9:30 am');
        insert tarifa;

        List<Delivery_Rates__Share> sharedRecords = [SELECT Id, ParentId, UserOrGroupId FROM Delivery_Rates__Share WHERE parentId=:tarifa.Id];
        System.debug(sharedRecords.size());
        System.assert(sharedRecords.size()>0, 'La tarifa de envio no fue compartida con el grupo de la agencia.');
   
        Test.stopTest();
    }

}