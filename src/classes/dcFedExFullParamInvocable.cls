/**
 * @File Name          : dcFedExFullParamInvocable.cls
 * @Description        : Class with invocable method to generate a shipment with all parameters
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 4/8/2020 5:04:25 p. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    15/7/2020   Silvia Velazquez     Initial Version
**/
public with sharing class dcFedExFullParamInvocable {

    @InvocableMethod(label='Generate Tracking' description='Generate Shipment Information and Label file using parameters received from UI.')
    public static List<String> generateTrackingFullParam(List<InputParam> params) {
        dcShipmentManager mgr = new dcShipmentManager();
        Integer length = (params[0].length !=null)?params[0].length:5;
        Integer height = (params[0].height !=null)?params[0].height:5;
        Integer width = (params[0].width !=null)?params[0].width:5;
        String tipo = '';
        String result = mgr.generateShipment(false,null,null,params[0].loteId,params[0].packageCount,params[0].packageType,params[0].serviceType,params[0].weight,length,width,height,tipo);
        List<String> trackingsID = new List<String>{result};
        return trackingsID;
    }

    //input Ids that comes to apex from flow
    public class InputParam{
        @InvocableVariable(required=true)
        public Id loteId;
        
        @InvocableVariable(required=true)
        public Integer packageCount;

        @InvocableVariable(required=true)
        public String packageType;

        @InvocableVariable(required=true)
        public String serviceType;

        @InvocableVariable(required=true)
        public Decimal weight;

        @InvocableVariable
        public Integer length;

        @InvocableVariable
        public Integer width;

        @InvocableVariable
        public Integer height;
        
    }

}