/**
 * @File Name          : dcSearchContactResultTest.cls
 * @Description        : 
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Ibrahim Napoles
 * @Last Modified On   : 08-26-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    31/7/2020   Silvia Velazquez     Initial Version
**/
@isTest
private class dcSearchContactResultTest {
    @isTest
    static void constructorTest1(){
        Test.startTest();
        dcSearchContactResult result = new dcSearchContactResult(new dcSearchContact());
        Test.stopTest();
        System.assertNotEquals(null, result, '');
    }

    @isTest
    static void constructorTest2(){
        Test.startTest();
        List<dcContactProcedureSearch> listResults = new List<dcContactProcedureSearch>();
        dcSearchContactResult result = new dcSearchContactResult(listResults,new dcSearchContact());
        Test.stopTest();
        System.assertNotEquals(null, result, '');
    }
}