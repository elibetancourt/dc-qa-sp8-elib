/**
 * General class to check Permissions
 * Note: Refactoring pending. It was copied quickly to pass security review...
 */
public with sharing class CRUDEnforce {
    public static final String MESSAGE = 'You have no permission to ';
    public static final String ACCESS_MESSAGE = MESSAGE + 'ACCESS: ';
    public static final String CREATE_MESSAGE = MESSAGE + 'CREATE: ';
    public static final String UPSERT_MESSAGE = MESSAGE + 'UPSERT: ';
    public static final String UPDATE_MESSAGE = MESSAGE + 'UPDATE: ';
    public static final String DELETE_MESSAGE = MESSAGE + 'DELETE: ';

    private static final Set<String> FIELDS_TO_EXCLUDE =
        new Set<String> {
        'Id', 'CreatedDate'
    };

    /******************** ACCESSIBLE ********************/

    /**
     * @description Check Accessible permission
     * @author Ibrahim Napoles | 08-04-2020
     * @param List<SObject> objList
     * @param String pMethodName
     **/
    public static Boolean dmlAccessible(
        String objectName,
        List<String> objFields,
        String pMethodName)
    {
        if (objectName == null || objFields == null ||
            objFields.isEmpty()) {return false;}

        SObjectType myType =
            Schema.getGlobalDescribe().get(objectName);

        if (!myType.getDescribe().isAccessible()) {
            throwException(pMethodName, ACCESS_MESSAGE, myType);
        }

        return checkAccessible(myType, objFields, pMethodName);
    }

    /******************** INSERT ********************/

    /**
     * @description Check Create permissino
     * @author Ibrahim Napoles | 08-04-2020
     * @param List<SObject> objList
     * @param String pMethodName
     **/
    public static void dmlInsert(
        List<SObject> objList,
        String pMethodName)
    {
        dmlInsert(objList, pMethodName, true);
    }

    /**
     * @description Check Insert Permission
     * @author Ibrahim Napoles | 08-04-2020
     * @param List<SObject> objList
     * @param String pMethodName
     * @param Boolean pAllOrNone
     * @return List<Database.SaveResult>
     **/
    public static List<Database.SaveResult> dmlInsert(
        List<SObject> objList,
        String pMethodName,
        Boolean pAllOrNone)
    {
        if (objList == null || objList.isEmpty())
        {return new List<Database.SaveResult>();}

        SObjectType myType =
            objList[0].getSObjectType();

        if (!myType.getDescribe().isCreateable()) {
            throwException(pMethodName, CREATE_MESSAGE, myType);
        }

        checkOperation(objList, myType, pMethodName, 1);

        return Database.insert(objList, pAllOrNone);
    }

    /******************** UPDATE ********************/

    /**
     * @description Check Update permission
     * @author Ibrahim Napoles | 08-04-2020
     * @param List<SObject> objList
     * @param String pMethodName
     **/
    public static void dmlUpdate(
        List<SObject> objList,
        String pMethodName)
    {
        dmlUpdate(objList, pMethodName, true);
    }

    /**
     * @description Check Update Permission
     * @author Ibrahim Napoles | 08-04-2020
     * @param List<SObject> objList
     * @param String pMethodName
     * @param Boolean pAllOrNone
     * @return List<Database.SaveResult>
     **/
    public static List<Database.SaveResult> dmlUpdate(
        List<SObject> objList,
        String pMethodName,
        Boolean pAllOrNone)
    {
        if (objList == null || objList.isEmpty())
        {return new List<Database.SaveResult>();}

        SObjectType myType = objList[0].getSObjectType();

        if (!myType.getDescribe().isUpdateable()) {
            throwException(pMethodName, UPDATE_MESSAGE, myType);
        }

        checkOperation(objList, myType, pMethodName, 2);

        return Database.update(objList, pAllOrNone);
    }

    /******************** UPSERT ********************/

    /**
     * @description Check Upsert permission
     * @author Ibrahim Napoles | 08-04-2020
     * @param List<SObject> objList
     * @param String pMethodName
     **/
    public static void dmlUpsert(
        List<SObject> objList,
        String pMethodName)
    {
        dmlUpsert(objList, pMethodName, true);
    }

    /**
     * @description Check Upsert Permission
     * @author Ibrahim Napoles | 08-04-2020
     * @param List<SObject> objList
     * @param String pMethodName
     * @param Boolean pAllOrNone
     * @return List<Database.UpsertResult>
     **/
    public static List<Database.UpsertResult> dmlUpsert(
        List<SObject> objList,
        String pMethodName,
        Boolean pAllOrNone)
    {
        if (objList == null || objList.isEmpty())
        {return new List<Database.UpsertResult>();}

        SObjectType myType = objList[0].getSObjectType();
        Schema.DescribeSObjectResult objDesc = myType.getDescribe();
        // if (!objDesc.isCreateable() || !objDesc.isUpdateable()) {
            System.debug('Is Createable: ' + objDesc.isCreateable());
            System.debug('Is Updateable: ' + objDesc.isUpdateable());

        if (!objDesc.isCreateable() || !objDesc.isUpdateable()) {
            throwException(pMethodName, UPSERT_MESSAGE, myType);
        }

        checkOperation(objList, myType, pMethodName, 0);

        return Database.upsert(objList, pAllOrNone);
    }

    /******************** DELETE ********************/

    /**
     * @description Check Delete Permission
     * @author Ibrahim Napoles | 08-04-2020
     * @param List<SObject> objList
     * @param String pMethodName
     **/
    public static void dmlDelete(
        List<SObject> objList,
        String pMethodName)
    {
        dmlDelete(objList, pMethodName, true);
    }

    /**
     * @description Check Delete Permission
     * @author Ibrahim Napoles | 08-04-2020
     * @param List<SObject> objList
     * @param String pMethodName
     * @param Boolean pAllOrNone
     * @return List<Database.DeleteResult>
     **/
    public static List<Database.DeleteResult> dmlDelete(
        List<SObject> objList,
        String pMethodName,
        Boolean pAllOrNone)
    {
        if (objList == null || objList.isEmpty())
        {return new List<Database.DeleteResult>();}

        SObjectType myType = objList[0].getSObjectType();

        if (!myType.getDescribe().isDeletable()) {
            throwException(pMethodName, DELETE_MESSAGE, myType);
        }

        return Database.delete(objList, pAllOrNone);
    }

    /******************** GENERAL ********************/

    /**
     * @description Check Object's fields Accessible
     * @author Ibrahim Napoles | 08-04-2020
     * @param SObject[] objList
     * @param SObjectType myType
     * @param String pMethodName
     **/
    public static Boolean checkAccessible(
        SObjectType myType,
        List<String>  objFields,
        String pMethodName)
    {
        Map<String, Schema.SObjectField> fieldMap =
            myType.getDescribe().fields.getMap();

        for (String fieldName : objFields) {
            Schema.SObjectField myField = fieldMap.get(fieldName);
            if (myField == null) {continue;}

            if (!myField.getDescribe().isAccessible()) {
                throwException(
                    pMethodName,
                    ACCESS_MESSAGE,
                    myType,
                    myField);
            }
        }
        return true;
    }

    /**
     * @description Check Object's fields Operation
     * @author Ibrahim Napoles | 08-04-2020
     * @param SObject[] objList
     * @param SObjectType myType
     * @param String pMethodName
     * @param Integer pOperation 1=>Insert, 2=>Update, Anyother=>Upsert
     **/
    public static void checkOperation(
        SObject[] objList,
        SObjectType myType,
        String pMethodName,
        Integer pOperation)
    {
        Schema.SObjectField myField;
        Schema.DescribeFieldResult fDesc;

        for (SObject so : objList) {
            Map<String, Schema.SObjectField> fieldMap =
                myType.getDescribe().fields.getMap();

            for (String fieldName :so.getPopulatedFieldsAsMap().keySet()) {
                // Check for excluded fields
                if(FIELDS_TO_EXCLUDE.contains(fieldName)) {continue;}

                myField = fieldMap.get(fieldName);

                if (myField == null) {continue;}

                fDesc = myField.getDescribe();

                if (isExclude(fDesc)) {continue;}

                //System.debug('Field: '+fDesc);

                switch on pOperation {
                    // INSERT
                    when 1 {
                        if (fDesc.isCreateable() == false) {
                            throwException(
                                pMethodName,
                                CREATE_MESSAGE,
                                myType,
                                myField);
                        }
                    }
                    // UPDATE
                    when 2 {
                        if (isNotExclude(fDesc)) {
                            throwException(
                                pMethodName,
                                UPDATE_MESSAGE,
                                myType,
                                myField);
                        }
                    }
                    // UPSERT
                    when else {
                        if (!fDesc.isCreateable()
                            && isNotExclude(fDesc)) {
                            throwException(
                                pMethodName,
                                UPSERT_MESSAGE,
                                myType,
                                myField);
                        }
                    }
                }
            }
        }
    }

    /**
     * @description Exclude autoNumbers, calculated and masterDetails
     * @author Ibrahim Napoles | 08-26-2020
     * @param Schema.DescribeFieldResult fDesc
     * @return Boolean
     **/
    private static Boolean isExclude(Schema.DescribeFieldResult fDesc){
        return fDesc.isCalculated()
               || fDesc.isAutoNumber()
               || fDesc.getRelationshipOrder() == null;
    }

    /**
     * @description Exclude autoNumbers, calculated and masterDetails
     * @author Ibrahim Napoles | 08-26-2020
     * @param Schema.DescribeFieldResult fDesc
     * @return Boolean
     **/
    private static Boolean isNotExclude(Schema.DescribeFieldResult fDesc){
        return !fDesc.isCalculated()
               && !fDesc.isAutoNumber()
               && fDesc.getRelationshipOrder() == null
               && !fDesc.isUpdateable();
    }

    /**
     * Throw particular exception
     */
    @TestVisible
    private static void throwException(
        String methodName,
        String msg,
        SObjectType obj,
        Schema.SObjectField field)
    {
        throwException(
            methodName + ' ' + msg + obj +
            ' / ' + field);
    }

    @TestVisible
    private static void throwException(
        String methodName,
        String msg,
        SObjectType obj)
    {
        throwException(
            methodName + ' ' + msg + obj);
    }

    /**
     * Throw particular exception
     */
    private static void throwException(String msg)
    {
        DCException.throwPermiException(msg);
    }
}