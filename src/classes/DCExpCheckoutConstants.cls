/**
 * @description       : Class to define Express Checkout Constants.
 * @author            : 
 * @group             : 
 * @last modified on  : 11-04-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author             Modification
 * 1.0   10-06-2020   Silvia Velazquez   Initial Version
**/
global without sharing class DCExpCheckoutConstants {
    //Numeric constant values for Procedure RecordTypes
    public static final String PRIMERA_VEZ = '1'; 
    public static final String RENOVACION = '2'; 
    public static final String PRORROGA = '3'; 
    public static final String PRORROGA_ESTANCIA = '4'; 
    public static final String VISA_TURISMO = '5'; 
    public static final String VISA_HE11 = '6'; 
    public static final String CIUDADANIA_CUBANA = '7'; 
    public static final String CERT_ORIGINALES = '8'; 
    public static final String LEGALIZACION = '9';
    public static final String PODER_NOTARIAL = '10';
    public static final String PRORROGA_DOBLE = '11';
    //Values to store in SF
    public static final String DOUBLE_EXTENSION_TYPE = 'Doble';
    public static final String REGULAR_EXTENSION_TYPE = 'Regular';
    public static final String DC_AGENCY = 'District Cuba';
    public static final String PROCEDURE_STATUS = 'Preparacion';
    public static final String PROCEDURE_ORIGIN = 'Web';
    public static final String OPERATIONS_ACCOUNT = 'Operaciones';
    public static final String ESCROW_ACCOUNT = 'Escrow';
    public static final String PAYMENT_TYPE = 'Pago Regular';
    public static final String PAYMENT_METHOD = 'Tarjeta de Credito/Debito';
    public static final String ONLINE_STEP_INFORMATION = 'Entrada de Datos';
    public static final String ONLINE_STEP_CONFIRMATION = 'Confirmación';
    public static final String ONLINE_STEP_PAYMENT = 'Pago';
    //Default values for picklist fields
    public static final String BIRTH_PROVINCE_LABEL = 'Provincia de nacimiento en Cuba';
    public static final String BIRTH_CITY_LABEL = 'Municipio de nacimiento en Cuba';
    public static final String BIRTH_COUNTRY_LABEL = 'País de origen si nació fuera de Cuba';
    public static final String REF_PROVINCE_LABEL = 'Provincia de la persona de referencia*';
    public static final String REF_CITY_LABEL = 'Municipio de la persona de referencia*';
    public static final String LAST_PROVINCE_LABEL = 'Mi última dirección: Provincia*';
    public static final String PENULT_PROVINCE_LABEL = 'Penúltima dirección: Provincia';
    public static final String PENULT_CITY_LABEL = 'Penúltima dirección: Municipio';
    public static final String MARITAL_STATUS_LABEL = 'Estado Civil*';
    public static final String GENDER_LABEL = 'Sexo*';
    public static final String EYES_COLOR_LABEL = 'Color de ojos*';
    public static final String SKIN_COLOR_LABEL = 'Color de piel*';
    public static final String HAIR_COLOR_LABEL = 'Color de pelo*';
    public static final String MIGRATION_STATUS_LABEL = 'Clasificación Migratoria al salir de Cuba*';
	public static final String EDUCATION_LABEL = 'Nivel de escolaridad';
	public static final String STATE_LABEL = 'Estado donde reside en EEUU*';
	public static final String SHIP_STATE_LABEL = 'Estado de Envío*';
	public static final String SHIP_COUNTRY = 'US';
	public static final String WHOLESALER_ACCOUNT = 'Mayorista';

    public static Map<String,String> typesMap = new Map<String,String>{
                        PRIMERA_VEZ => 'Pasaporte 1ra Vez',
                        RENOVACION => 'Renovacion de Pasaporte',
                        PRORROGA => 'Prorroga',
                        PRORROGA_DOBLE => 'Prorroga',
                        PRORROGA_ESTANCIA => 'Prorroga de Estancia',
                        VISA_TURISMO => 'Visa de Turismo',
                        VISA_HE11 => 'Visa HE-11',
                        CIUDADANIA_CUBANA => 'Ciudadania Cubana',
                        CERT_ORIGINALES => 'Certificados Originales',
                        LEGALIZACION => 'Legalizacion',
                        PODER_NOTARIAL => 'Poder Notarial'
    };

}