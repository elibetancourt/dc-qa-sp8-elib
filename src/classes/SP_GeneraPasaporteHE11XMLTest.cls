/**
 * @description       : 
 * @author            : William Santana Méndez
 * @group             : 
 * @last modified on  : 07-27-2020
 * @last modified by  : William Santana Méndez
 * Modifications Log 
 * Ver   Date         Author                   Modification
 * 1.0   07-27-2020   William Santana Méndez   Initial Version
**/
@isTest
public  class SP_GeneraPasaporteHE11XMLTest {
    @TestSetup
    static void makeData(){
        
        Account account1=SP_XMLDataFactory.createAccount();
        insert account1;
        
        Contact contact1=SP_XMLDataFactory.createContact();
        insert contact1;        

        List<Manifest__c> manifiestoList=SP_XMLDataFactory.createManifestList();
        insert manifiestoList;

        List<Procedure__c> procedureListFull=SP_XMLDataFactory.createProcedureListFull(contact1, account1, manifiestoList);
        insert procedureListFull;              
    }

    @isTest
    public static void checkXMLHE(){
        Test.startTest();
        List<Manifest__c> manifestList = [SELECT Id, Name FROM Manifest__c WHERE Type__c = 'Visa HE-11'];
        List<String> manifiestos = new List<String>();
        manifiestos.add(manifestList[0].Id);
        SP_GeneraPasaporteHE11XML.generaXML(manifiestos);
        ContentDocumentLink zipFile = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =:manifiestos.get(0) LIMIT 1];
        Boolean exists=false;
        if(zipFile!=null)
            exists=true;    
        System.assertEquals(true,exists,'The file not exist');
        Test.stopTest();
    }
}