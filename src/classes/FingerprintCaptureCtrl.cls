/**
 * FingerprintCaptur controller
 * @version 0.1
 * @author Fernando Gomez
 */
global class FingerprintCaptureCtrl {
	/**
	 * Returns the current session id
	 * @param recordId
	 * @param imageData
	 */
	@AuraEnabled
	global static String getFingerprint(Id recordId) {
		try {
			return [
				SELECT FingerprintURL__c
				FROM Procedure__c
				WHERE Id = :recordId
			].FingerprintURL__c;
		} catch (Exception ex) {
			throw new AuraException(ex.getMessage());
		}
	}

	/**
	 * Returns the current session id
	 * @param recordId
	 * @param imageData
	 */
	@AuraEnabled
	global static String saveFingerprint(Id recordId, String imageData) {
		Procedure__c t;
		Attachment a;
		try {
			a = new Attachment(
				ParentId = recordId,
				ContentType = 'image/bmp',
				Name = 'fingerprint.bmp',
				Body = EncodingUtil.base64Decode(imageData)
			);
			insert a;

			t = [
				SELECT Id
				FROM Procedure__c
				WHERE Id = :recordId
			];

			t.FingerprintURL__c = '/servlet/servlet.FileDownload?file=' + a.Id;
			update t;
			return t.FingerprintURL__c;
		} catch (Exception ex) {
			throw new AuraException(ex.getMessage());
		}
	}
}