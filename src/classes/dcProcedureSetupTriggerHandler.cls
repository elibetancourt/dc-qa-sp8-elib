/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-02-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   11-02-2020   Elizabeth Betancourt Herrera   Initial Version
**/
public without sharing class dcProcedureSetupTriggerHandler {
    
    Boolean isBefore;
    Boolean isAfter;
    Boolean isInsert;
    Boolean isDelete;
    Boolean isUpdate;
    List<Procedure_Setup__c> newList {get; set;}
    List<Procedure_Setup__c> oldList {get; set;}
    Map<Id, Procedure_Setup__c> oldMap {get; set;}
    
    public dcProcedureSetupTriggerHandler() {
        newList = trigger.new;
        oldList = trigger.old;
        oldMap = (Map<Id, Procedure_Setup__c>)trigger.oldMap;
        this.isBefore = trigger.isBefore;
        this.isAfter = trigger.isAfter;
        this.isInsert = trigger.isInsert;
        this.isUpdate = trigger.isUpdate;
        this.isDelete = trigger.isDelete;
    }

    public void procedureSetupTriggerActions (){
        
        if(isAfter && isInsert) {
            //share account whith user in public group
            shareRecord();
        }                           
    }

    public void shareRecord (){
        
       Map<String, List<sObject>> groupProcedureSetupList = getGroupListToShare();
       System.debug('Lista a compartir') ;
       for(String groupId: groupProcedureSetupList.keySet()){
           System.debug('groupId --> '+groupId) ;
           System.debug('total a compratir --> '+groupProcedureSetupList.get(groupId).size()) ;
           dcSharingManager.shareRecords(groupProcedureSetupList.get(groupId), 
                                         'Procedure_Setup__Share', 'Edit', groupId, 'Manual');
       }
    }

    public Map<String, List<sObject>> getGroupListToShare (){
        Map<String, String> agencyGroupMap;
        
        if(newList.size() == 1){
            agencyGroupMap = dcPublicGroupUtils.getAgencyGroupMap(newList[0].Agency__c);
        } else {
            agencyGroupMap = dcPublicGroupUtils.getAgencyGroupMap('');
        }

        Map<String, List<sObject>> groupProcedureSetupList = new Map<String, List<sObject>>();
        
        for( Procedure_Setup__c newStup: newList){
            List<sObject> procSetupToShareList = new List<sObject>{newStup};
            if(agencyGroupMap != null && agencyGroupMap.containsKey(newStup.Agency__c)){
                String groupId = agencyGroupMap.get(newStup.Agency__c);
                groupProcedureSetupList.put(groupId, procSetupToShareList);
            }   
        }

        return groupProcedureSetupList;
    }
}