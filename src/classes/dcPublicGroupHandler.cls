/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 10-29-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   10-28-2020   Elizabeth Betancourt Herrera   Initial Version
**/
public without sharing class dcPublicGroupHandler {
    
    @InvocableMethod(label='Create Public Group' description='Returns id of public group created. ')
    public static List<String> createPublicGroup(List<String> groupName) {
        
        List<Group> groupList = new List<Group>();
        
        for(string name: groupName){
            Group g = new Group(
                                    Name='DC-'+name,
                                    DeveloperName='DC_'+name.replace(' ','_'),
                                    Type='Regular'
                                );
            groupList.add(g);

        }

        CRUDEnforce.dmlInsert(groupList, 'dcPublicGroupHandler.createPublicGroup');
        
        List<String> groupIdsList = new List<String>();
        for(Group g: groupList){
            groupIdsList.add(g.Id);
        }

        return groupIdsList;

       /* try {
            insert g;
        }
        catch (DmlException e){
            DCException.throwException('dcPublicGroupHandler.createPublicGroup Error: '+e.getDmlMessage(0));
        }*/
    }

    public static Map<String, String> createPublicGroupWithMap(List<String> groupName) {
        List<Group> groupList = new List<Group>();
        
        for(string name: groupName){
            Group g = new Group(
                                    Name='DC-'+name,
                                    DeveloperName='DC_'+name.replace(' ','_'),
                                    Type='Regular'
                                );
            groupList.add(g);

        }

        CRUDEnforce.dmlInsert(groupList, 'dcPublicGroupHandler.createPublicGroup');
        
        Map<String, String> nameGroupIdMap = new Map<String, String>();
        
        for(Integer i=0; i<groupName.size(); i++){
            nameGroupIdMap.put(groupName[i], groupList[i].Id);
        }

        return nameGroupIdMap;

       /* try {
            insert g;
        }
        catch (DmlException e){
            DCException.throwException('dcPublicGroupHandler.createPublicGroup Error: '+e.getDmlMessage(0));
        }*/
    }

}