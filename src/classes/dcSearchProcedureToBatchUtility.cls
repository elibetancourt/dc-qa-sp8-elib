/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 10-21-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   10-19-2020   Elizabeth Betancourt Herrera   Initial Version
**/
public with sharing class dcSearchProcedureToBatchUtility {

    public static dcSearchProcedureResult procedureResult {get; set;}
    public static String resultFields = 'Id, Name, CreatedDate, RecordType.Name, Status__c, Agency__r.Name, Customer__c, Customer__r.FirstName, Customer__r.LastName, Customer__r.Middle_Name__c, Customer__r.Second_Last_Name__c, Customer__r.MobilePhone';
    
    public static dcSearchProcedureResult count(dcSearchProcedureToBatch searchPattern){
        //check accesibility
        checkAccessible();
        procedureResult = search(searchPattern);
        String query = generateQuery(searchPattern, false); 
        
        List<Procedure__c> listProcedure = Database.query(query);
                
        procedureResult.count = listProcedure.size();
        return procedureResult;
    }
    
    public static dcSearchProcedureResult search(dcSearchProcedureToBatch searchPattern){
        
        //check accesibility
        checkAccessible();
        String query = generateQuery(searchPattern, true);     
        System.debug(query);
        List<Procedure__c> listProcedure = Database.query(query);

        List<dcContactProcedureSearch> resultList = new List<dcContactProcedureSearch>();
        if(listProcedure.size() > 0){            
            for(Procedure__c proc:listProcedure){
                
                    dcContactProcedureSearch item = new dcContactProcedureSearch();
                    /*item.contactId = proc.Customer__c;
                    item.firstName = proc.Customer__r.FirstName;
                    item.middleName = proc.Customer__r.Middle_Name__c;
                    item.lastName = proc.Customer__r.LastName;
                    item.secondLastName = proc.Customer__r.Second_Last_Name__c;*/
                    item.fullName = getFullName(proc);
                    item.phone = proc.Customer__r.MobilePhone;
                    item.procedureId = proc.Id;
                    item.procedureName = proc.Name;
                    item.procedureType = proc.RecordType.Name;
                    /*item.procedureStatus = proc.Status__c;
                    item.procedureAgency = proc.Agency__r.Name;*/
                    String strDatetime = proc.CreatedDate.format('MM/dd/yyyy', 'UTC');
                    //Datetime formattedDate = (DateTime)JSON.deserialize('"' + strDatetime + '"', DateTime.class);
                    item.procedureDate = strDatetime;
                    item.procedureNameLink = '/'+item.procedureId;
                    resultList.add(item);
                
            } 
        }
    
        procedureResult = new dcSearchProcedureResult(resultList,searchPattern);
        return procedureResult;
    }
    
    
    public static dcSearchProcedureResult reset(dcSearchProcedureToBatch searchPattern){
        procedureResult = new dcSearchProcedureResult(searchPattern);
    
        return procedureResult;
    }

    public static String generateQuery(dcSearchProcedureToBatch searchPattern, Boolean withLimit){
       
        String procedureWhere = getProcedureWhere(searchPattern,withLimit);
        String orderOrientation = getOrientationOrder(searchPattern);
        string query = 'SELECT '+ string.escapeSingleQuotes(resultFields) +' FROM Procedure__c WHERE '+ procedureWhere + ' ORDER BY ' +searchPattern.sortBy +' '+ orderOrientation;
        
        if(withLimit == true){
            query += ' LIMIT '+searchPattern.pageSize+' OFFSET '+searchPattern.offset;
        }
        
        return query;        
    }

    public static String getProcedureWhere(dcSearchProcedureToBatch searchPattern, Boolean withLimit){
        
        String result = '';
        if (searchPattern.status != '')
               result = 'Status__c =\'' + searchPattern.status + '\'';
        if (searchPattern.agency != '') {
            if (result != '') result += ' AND ';
            result += 'Agency__c =\''+ searchPattern.agency + '\'';
        }
        if (searchPattern.noBatch) {
            if (result != '') result += ' AND ';
            result += 'Agency_Batch__c = null';
        } 
      
        if (searchPattern.noDistributor) {
            if (result != '') result += ' AND ';
            result += 'Distributor_Batch__c = null';
        }

        if (searchPattern.filter != '') {
            if (result != '') result += ' AND ';
            result += '(First_Name__c LIKE \'%'+ searchPattern.filter + '%\' OR ';
            result += 'Middle_Name__c LIKE \'%'+ searchPattern.filter + '%\' OR ';
            result += 'Last_Name__c LIKE \'%'+ searchPattern.filter + '%\' OR ';
            result += 'Second_Last_Name__c LIKE \'%'+ searchPattern.filter + '%\' )';
        }

        if (searchPattern.dateFrom != null){
            String formattedDate = getDateFormat(searchPattern.dateFrom)+'T00:00:00.0Z';
            
            if (result != '') result += ' AND ';
            result += 'CreatedDate >= '+ formattedDate;
        }

        if (searchPattern.dateTo != null){
            String formattedDate = getDateFormat(searchPattern.dateTo)+'T23:59:59.0Z';
            
            if (result != '') result += ' AND ';
            result += 'CreatedDate <= '+ formattedDate;
        }

        if (searchPattern.procedureTypes != null && searchPattern.procedureTypes.size() > 0){
            String procedureTypes ='';
            for(String pType: searchPattern.procedureTypes){
                if (procedureTypes != '') procedureTypes += ',';
                procedureTypes += '\'' + pType + '\'';
            }
            if (result != '') result += ' AND ';
            result += 'RecordType.Name IN ('+ procedureTypes + ')';
        }

        return result;
    }
    
    public static String getDateFormat(Date pDate){
        String month = pDate.month() > 9 ? String.valueOf(pDate.month()):'0'+String.valueOf(pDate.month());
        String day = pDate.day() > 9 ? String.valueOf(pDate.day()):'0'+String.valueOf(pDate.day());
        
        //DateTime DT = DateTime.newInstance(pDate.year() , month, day);

        return pDate.year() + '-' + month + '-' + day;
    }

    public static String getOrientationOrder(dcSearchProcedureToBatch searchPattern){
        return (searchPattern.isDescending == true)? 'DESC': 'ASC';
    }

    public static String getFullName(Procedure__c p){
       return (String.isEmpty(p.Customer__r.Middle_Name__c ) ?
               p.Customer__r.FirstName + ' ' + p.Customer__r.LastName + ' ' + p.Customer__r.Second_Last_Name__c :
               p.Customer__r.FirstName + ' ' + p.Customer__r.Middle_Name__c + ' ' + p.Customer__r.LastName + ' ' + p.Customer__r.Second_Last_Name__c);
    }

    public static void checkAccessible(){
        if(!(Schema.sObjectType.Contact.isAccessible() &&
             Schema.sObjectType.Contact.fields.Id.isAccessible() &&
             Schema.sObjectType.Contact.fields.Full_Name__c.isAccessible() &&
             /*Schema.sObjectType.Contact.fields.FirstName.isAccessible() &&
             Schema.sObjectType.Contact.fields.Middle_Name__c.isAccessible() &&
             Schema.sObjectType.Contact.fields.LastName.isAccessible() &&
             Schema.sObjectType.Contact.fields.Second_Last_Name__c.isAccessible() &&
             Schema.sObjectType.Contact.fields.Gender__c.isAccessible() &&
             Schema.sObjectType.Contact.fields.Birthdate.isAccessible() &&*/
             Schema.sObjectType.Account.isAccessible() &&
             Schema.sObjectType.Account.fields.Id.isAccessible() &&
             Schema.sObjectType.Account.fields.Name.isAccessible() &&
             Schema.sObjectType.Procedure__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Customer__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Name.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.CreatedDate.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.RecordTypeId.isAccessible() &&
             Schema.sObjectType.RecordType.isAccessible() &&
             Schema.sObjectType.RecordType.fields.Name.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Status__c.isAccessible())) { 
            DCException.throwPermiException('dcSearchProcedureToBatchUtility.checkAccessible');
        }
    }
}