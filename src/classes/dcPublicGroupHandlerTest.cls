/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 10-29-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   10-28-2020   Elizabeth Betancourt Herrera   Initial Version
**/
@isTest
public with sharing class dcPublicGroupHandlerTest {
    @TestSetup
    static void makeData(){
        //create data to insert
        Account acc = new Account(Active__c = 'Yes',Name = 'District Cuba',Phone='2023211169',Type='Mayorista',Sigla__c = 'DCW');
        insert acc;

        user userAux = new user(FirstName = 'sky test', 
                                LastName = 'dc',
                                Username ='dcskytest@theskyplanner.com',
                                Email = 'dcskytest@theskyplanner.com',
                                Alias = 'sky',
                                TimeZoneSidKey='America/Los_Angeles',
                                LocaleSidKey='en_US',
                                EmailEncodingKey='ISO-8859-1',
                                ProfileId=userinfo.getProfileId(),
                                LanguageLocaleKey='en_US',
                                IsActive = false);
        insert userAux;
        User_Agency_RelationShip__c employee = new User_Agency_RelationShip__c(User__c=userAux.Id, Agency__c=acc.Id);
        insert employee;
    }
    
    @isTest
    static void testPublicGroup(){
        test.startTest();

        Account acc = [SELECT Id, PublicGroupId__c FROM Account Limit 1];
        List<GroupMember> groupMemberList=[SELECT Id FROM GroupMember WHERE GroupId =:acc.PublicGroupId__c ];
        System.assert(groupMemberList.size()==0, 'El grupo no debe tener miembros todavia');
        
        dcPublicGroupUserHandler.updateUserPublicGroup(new List<String>{acc.Id});  
        groupMemberList=[SELECT Id FROM GroupMember WHERE GroupId =:acc.PublicGroupId__c ];
        System.assert(groupMemberList.size()==1, 'El user no fue includido como miembro del grupo.');
        
        test.stopTest();
    }   
}