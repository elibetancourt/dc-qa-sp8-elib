/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-02-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   10-29-2020   Elizabeth Betancourt Herrera   Initial Version
**/
@isTest
public with sharing class dcAccountTriggerHandlerTest {
    /**
    * Testing all before insert trigger 
    **/
    @isTest
    static void accountCreateTest() {
        Test.startTest();
        
        List<Account> accList = new list<Account>();
        //create data to insert
        Account accMay = new Account(Active__c = 'Yes',
                                     Name = 'TestMayorista',
                                     Phone='2023211169',
                                     Type='Mayorista',
                                     Sigla__c = 'DCW');
        insert accMay;
        accMay = [SELECT Id, PublicGroupId__c FROM Account WHERE Name like 'TestMayorista' LIMIT 1];
        System.assert(String.isNotEmpty(accMay.PublicGroupId__c ), 'El grupo de agencia mayorista no fue registrado.');
        
        Account accMin = new Account(Active__c = 'Yes',
                                     Name = 'TestMinorista1',
                                     Phone='32023211369',
                                     Type='Minorista',
                                     Sigla__c = 'TM1',
                                     Distributor__c=accMay.Id);
        accList.add(accMin);

        accMin = new Account(Active__c = 'Yes',
                             Name = 'TestMinorista2',
                             Phone='1202321111',
                             Type='Minorista',
                             Sigla__c = 'TM2',
                             Distributor__c=accMay.Id);
                             accList.add(accMin);

        accMin = new Account(Active__c = 'Yes',
                             Name = 'TestMinorista3',
                             Phone='22023211269',
                             Type='Minorista',
                             Sigla__c = 'TM3',
                             Distributor__c=accMay.Id);
                             accList.add(accMin);

        insert accList;

        accList = [SELECT Id, PublicGroupId__c FROM Account WHERE Distributor__c <> null AND  PublicGroupId__c <> null ORDER BY Id];
        System.assert(accList.size()==3, 'El grupo de las cuentas no fue registrado.');
        
        List<AccountShare> sharedRecords = [SELECT Id FROM AccountShare WHERE UserOrGroupId=:accMay.PublicGroupId__c];
        System.assert(sharedRecords.size()==4, 'Las cuentas minoristas no fueron compartidas con la mayorista.');
        
        List<String> groupList = new List<String>();
        for(account accShared: accList){
            groupList.add(accShared.PublicGroupId__c);
        }
        sharedRecords = [SELECT Id, AccountId FROM AccountShare WHERE UserOrGroupId IN: groupList ORDER BY AccountId];
        System.assert(sharedRecords.size()==3, 'Las cuentas minorista no fueron compartidas.');

        for(Integer i=0; i<sharedRecords.size() ;i++)    
        {
            System.assert(sharedRecords[i].AccountId==accList[i].Id, 'La cuenta minorista no fue compartida con su grupo.');
        }    

        Test.stopTest();
    }
}