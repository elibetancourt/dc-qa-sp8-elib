/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 10-19-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   10-19-2020   Elizabeth Betancourt Herrera   Initial Version
**/
public without sharing class dcAddProcedureToBatchController {
    public static dcSearchProcedureToBatch decodeJson(String searchPattern){
        dcSearchProcedureToBatch objConverted = (dcSearchProcedureToBatch) JSON.deserialize(searchPattern, dcSearchProcedureToBatch.class);
        return objConverted;
    }

    @AuraEnabled
    public static dcSearchProcedureToBatch initData(String searchPattern){
        //System.debug('criterios-->'+searchPattern);
        dcSearchProcedureToBatch searchContact = new dcSearchProcedureToBatch(decodeJson(searchPattern));
        return searchContact;
    }

    @AuraEnabled
    public static dcSearchProcedureResult count(String searchPattern){
        dcSearchProcedureToBatch searchContact = new dcSearchProcedureToBatch(decodeJson(searchPattern));
        return dcSearchProcedureToBatchUtility.count(searchContact);
    }

    @AuraEnabled
    public static dcSearchProcedureResult search(String searchPattern){
        System.debug(searchPattern);
        dcSearchProcedureToBatch searchContact = new dcSearchProcedureToBatch(decodeJson(searchPattern));
        return dcSearchProcedureToBatchUtility.search(searchContact);
    }

    @AuraEnabled
    public static dcSearchProcedureResult reset(String searchPattern){
        dcSearchProcedureToBatch searchContact = new dcSearchProcedureToBatch(decodeJson(searchPattern));
        return dcSearchProcedureToBatchUtility.reset(searchContact);
    }
}