/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-04-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   09-22-2020   Elizabeth Betancourt Herrera   Initial Version
**/
global without sharing class dcPaymentsTriggerHandler {
    Boolean isBefore;
    Boolean isAfter;
    Boolean isInsert;
    Boolean isDelete;
    Boolean isUpdate;
    List<Payments__c> newList {get; set;}
    List<Payments__c> oldList {get; set;}
    Map<Id, Payments__c> oldMap {get; set;}
    
    public dcPaymentsTriggerHandler() {
        newList = trigger.new;
        oldList = trigger.old;
        oldMap = (Map<Id, Payments__c>)trigger.oldMap;
        this.isBefore = trigger.isBefore;
        this.isAfter = trigger.isAfter;
        this.isInsert = trigger.isInsert;
        this.isUpdate = trigger.isUpdate;
        this.isDelete = trigger.isDelete;
    }

    public void updateTotalPayments (){
        //procedure payments
        Map<Id, Id> procedureIdToUpdate = getProcedureToUpdate();                                      
        updateProcedures(procedureIdToUpdate);    
        
        //batch payments
        Map<Id, Id> batchIdToUpdate = getBatchToUpdate();                                      
        updateBatches(batchIdToUpdate);  
    }

    private Map<Id, Id> getProcedureToUpdate (){
        Map<Id, Id> procedureIdToUpdate = new Map<Id,Id>();

        if (newList == null)    {
            for(Payments__c payment: oldMap.values()){
                if(payment.Procedure__c != null &&
                   !procedureIdToUpdate.containsKey(payment.Procedure__c)){
                    procedureIdToUpdate.put(payment.Procedure__c, payment.Procedure__c);
                }
            }
        } 
        else {
            for(Payments__c payment: newList){
                if(payment.Procedure__c != null &&
                   (oldMap == null ||
                   payment.Procedure__c != oldMap.get(payment.Id).Procedure__c ||
                   payment.Amount__c != oldMap.get(payment.Id).Amount__c) &&
                   !procedureIdToUpdate.containsKey(payment.Procedure__c)){
                    procedureIdToUpdate.put(payment.Procedure__c, payment.Procedure__c);
                }
            }
        }
        return procedureIdToUpdate;
    }

    private void updateProcedures(Map<Id, Id> procedureIdToUpdate){
        
        if( procedureIdToUpdate.size() > 0){
            
            /*if(!(Schema.sObjectType.Payments__c.isAccessible() &&
                Schema.sObjectType.Payments__c.fields.Id.isAccessible() &&
                Schema.sObjectType.Payments__c.fields.Amount__c.isAccessible() &&
                Schema.sObjectType.Payments__c.fields.Procedure__c.isAccessible() &&
                Schema.sObjectType.Procedure__c.isAccessible() &&
                Schema.sObjectType.Procedure__c.fields.Amount_Paid__c.isAccessible() &&
                Schema.sObjectType.Procedure__c.fields.Id.isAccessible())) {
                DCException.throwPermiException('dcPaymentsTriggerHandler.updateProcedures');
            }*/

            List<Procedure__c> procListToUpdate = [SELECT Amount_Paid__c
                                                   FROM Procedure__c 
                                                   WHERE Id in: procedureIdToUpdate.values() Order By Id];

            AggregateResult[] rollUpSummaries= [SELECT Procedure__c, SUM(Amount__c) totalPaid
                                                FROM Payments__c  
                                                WHERE Procedure__c in: procedureIdToUpdate.values() 
                                                GROUP BY Procedure__c Order By Procedure__c];
            Integer j=0;
            for(Integer i=0; i < procListToUpdate.size(); i++){
                System.debug(procListToUpdate[i].Id);
                if (j < rollUpSummaries.size() && procListToUpdate[i].Id == (string)rollUpSummaries[j].get('Procedure__c')){
                   System.debug(rollUpSummaries[j].get('Procedure__c'));
                   System.debug(rollUpSummaries[j].get('totalPaid'));
                   procListToUpdate[i].Amount_Paid__c = (decimal)rollUpSummaries[j].get('totalPaid');
                   j++;
                }
                else{
                    procListToUpdate[i].Amount_Paid__c = 0;
                }
            }

            procListToUpdate.sort(); //Sorting by Sobject Type to prevent Error: "System.TypeException: Cannot have more than 10 chunks in a single operation."
            update procListToUpdate;
            /*try {
                CRUDEnforce.dmlUpdate(procListToUpdate, 'dcPaymentsTriggerHandler.updateProcedures');
            } catch (DmlException e) {
                DCException.throwException(e.getMessage());
            } */
        }
    }

    private Map<Id, Id> getBatchToUpdate (){
        Map<Id, Id> batchIdToUpdate = new Map<Id,Id>();

        if (newList == null)    {
            for(Payments__c payment: oldMap.values()){
                if(payment.Batch__c != null &&
                   !batchIdToUpdate.containsKey(payment.Batch__c)){
                    batchIdToUpdate.put(payment.Batch__c, payment.Batch__c);
                }
            }
        } 
        else {
            for(Payments__c payment: newList){
                if(payment.Batch__c != null &&
                   (oldMap == null ||
                   payment.Batch__c != oldMap.get(payment.Id).Batch__c ||
                   payment.Amount__c != oldMap.get(payment.Id).Amount__c) &&
                   !batchIdToUpdate.containsKey(payment.Batch__c)){
                    batchIdToUpdate.put(payment.Batch__c, payment.Batch__c);
                }
            }
        }
        return batchIdToUpdate;
    }

    private void updateBatches(Map<Id, Id> batchIdToUpdate){
        
        if( batchIdToUpdate.size() > 0){
            
            if(!(Schema.sObjectType.Payments__c.isAccessible() &&
                Schema.sObjectType.Payments__c.fields.Id.isAccessible() &&
                Schema.sObjectType.Payments__c.fields.Amount__c.isAccessible() &&
                Schema.sObjectType.Payments__c.fields.Batch__c.isAccessible() &&
                Schema.sObjectType.Batch__c.isAccessible() &&
                Schema.sObjectType.Batch__c.fields.Amount_Paid__c.isAccessible() &&
                Schema.sObjectType.Batch__c.fields.Id.isAccessible())) {
                DCException.throwPermiException('dcPaymentsTriggerHandler.updateBatches');
            }

            List<Batch__c> batchListToUpdate = [SELECT Id, Amount_Paid__c
                                                FROM Batch__c 
                                                WHERE Id in: batchIdToUpdate.values() Order By Id];

            AggregateResult[] rollUpSummaries= [SELECT Batch__c, SUM(Amount__c) totalPaid
                                                FROM Payments__c  
                                                WHERE Batch__c in: batchIdToUpdate.values() 
                                                GROUP BY Batch__c Order By Batch__c];
            Integer j=0;
            for(Integer i=0; i < batchIdToUpdate.size(); i++){
                System.debug(batchListToUpdate[i].Id);
                if (j < rollUpSummaries.size() && batchListToUpdate[i].Id == (string)rollUpSummaries[j].get('Batch__c')){
                   System.debug(rollUpSummaries[j].get('Batch__c'));
                   System.debug(rollUpSummaries[j].get('totalPaid'));
                   batchListToUpdate[i].Amount_Paid__c = (decimal)rollUpSummaries[j].get('totalPaid');
                   j++;
                }
                else{
                    batchListToUpdate[i].Amount_Paid__c = 0;
                }
            }

            batchListToUpdate.sort(); //Sorting by Sobject Type to prevent Error: "System.TypeException: Cannot have more than 10 chunks in a single operation."
            try {
                CRUDEnforce.dmlUpdate(batchListToUpdate, 'dcPaymentsTriggerHandler.updateBatches');
            } catch (DmlException e) {
                DCException.throwException(e.getMessage());
            } 
        }
    }

    public void PaymentTriggerActions (){
        
        if(isAfter && isInsert) {
            //share account whith user in public group
            shareRecord();
        }   
        updateTotalPayments();                        
    }

    public void shareRecord (){
        
       Map<String, List<sObject>> groupPaymentsList = getGroupListToShare();
       System.debug('Lista pagos a compartir');
       for(String groupId: groupPaymentsList.keySet()){
           System.debug('groupId --> '+groupId) ;
           System.debug('total a compratir --> '+groupPaymentsList.get(groupId).size()) ;
           dcSharingManager.shareRecords(groupPaymentsList.get(groupId), 
                                         'Payments__Share', 'Edit', groupId, 'Manual');
       }
    }

    public Map<String, List<sObject>> getGroupListToShare (){
        
            Map<String, String> agencyGroupMap;

            System.debug('list size --> '+newList.size()) ;
            if(newList.size() == 1){
                agencyGroupMap = dcPublicGroupUtils.getAgencyGroupMap(newList[0].Agencia__c);
            } else {
                agencyGroupMap = dcPublicGroupUtils.getAgencyGroupMap('');
            }
    
            Map<String, List<sObject>> groupPaymentList = new Map<String, List<sObject>>();
            
            for( Payments__c newPayment: newList){
                List<sObject> paymentToShareList = new List<sObject>();
                if(agencyGroupMap != null && agencyGroupMap.containsKey(newPayment.Agencia__c)){
                    String groupId = agencyGroupMap.get(newPayment.Agencia__c);
                    if (groupPaymentList.containsKey(groupId)) {
                        paymentToShareList = groupPaymentList.get(groupId); 
                    }
                    paymentToShareList.add(newPayment);
                    groupPaymentList.put(groupId, paymentToShareList);
                    
                }   
            }

            /* comprobar si la agencia mayorista ya esta incluida en el mapa*/
            
            String groupIdAgencyMayorista = dcPublicGroupUtils.getGroupAgencyMayorista();
            
            if(String.isNotEmpty(groupIdAgencyMayorista)){
                 groupPaymentList.put(groupIdAgencyMayorista, newList);
            }
            
            return groupPaymentList;
        
    }  
}