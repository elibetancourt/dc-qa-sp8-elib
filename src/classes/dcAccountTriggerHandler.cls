/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-02-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   10-29-2020   Elizabeth Betancourt Herrera   Initial Version
**/
public without sharing class dcAccountTriggerHandler {
    
    Boolean isBefore;
    Boolean isAfter;
    Boolean isInsert;
    Boolean isDelete;
    Boolean isUpdate;
    List<Account> newList {get; set;}
    List<Account> oldList {get; set;}
    Map<Id, Account> oldMap {get; set;}

    public dcAccountTriggerHandler() {
        
        newList = trigger.new;
        oldList = trigger.old;
        oldMap = (Map<Id, Account>)trigger.oldMap;
        this.isBefore = trigger.isBefore;
        this.isAfter = trigger.isAfter;
        this.isInsert = trigger.isInsert;
        this.isUpdate = trigger.isUpdate;
        this.isDelete = trigger.isDelete;
    }

    public void accountTriggerActions (){
        
        if (isBefore && isInsert){
            createPublicGroup();   
        }     
        else if(isAfter && isInsert) {
            //share account whith user in public group
            shareRecord();
        }                           
    }

    public void createPublicGroup (){
        List<string> nameList = new List<string>();
        for (account acc: newList){
            nameList.add(acc.Name);
        }
        Map<String, String> groupIdMap = dcPublicGroupHandler.createPublicGroupWithMap(nameList);
        
        for (account acc: (List<Account>)trigger.new){
            acc.PublicGroupId__c = groupIdMap.get(acc.Name);
        }
    }

    public void shareRecord (){
        
       Map<String, List<sObject>> groupAccountList = getGroupListToShare();
       System.debug('Lista a compartir') ;
       for(String groupId: groupAccountList.keySet()){
           System.debug('groupId --> '+groupId) ;
           System.debug('total a compratir --> '+groupAccountList.get(groupId).size()) ;
           dcSharingManager.shareRecords(groupAccountList.get(groupId), 
                                         'AccountShare', 'Edit', groupId, 'Manual');
       }
    }

    public Map<String, List<sObject>> getGroupListToShare (){
        
        Map<String, String> agencyGroupMap = dcPublicGroupUtils.getAgencyGroupMap('');
       
        Map<String, List<sObject>> groupAccountList = new Map<String, List<sObject>>();
        
        for( account newAccount: newList){
            List<sObject> accToShareList = new List<sObject>{newAccount};
            if(agencyGroupMap.containsKey(newAccount.Id)){
                String groupId = agencyGroupMap.get(newAccount.Id);
                groupAccountList.put(groupId, accToShareList);
            }
            /* buscar la cuenta de mayorista */
            if (newAccount.Distributor__c != null){
                if(agencyGroupMap.containsKey(newAccount.Distributor__c)){
                    String groupId = agencyGroupMap.get(newAccount.Distributor__c);
                    List<sObject> accToShareWithMayoristaList;
                    if (groupAccountList.containsKey(groupId)){
                        accToShareWithMayoristaList = groupAccountList.get(groupId);
                        accToShareWithMayoristaList.add(newAccount);
                    }
                    else{
                        accToShareWithMayoristaList = new List<sObject>{newAccount};
                    }
                    groupAccountList.put(groupId, accToShareWithMayoristaList); 
                }
            }
        }

        return groupAccountList;
    }
}