/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-02-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   11-02-2020   Elizabeth Betancourt Herrera   Initial Version
**/
@isTest
public with sharing class dcNumberXmlTriggerHandlerTest {
    /**
    * Testing all after insert trigger
    **/
    @isTest
    static void numberXmlCreateTest() {
        Test.startTest();
        
        Number_for_XML_File__c numberXml = new Number_for_XML_File__c(Manifesto__c='M0020');
        insert numberXml;

        List<Number_for_XML_File__Share> sharedRecords = [SELECT Id, ParentId, UserOrGroupId FROM Number_for_XML_File__Share WHERE parentId=:numberXml.Id];
        System.debug(sharedRecords.size());
        System.assert(sharedRecords.size()>0, 'El numero para XML File no fue compartido con el grupo de la agencia.');
   
        Test.stopTest();
    }    
}