/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-03-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   09-18-2020   Elizabeth Betancourt Herrera   Initial Version
**/
@isTest
public with sharing class dcTransactionTriggerHandlerTest {
    public dcTransactionTriggerHandlerTest() {

    }

    @TestSetup
    static void makeData(){
        
        Account acc = new Account(Active__c = 'Yes',Name = 'District Cuba',Phone='2023211169',Type='Mayorista',Sigla__c = 'DCW');
        insert acc;
                
        Contact c = new Contact(FirstName='Luis',LastName='Suarez',AccountId=acc.Id,Eyes_Color__c='Negros');
        insert c;
        
        Date d = date.today();
        //two records for each RecordType, one complete and one incomplete
        
        //pasaporte 1ra vez incompleto
        Id pas1raVezRT =  DCUtils.getRecTypeIdByDevName('Procedure__c', 'Pasaporte_1ra_Vez');
        Procedure__c p = new Procedure__c(Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId=pas1raVezRT,
                                          First_Name__c='Luis', Last_Name__c='Suarez', Street__c='calle 10 entre 1ra y 3ra',
                                          Eyes_Color__c='Negros', Consulate_Payment__c = 375);
        insert p;
       

    }

    @isTest
    static void updateProcedureEscrowAmountTest(){

        Procedure__c proc = [SELECT Id, Total_Payments_To_Escrow_Account__c FROM Procedure__c LIMIT 1]; 
        Account acc = [SELECT Id FROM Account LIMIT 1];
        /*  al introducir un pago el sistema crea una transaccion escrow que dispara 
        **  el trigger del objeto transaction__c*/
        Transaction__c escrowTransaction = new Transaction__c(Agency__c = acc.Id,
                                                                         GL_Account__c = 'Escrow',
                                                                         Description__c = 'Transacción TEST.',
                                                                         Date__c = Datetime.now(),
                                                                         Monto__c = 250,
                                                                         Procedure__c = proc.Id);
        
        Test.startTest();
        // valor incial antes de insertar transaccion
        System.assert( proc.Total_Payments_To_Escrow_Account__c == null ||
                       proc.Total_Payments_To_Escrow_Account__c == 0, 
                       'Total_Payments_To_Escrow_Account__c valor inicial inesperado');
        insert escrowTransaction;
        proc = [SELECT Id, Total_Payments_To_Escrow_Account__c FROM Procedure__c LIMIT 1]; 
        System.assert(  proc.Total_Payments_To_Escrow_Account__c == 250, 
                       'Total_Payments_To_Escrow_Account__c no fue actualizado');
        
        /*verificar sharing manual */
        String gId = dcPublicGroupUtils.getGroupAgencyMayorista();
        List<Transaction__Share> sharedRecords = [SELECT Id, ParentId, UserOrGroupId 
                                                  FROM Transaction__Share 
                                                  WHERE UserOrGroupId=:gId];
        System.debug(sharedRecords.size());
        System.assert(sharedRecords.size()>0, 'los tracking no fueron compartidos con el grupo de la agencia mayorista.');

        /*  al actualizar el monto del pago se actualiza el monto de la transaccion escrow 
        **  que dispara el trigger del objeto transaction__c  */
        escrowTransaction.Monto__c = 200;
        update escrowTransaction;
        proc = [SELECT Id, Total_Payments_To_Escrow_Account__c FROM Procedure__c LIMIT 1]; 
        System.assert(  proc.Total_Payments_To_Escrow_Account__c > 0 &&
                        proc.Total_Payments_To_Escrow_Account__c == 200,
                        'Total_Payments_To_Escrow_Account__c no fue actualizado');
        
        delete escrowTransaction;
        proc = [SELECT Id, Total_Payments_To_Escrow_Account__c FROM Procedure__c LIMIT 1]; 
        System.debug(proc.Total_Payments_To_Escrow_Account__c);
        System.assert(  proc.Total_Payments_To_Escrow_Account__c == 0,
                        'Total_Payments_To_Escrow_Account__c no fue actualizado');
        Test.stopTest();
    }
}