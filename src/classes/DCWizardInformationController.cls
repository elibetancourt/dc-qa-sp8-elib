/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 11-04-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author             Modification
 * 1.0   10-13-2020   Silvia Velazquez   Initial Version
**/
global without sharing class DCWizardInformationController {

    global Date currentDay {get; set;}
    global Date minBirthDate {get; set;}
    global Date maxDepartureDate {get; set;}

    /**
    * @description Constructor
    * @author Silvia Velazquez | 10-21-2020 
    **/
    public DCWizardInformationController(){
        currentDay = Date.today();
        minBirthDate = Date.newInstance(1900, 1, 1);
        maxDepartureDate = Date.today().addDays(-180);
    }

    /**
    * @description Generate or Update Procedure
    * @author Silvia Velazquez | 10-21-2020 
    * @param Procedure__c procedure 
    * @param ValuesToConvertWrapper values 
    * @return Procedure__c 
    **/
    @RemoteAction
    global static Procedure__c generateProcedure(Procedure__c procedure, ValuesToConvertWrapper values){                                           
        DCExpCheckoutProcedureManager manager = new DCExpCheckoutProcedureManager();
        validateNotRequiredData(procedure);
        completeProcedureData(procedure, values);
        return manager.upsertProcedure(procedure, values.procedureParam);
    }

    global static void validateNotRequiredData(Procedure__c procedure){

        procedure.Birth_Province__c = (procedure.Birth_Province__c == DCExpCheckoutConstants.BIRTH_PROVINCE_LABEL)
                                      ? null : procedure.Birth_Province__c;

        procedure.Birth_Municipality_City__c = (procedure.Birth_Municipality_City__c == DCExpCheckoutConstants.BIRTH_CITY_LABEL)
                                                ? null : procedure.Birth_Municipality_City__c;

        procedure.Birth_Country__c = (procedure.Birth_Country__c == DCExpCheckoutConstants.BIRTH_COUNTRY_LABEL)
                                      ? 'Cuba' : procedure.Birth_Country__c;

        procedure.Nivel_de_Escolaridad__c = (procedure.Nivel_de_Escolaridad__c == DCExpCheckoutConstants.EDUCATION_LABEL)
                                            ? null : procedure.Nivel_de_Escolaridad__c;

        procedure.References_Province__c = (procedure.References_Province__c == DCExpCheckoutConstants.REF_PROVINCE_LABEL)
                                            ? null : procedure.References_Province__c;

        procedure.References_Municipality__c = (procedure.References_Municipality__c == DCExpCheckoutConstants.REF_CITY_LABEL)
                                                ? null : procedure.References_Municipality__c;

        procedure.Residence_Address_2_Province__c = (procedure.Residence_Address_2_Province__c == DCExpCheckoutConstants.PENULT_PROVINCE_LABEL)
                                      ? null : procedure.Residence_Address_2_Province__c;

        procedure.Residence_Address_2_City__c = (procedure.Residence_Address_2_City__c == DCExpCheckoutConstants.PENULT_CITY_LABEL)
                                      ? null : procedure.Residence_Address_2_City__c;
    }

    global static void completeProcedureData(Procedure__c procedure, ValuesToConvertWrapper values){
        procedure.Reference_Phone__c = String.valueOf(values.Reference_Phone);
        procedure.Residence_Year_1_From__c = String.valueOf(values.Residence_Year_1_From);
        procedure.Residence_Year_1_To__c = String.valueOf(values.Residence_Year_1_To);
        procedure.Residence_Year_2_From__c = (values.Residence_Year_2_From !=null) 
                                            ? String.valueOf(values.Residence_Year_2_From) 
                                            : null;
        procedure.Residence_Year_2_To__c = (values.Residence_Year_2_To != null)
                                            ? String.valueOf(values.Residence_Year_2_To)
                                            : null;

        procedure.Birthday__c = DateTime.newInstance(values.Birthday).date();
        procedure.Departure_Date__c = DateTime.newInstance(values.Departure_Date).date();
        procedure.Shipment_Country__c = DCExpCheckoutConstants.SHIP_COUNTRY;   
    }

    global static Date convertToDate(String dateStr){
        //1988-01-01T00:00:00
        dateStr = dateStr.substringBefore('T');
        return Date.valueOf(dateStr);
    }

    @RemoteAction
    global static InformationWrapper getPicklistValues(){
        InformationWrapper result = new InformationWrapper();
       
        result.birthProvinces = new List<String>{DCExpCheckoutConstants.BIRTH_PROVINCE_LABEL};
        result.refProvinces = new List<String>{DCExpCheckoutConstants.REF_PROVINCE_LABEL};
        result.lastProvinces = new List<String>{DCExpCheckoutConstants.LAST_PROVINCE_LABEL};
        result.penultProvinces = new List<String>{DCExpCheckoutConstants.PENULT_PROVINCE_LABEL};

		for(Schema.PicklistEntry pickListVal : Procedure__c.Birth_Province__c.getDescribe().getPicklistValues()){
            result.birthProvinces.add(pickListVal.getLabel());
            result.refProvinces.add(pickListVal.getLabel());
            result.lastProvinces.add(pickListVal.getLabel());
            result.penultProvinces.add(pickListVal.getLabel());
        }

        result.birthCountries = new List<String>();
        result.birthCountries.add(DCExpCheckoutConstants.BIRTH_COUNTRY_LABEL);	
        for(Schema.PicklistEntry pickListVal : Procedure__c.Birth_Country__c.getDescribe().getPicklistValues()){
            result.birthCountries.add(pickListVal.getLabel());
        }

        result.maritalStatus = new List<String>();
        result.maritalStatus.add(DCExpCheckoutConstants.MARITAL_STATUS_LABEL);	
        for(Schema.PicklistEntry pickListVal : Procedure__c.Marital_Status__c.getDescribe().getPicklistValues()){
            result.maritalStatus.add(pickListVal.getLabel());
        }

        result.genders = new List<String>();
        result.genders.add(DCExpCheckoutConstants.GENDER_LABEL);
        for(Schema.PicklistEntry pickListVal : Procedure__c.Gender__c.getDescribe().getPicklistValues()){
            result.genders.add(pickListVal.getLabel());
        }
        
        result.eyesColor = new List<String>();
        result.eyesColor.add(DCExpCheckoutConstants.EYES_COLOR_LABEL);	
        for(Schema.PicklistEntry pickListVal : Procedure__c.Eyes_Color__c.getDescribe().getPicklistValues()){
            result.eyesColor.add(pickListVal.getLabel());
        }

        result.skinColor = new List<String>();
        result.skinColor.add(DCExpCheckoutConstants.SKIN_COLOR_LABEL);
        for(Schema.PicklistEntry pickListVal : Procedure__c.Skin_Color__c.getDescribe().getPicklistValues()){
            result.skinColor.add(pickListVal.getLabel());
        }

        result.hairColor = new List<String>();
        result.hairColor.add(DCExpCheckoutConstants.HAIR_COLOR_LABEL);	
        for(Schema.PicklistEntry pickListVal : Procedure__c.Hair_Color__c.getDescribe().getPicklistValues()){
            result.hairColor.add(pickListVal.getLabel());
        }

        result.migrationStatus = new List<String>();
        result.migrationStatus.add(DCExpCheckoutConstants.MIGRATION_STATUS_LABEL);	
        for(Schema.PicklistEntry pickListVal : Procedure__c.Immigration_Status__c.getDescribe().getPicklistValues()){
            result.migrationStatus.add(pickListVal.getLabel());
        }

        result.educationalLevel = new List<String>();
        result.educationalLevel.add(DCExpCheckoutConstants.EDUCATION_LABEL);	
        for(Schema.PicklistEntry pickListVal : Procedure__c.Nivel_de_Escolaridad__c.getDescribe().getPicklistValues()){
            result.educationalLevel.add(pickListVal.getLabel());
        }

        result.usstates = new List<StateWrapper>();
        StateWrapper defaultValue = new StateWrapper();
        defaultValue.id = '0';
        defaultValue.value = DCExpCheckoutConstants.STATE_LABEL;
        result.usstates.add(defaultValue);

        result.shipstates = new List<StateWrapper>();
        StateWrapper shipValue = new StateWrapper();
        shipValue.id = '0';
        shipValue.value = DCExpCheckoutConstants.SHIP_STATE_LABEL;
        result.shipstates.add(shipValue);
        	
        for(Schema.PicklistEntry pickListVal : Procedure__c.State__c.getDescribe().getPicklistValues()){
            StateWrapper state = new StateWrapper();
            state.id = pickListVal.getValue();
            state.value = pickListVal.getLabel();
            result.usstates.add(state);
            result.shipstates.add(state);
            //pickListVal.getValue() --->API Name
        }

        result.dependentMunicipalities = getDependentPicklistValues();
        
        return result;
	}
	
	global static List<ProvincesMapWrapper> getDependentPicklistValues(){
        Map<Object,List<String>> dependentPicklistValues = new Map<Object,List<String>>();
        //Get dependent field result
        Schema.DescribeFieldResult dependentFieldResult = Procedure__c.Birth_Municipality_City__c.getDescribe();
        //Get dependent field controlling field 
        Schema.sObjectField controllerField = dependentFieldResult.getController();
        //Check controlling field is not null
        if(controllerField == null){
            return null;
        } 
        //Get controlling field result
        Schema.DescribeFieldResult controllerFieldResult = controllerField.getDescribe();
        //Get controlling field picklist values if controlling field is not a checkbox
        List<Schema.PicklistEntry> controllerValues = (controllerFieldResult.getType() == Schema.DisplayType.Boolean ? null : controllerFieldResult.getPicklistValues());
        
        //It is used to decode the characters of the validFor fields. 
        String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        
        for (Schema.PicklistEntry entry : dependentFieldResult.getPicklistValues()){
            if (entry.isActive()){
            //The PicklistEntry is serialized and deserialized using the Apex JSON class and it will check to have a 'validFor' field
                List<String> base64chars = String.valueOf(((Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')).split('');
                for (Integer i = 0; i < controllerValues.size(); i++){
                    Object controllerValue = (controllerValues == null ? (Object) (i == 1) : (Object) (controllerValues[i].isActive() ? controllerValues[i].getLabel() : null));
                    Integer bitIndex = i / 6;
                    Integer bitShift = 5 - Math.mod(i, 6 );
                    if(controllerValue == null || (base64map.indexOf(base64chars[bitIndex]) & (1 << bitShift)) == 0){
                        continue;
                    } 
                    if(!dependentPicklistValues.containsKey(controllerValue)){
                        dependentPicklistValues.put(controllerValue, new List<String>());
                    }
                    dependentPicklistValues.get(controllerValue).add(entry.getLabel());
                }
            }
        }
        List<ProvincesMapWrapper> result = new List<ProvincesMapWrapper>();
        
        for(Object key: dependentPicklistValues.keySet()){
            ProvincesMapWrapper item = new ProvincesMapWrapper();
            item.province = key;
            item.municipalities = dependentPicklistValues.get(key);
            result.add(item);
        }
        return result;
	}
	
	global class InformationWrapper{
        public List<String> birthProvinces;
        public List<String> birthCountries;
        public List<String> refProvinces;
        public List<String> lastProvinces;
        public List<String> penultProvinces;
        public List<String> maritalStatus;
        public List<String> genders;
        public List<String> eyesColor;
        public List<String> skinColor;
        public List<String> hairColor;
        public List<String> migrationStatus;
        public List<String> educationalLevel;
        public List<StateWrapper> usstates;
        public List<StateWrapper> shipstates;
        public List<ProvincesMapWrapper> dependentMunicipalities;
    }

    global class ProvincesMapWrapper{
        public Object province;
        public List<String> municipalities;
    }

    global class StateWrapper{
        public String id;
        public String value;
    }

    global class ValuesToConvertWrapper{
        public Long Phone;
        public Long Reference_Phone;
        public Long PostalCode;
        public Integer Residence_Year_1_From;
        public Integer Residence_Year_1_To;
        public Integer Residence_Year_2_From;
        public Integer Residence_Year_2_To;
        public Long Birthday;
        public Long Departure_Date;
        public String procedureType;
        public String procedureParam;
    }
}