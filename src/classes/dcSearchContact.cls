/**
 * @File Name          : dcSearchContact.cls
 * @Description        : 
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 30/7/2020 6:33:58 p. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    27/7/2020   Silvia Velazquez     Initial Version
**/
public with sharing class dcSearchContact {
    @AuraEnabled
    public String firstName {get; set;}
    @AuraEnabled
    public String middleName {get; set;}
    @AuraEnabled
    public String lastName {get; set;}
    @AuraEnabled
    public String secondLastName {get; set;}
    @AuraEnabled
    public Date birthdate {get; set;}
    
    @AuraEnabled
    public Integer pageSize {get; set;}
    @AuraEnabled
    public Integer offset {get; set;}
    @AuraEnabled
    public String sortBy {get; set;}
    @AuraEnabled
    public Boolean isDescending {get; set;}
    
    public dcSearchContact() {
        firstName = '';
        middleName = '';
        lastName = '';
        secondLastName = '';
        birthdate = Date.today();
        pageSize = 10;
        offset = 0;
        sortBy = 'Name, CreatedDate';
        isDescending = true;
    }

    public dcSearchContact(dcSearchContact searchContact){
        firstName = searchContact.firstName;
        middleName = searchContact.middleName;
        lastName = searchContact.lastName;
        secondLastName = searchContact.secondLastName;
        birthdate = searchContact.birthdate;
        pageSize = searchContact.pageSize;
        offset = searchContact.offset;
        sortBy = searchContact.sortBy;
        isDescending = searchContact.isDescending;
    }
}