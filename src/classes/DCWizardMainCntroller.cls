/**
 * @description       : 
 * @author            : Silvia Velazquez
 * @group             : 
 * @last modified on  : 10-29-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author             Modification
 * 1.0   10-08-2020   Silvia Velazquez   Initial Version
**/
global without sharing class DCWizardMainCntroller {	
	/**
	* @description 
	* @author Silvia Velazquez | 10-08-2020 
	* @param Id procedureId
	* @return Procedure__c 
	**/
	@RemoteAction
	global static Procedure__c getProcedure(String procedureId) {	
		DCExpCheckoutProcedureManager manager = new DCExpCheckoutProcedureManager();
		return manager.getProcedure(procedureId);
    }

    /**
    * @description Load Procedure given an Id and load piklist values.
    * @author Silvia Velazquez | 10-27-2020 
    * @param String id 
    * @return WizardInfoWrapper 
    **/
    @RemoteAction 
    global static WizardInfoWrapper loadWizardData(String id){
        WizardInfoWrapper data = new WizardInfoWrapper();
        data.picklistValues = DCWizardInformationController.getPicklistValues();
        data.procedure = getProcedure(id);

        if(data.procedure == null){
            DCException.throwException('Error al recuperar la Informacion del Trámite.');
            return null;
        }

        data.numericProcedureType = getNumericProcedureType(data.procedure.RecordType.Name, data.procedure.Type__c);
        return data;
    }

    global static String getNumericProcedureType(String recordTypeName, String extensionType){
        String numericValue = '1';
        for (String keyValue : DCExpCheckoutConstants.typesMap.keySet()) {
            if(recordTypeName == DCExpCheckoutConstants.typesMap.get(keyValue)){
                numericValue = keyValue;
            }
        }

        if(recordTypeName == 'Prorroga' && extensionType == 'Regular'){
            numericValue = DCExpCheckoutConstants.PRORROGA;
        }
        else if(recordTypeName == 'Prorroga' && extensionType == 'Doble'){
            numericValue = DCExpCheckoutConstants.PRORROGA_DOBLE;
        }
        
        return numericValue;
    }
    
    global class WizardInfoWrapper{
        DCWizardInformationController.InformationWrapper picklistValues;
        Procedure__c procedure;
        String numericProcedureType;
    }
}