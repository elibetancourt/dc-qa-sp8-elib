/**
 * @File Name          : dcProcedureDefaultValuesTest.cls
 * @Description        : Test Class for dcProcedureDefaultValues
 * @Author             : Elizabeth Betancourt
 * @Group              : 
 * @Last Modified By   : Ibrahim Napoles
 * @Last Modified On   : 08-26-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    23/7/2020   Elizabeth Betancourt    Initial Version
**/
@isTest
public class dcProcedureDefaultValuesTest {

    static Map<String, String> createDataSetupTest(){
        
        Map<String, String> param = new Map<String, String>();
        Account acc = new Account(Active__c = 'Yes',Name = 'District Cuba',Phone='2023211169',Type='Mayorista',Sigla__c = 'DCW');
        insert acc;
        param.put('accountId',acc.Id);
        
        Contact c = new Contact(FirstName='Luis',LastName='Suarez',AccountId=acc.Id,Eyes_Color__c='Negros');
        insert c;
        param.put('contactId',c.Id);
        
        Delivery_Rates__c tarifa = new Delivery_Rates__c(Active__c=true,Name='FedEx Standard Overnight',Rate__c=35,Description__c='Public rate $99.99 delivery time 9:30 am');
        insert tarifa;
        Procedure_Setup__c configuracion = new Procedure_Setup__c(Procedure_Type__c='Pasaporte 1ra Vez',Sales_Price__c=200.00,Number_of_Packages__c = 1,PackageWeight__c=1,PackageType__c='FedEx Envelope',FedexServiceType__c=tarifa.Id,Agency__c=acc.Id);
        insert configuracion;
        param.put('configId',configuracion.Id);
        
        return param;
    } 
    
    @isTest
    static void testProcedureDefaultValues(){
        //crear records para la configuracion y un tramite
        Map<string,String> params = createDataSetupTest();
        string contactId = params.get('contactId');
        string configId = params.get('configId');
        string contactfields = 
            'Id,AccountId,FirstName,LastName,Middle_Name__c,Second_Last_Name__c,Birthdate,MobilePhone,Email,DNI__c,Mother_Name__c,Father_Name__c,Eyes_Color__c,Hair_Color__c,Skin_Color__c,Height__c,Marital_Status_c__c,Gender__c,Immigration_Status__c,Departure_Date__c,Birth_Country__c,Birth_Province__c,Birth_Municipality_City__c,MailingStreet,MailingCity,MailingPostalCode,MailingStateCode,MailingCountryCode,Work_Name__c,Work_Phone__c,Profession__c,Profession_Category__c,Title,Work_City__c,Work_Email__c,Nivel_de_Escolaridad__c,Reference_Full_Name__c,Reference_Address__c,Reference_Phone__c,Relationship__c,Residence_Address_1__c,Residence_Address_1_City__c,Residence_Year_1_From__c,Residence_Address_1_Province__c,Residence_Year_1_To__c,Residence_Address_2__c,Residence_Address_2_City__c,Residence_Year_2_From__c,Residence_Address_2_Province__c,Residence_Year_2_To__c,Birth_Certificate_Issue_At__c,Birth_Certificate_Issue_Date__c,Reference_District__c,Reference_Phone1__c,Reference_Phone2__c,Category__c,Birth_ProvinceT__c,Birth_Municipality_CityT__c,Reference_Second_Name__c,Reference_Last_Name__c,Reference_Second_Surname__c,References_Province__c,References_Municipality__c,Special_Characteristics__c,Passport_Number__c,Passport_Type__c,Expired_Passport_Issue_Date__c,Passport_Expiration_Date__c,Expired_Passport_Issue_Country__c,Work_State__c,Work_Street__c,Work_Postal_Code__c';
            
        string configfields = 'Sales_Price__c,ConsulatePayment__c,Procedure_Type__c';
        Map<string,String> dvMap = dcProcedureDefaultValues.getDefaults(contactId, configId, contactfields, configfields).defaultMapValues;
        System.Debug(dvMap);                   
        System.Debug(dvMap.get('FirstName'));
        System.assert(dvMap.containsKey('FirstName'), '');
        System.assert(dvMap.get('FirstName')=='Luis', '');
        System.Debug(dvMap.get('LastName'));
        System.assert(dvMap.containsKey('LastName'), '');
        System.assert(dvMap.get('LastName')=='Suarez', '');
        System.Debug(dvMap.get('AccountId'));
        System.assert(dvMap.containsKey('AccountId'), '');
        System.assert(dvMap.get('AccountId')==params.get('accountId'), '');
        System.Debug(dvMap.get('Procedure_Type__c'));
        System.assert(dvMap.containsKey('Procedure_Type__c'), '');
        System.assert(dvMap.get('Procedure_Type__c')=='Pasaporte 1ra Vez', '');
        System.Debug(dvMap.get('Sales_Price__c'));
        System.assert(dvMap.containsKey('Sales_Price__c'), '');
        System.assert(double.valueOf(dvMap.get('Sales_Price__c'))==double.valueOf('200.00'), '');
        
    }
}