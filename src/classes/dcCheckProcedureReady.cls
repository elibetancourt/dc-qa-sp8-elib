/**
 * @File Name          : dcCheckProcedureReady.cls
 * @Description        : Class with invocable method to check if required fields have values
 * @Author             : Elizabeth Betancourt Herrera
 * @Group              :
 * @Last Modified By   : Elizabeth Betancourt Herrera
 * @Last Modified On   : 10-19-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    17/8/2020   Elizabeth Betancourt Herrera     Initial Version
 * 1.1   08-21-2020   Ibrahim Napoles          Validate CRUD permission before SOQL/DML operation.
 * 1.2   08-28-2020   Ibrahim Napoles          Escape query string.
 **/
public without sharing class dcCheckProcedureReady {

    @InvocableMethod(label='Check Required' description='Returns a string of required empty fields separated by commas. ')
    public static List<String> RequiredEmptyFields(List<String> idProcedures) {
        if(!(Schema.sObjectType.Procedure__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.RecordTypeId.isAccessible() &&
             Schema.sObjectType.RecordType.isAccessible() &&
             Schema.sObjectType.RecordType.fields.Name.isAccessible())) {
            DCException.throwPermiException('dcCheckProcedureReady.RequiredEmptyFields');
        }

        List<String> fields= new List<String> {''};
        /*   find the list of requiered fields for the corresponding record type  */
        if (idProcedures.size() > 0) {
            string idProcedure = idProcedures[0];
            List<Procedure__c> procList = [SELECT RecordType.Name FROM Procedure__c WHERE Id= :idProcedure];
            Map<String, String> fieldMap = getMapFields(procList[0].RecordType.Name);

            String fieldStr = getFieldStr(fieldMap.values());
            System.debug(fieldStr);
            if ( String.isEmpty(fieldStr) ) return fields;

            //check accesibility (cambiar en la org de empaquetado)
            //CRUDEnforce.dmlAccessible('inmix__Procedure__c', putNamespace(fieldMap.values()), 'dcCheckProcedureReady.RequiredEmptyFields');
            CRUDEnforce.dmlAccessible('Procedure__c', fieldMap.values(), 'dcCheckProcedureReady.RequiredEmptyFields');

            String query =  generateQuery(fieldStr,'Procedure__c',idProcedure);
            System.debug(query);

            procList = database.query(query);

            Map<string, Object> dvMapObject = procList[0].getPopulatedFieldsAsMap();

            String emptyFieldsStr = '';
            String field, field1;
            for (string label: fieldMap.keySet()) {
                field = fieldMap.get(label);
                field1 = 'inmix__' + field;
                if ( (!dvMapObject.containsKey(field) && !dvMapObject.containsKey(field1)) || 
                     ( String.isEmpty(String.valueOf(dvMapObject.get(field))) &&
                       String.isEmpty(String.valueOf(dvMapObject.get(field1))) ) ) {
                    emptyFieldsStr = (String.isEmpty(emptyFieldsStr) ? label : emptyFieldsStr + ', ' + label);
                }
            }
            fields[0] = emptyFieldsStr;
            System.debug(emptyFieldsStr);
            //guardar resultado
            if (emptyFieldsStr.length() > 255) emptyFieldsStr = emptyFieldsStr.substring(0, 254);
            procList[0].EmptyRequiredValues__c = emptyFieldsStr;
            //update procList[0];
            CRUDEnforce.dmlUpdate(new List<Procedure__c>{procList[0]}, 'dcCheckProcedureReady.RequiredEmptyFields');
        }
        System.debug(fields);
        return fields;
    }

    private static String generateQuery(string selectfields, string fromObject, string objId){
        string whereStr = ' FROM ' + string.escapeSingleQuotes(fromObject) + ' WHERE Id = \'' + string.escapeSingleQuotes(objId) + '\'';
        string queryStr ='SELECT ' + string.escapeSingleQuotes(selectfields) +  whereStr ;
        System.Debug(objId);
        System.Debug(whereStr);
        System.Debug(queryStr);
        return queryStr;
    }
    
    private static String getFieldStr (List<String> strList){
        String fieldStr = '';
        for(String str: strList) {
            fieldStr = ( String.isEmpty(fieldStr) ? str : fieldStr + ', '+ str);
        }
        return fieldStr;
    }

    private static Map<String, String> getMapFields (String recordTypeName){
        if(!(Schema.sObjectType.FieldsRequiredToReadyProcedure__mdt.isAccessible() &&
             Schema.sObjectType.FieldsRequiredToReadyProcedure__mdt.fields.Label.isAccessible() &&
             Schema.sObjectType.FieldsRequiredToReadyProcedure__mdt.fields.ProcedureRecordType__c.isAccessible() &&
             Schema.sObjectType.FieldsRequiredToReadyProcedure__mdt.fields.ProcedureFieldApiName__c.isAccessible())) {
            DCException.throwPermiException('dcCheckProcedureReady.getMapFields');
        }
        Map<String,String> fieldMap = new Map<String, String>();
        List<FieldsRequiredToReadyProcedure__mdt> mdtList = [SELECT Label, ProcedureFieldApiName__c
                                                             FROM FieldsRequiredToReadyProcedure__mdt
                                                             WHERE ProcedureRecordType__c= :recordTypeName ORDER BY Id];

        for (FieldsRequiredToReadyProcedure__mdt item: mdtList) {
            fieldMap.put(item.Label, item.ProcedureFieldApiName__c);
        }
        return fieldMap;
    }

    private static List<String> putNamespace(List<String> fields){
        List<String> withNamespaceList = new List<String>();
        String prefijo = '';
        
        for(string f: fields){
            withNamespaceList.add(prefijo+f); 
        }

        return withNamespaceList;
    }
}