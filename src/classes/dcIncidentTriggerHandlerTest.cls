/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-02-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   11-02-2020   Elizabeth Betancourt Herrera   Initial Version
**/
@isTest
public with sharing class dcIncidentTriggerHandlerTest {
    /**
    * Testing all after insert trigger
    **/
    @isTest
    static void incidentCreateTest() {
        Test.startTest();
        
        Account acc = new Account(Active__c = 'Yes',Name = 'District Cuba',Phone='2023211169',Type='Mayorista',Sigla__c = 'DCW');
        insert acc;
        
        Contact c = new Contact(FirstName='Luis',LastName='Suarez',AccountId=acc.Id,Eyes_Color__c='Negros');
        insert c;
        
        System.debug(acc.Id);
        Incident__c incident = new Incident__c(Agency__c=acc.Id,
                                               Subject__c='Testing Sharing',
                                               Customer__c=c.Id,
                                               Status__c='Nuevo',
                                               Origen__c='Teléfono',
                                               Tipo__c='Pagos');
        insert incident;

        List<Incident__Share> sharedRecords = [SELECT Id, ParentId, UserOrGroupId FROM Incident__Share WHERE parentId=:incident.Id];
        System.debug(sharedRecords.size());
        System.assert(sharedRecords.size()>0, 'El incidente no fue compartido con el grupo de la agencia.');
   
        Test.stopTest();
    }
}