/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 10-29-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   09-18-2020   Elizabeth Betancourt Herrera   Initial Version
**/
@isTest
public with sharing class dcEmployeeTriggerHandlerTest {
    public dcEmployeeTriggerHandlerTest() {

    }

    /**
    * Testing all after trigger (insert, update, delete)
    **/
    @isTest
    static void updateUserRollUpSummariesTest() {
        //create data to insert
        Account acc = new Account(Active__c = 'Yes',Name = 'District Cuba',Phone='2023211169',Type='Mayorista',Sigla__c = 'DCW');
        insert acc;
         
        user userAux = new user(FirstName = 'sky test', 
                                LastName = 'dc',
                                Username ='dcskytest@theskyplanner.com',
                                Email = 'dcskytest@theskyplanner.com',
                                Alias = 'sky',
                                TimeZoneSidKey='America/Los_Angeles',
                                LocaleSidKey='en_US',
                                EmailEncodingKey='ISO-8859-1',
                                ProfileId=userinfo.getProfileId(),
                                LanguageLocaleKey='en_US',
                                IsActive = false);
        insert userAux; 
        user userAux1 = new user(FirstName = 'sky test 1', 
                                 LastName = 'dc',
                                 Username ='dcskytest1@theskyplanner.com' , 
                                 Email = 'dcskytest1@theskyplanner.com',
                                 Alias = 'sky1',
                                 TimeZoneSidKey='America/Los_Angeles',
                                 LocaleSidKey='en_US',
                                 EmailEncodingKey='ISO-8859-1',
                                 ProfileId=userinfo.getProfileId(),
                                 LanguageLocaleKey='en_US',
                                 IsActive = false);
        insert userAux1; 
        User_Agency_RelationShip__c employee = new User_Agency_RelationShip__c(User__c=userAux.Id, Agency__c=acc.Id);
        
        Test.startTest();

        insert employee;
        userAux = [SELECT Id, Number_Account_Employee__c FROM USER WHERE FirstName like 'sky test'];
        //GroupMember userGroup = [SELECT Id FROM GroupMember WHERE UserOrGroupId=:userAux.Id LIMIT 1];
        System.assertEquals(1, userAux.Number_Account_Employee__c, 'Count agency was not as expected after insert');
        //System.assert(userGroup!=null, 'El user no fue incluido en el grupo al insertar.');

        employee.User__c = userAux1.Id;
        update employee;
        userAux = [SELECT Id, Number_Account_Employee__c FROM USER WHERE FirstName like 'sky test'];
        //userGroup = [SELECT Id FROM GroupMember WHERE UserOrGroupId=:userAux.Id LIMIT 1];
        System.assertEquals(0, userAux.Number_Account_Employee__c, 'Count agency was not as expected after update');
        //System.assert(userGroup==null, 'El user no fue excluido del grupo despues de actualizar.');
        userAux1 = [SELECT Id, Number_Account_Employee__c FROM USER WHERE FirstName like 'sky test 1'];
        //userGroup = [SELECT Id FROM GroupMember WHERE UserOrGroupId=:userAux1.Id LIMIT 1];
        System.assertEquals(1, userAux1.Number_Account_Employee__c, 'Count agency was not as expected after update user 1');
        //System.assert(userGroup!=null, 'El user no fue incluido en el grupo despues de actualizar.');

        delete employee;
        userAux1 = [SELECT Id, Number_Account_Employee__c FROM USER WHERE FirstName like 'sky test 1'];
        //userGroup = [SELECT Id FROM GroupMember WHERE UserOrGroupId=:userAux1.Id LIMIT 1];
        System.assertEquals(0, userAux1.Number_Account_Employee__c, 'Count agency was not as expected after delete employee');
        //System.assert(userGroup==null, 'El user no fue excluido del grupo despues de eliminar.');

        Test.stopTest();
    }
}