/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 10-23-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author             Modification
 * 1.0   10-22-2020   Silvia Velazquez   Initial Version
**/
global without sharing class DCWizardThankyouController {

    @RemoteAction
    global static Procedure__c applyPayment(String procedureId){
        DCWizardPaymentManager manager = new DCWizardPaymentManager();
        return manager.applyPayment(procedureId);
    }
}