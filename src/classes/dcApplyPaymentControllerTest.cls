/**
 * @description       : 
 * @author            : Silvia Velazquez
 * @group             : 
 * @last modified on  : 08-27-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author             Modification
 * 1.0   08-27-2020   Silvia Velazquez   Initial Version
 * 1.1   09-17-2020   Elizabeth Betancourt function removeAppliedPaymentsTest
**/
@isTest
private class dcApplyPaymentControllerTest {
    @testSetup 
    static void makeTestData(){
        Map<String,String> tramite = TestDataFactory.getTramiteParams(true, true, true);
        TestDataFactory.generateArticuloLote(tramite);
    }

    /**
    * @description generate JSON String for items to apply payment
    * @author Silvia Velazquez | 08-27-2020 
    * @return String 
    **/
    static String getItemsPattern(){
        dcApplyPaymentController.ItemsToApply_Wrapper items = new dcApplyPaymentController.ItemsToApply_Wrapper();
        Batch__c batch = [SELECT Id FROM Batch__c LIMIT 1];
        items.batchId = batch.Id;
        Batch_Item__c batchItem = [SELECT Id FROM Batch_Item__c WHERE Batch__c = :batch.Id LIMIT 1];
        items.itemsToApply = new List<String>{batchItem.Id};
        return JSON.serialize(items);
    }

    @isTest
    static void decodeJsonTest(){
        Test.startTest();
        String itemsPattern = getItemsPattern();
        dcApplyPaymentController.ItemsToApply_Wrapper result = dcApplyPaymentController.decodeJson(itemsPattern);
        System.assertNotEquals(null, result,'');
        Test.stopTest();
    }

    @isTest
    static void initDataTest(){
        Test.startTest();
        Batch__c batch = [SELECT Id FROM Batch__c LIMIT 1];
        dcApplyPaymentController.Batch_Wrapper result = dcApplyPaymentController.initData(batch.id);
        System.assertNotEquals(null, result,'');
        Test.stopTest();
    }

    @isTest
    static void applyPaymentsTest(){
        Test.startTest();
        String param = getItemsPattern();
        String result = dcApplyPaymentController.applyPayments(param);
        System.assertEquals('OK', result, '');
        Test.stopTest();
    }
    
    @isTest
    static void removeAppliedPaymentsTest(){
        Test.startTest();
        String param = getItemsPattern();
        String result = dcApplyPaymentController.applyPayments(param);
        System.assertEquals('OK', result, '');
        
        result = dcApplyPaymentController.removeAppliedPayment(param);
        System.assertEquals('OK', result, result);        
        Test.stopTest();
    }
}