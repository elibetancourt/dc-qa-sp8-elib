/**
 * @File Name          : dcContactProcedureSearch.cls
 * @Description        : 
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Elizabeth Betancourt Herrera
 * @Last Modified On   : 10-21-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    27/7/2020   Silvia Velazquez     Initial Version
**/
public with sharing class dcContactProcedureSearch {
    @AuraEnabled
    public String contactId {get; set;}
    @AuraEnabled
    public String firstName {get; set;}
    @AuraEnabled
    public String middleName{get; set;}
    @AuraEnabled
    public String lastName {get; set;}
    @AuraEnabled
    public String secondLastName {get; set;}
    @AuraEnabled
    public String gender {get; set;}
    @AuraEnabled
    public Date birthdate {get; set;}	
    @AuraEnabled
    public String fullName {get; set;}
    @AuraEnabled
    public String phone {get; set;}
    @AuraEnabled
    public String procedureId {get; set;}
    @AuraEnabled
    public String procedureName {get; set;}
    @AuraEnabled
    public String procedureType {get; set;}
    @AuraEnabled
    public String procedureStatus {get; set;}
    @AuraEnabled
    public Boolean consuladoStatus {get; set;}
    @AuraEnabled
    public String procedureAgency {get; set;}
    @AuraEnabled
    public String procedureDate {get; set;}
    @AuraEnabled
    public String procedureNameLink {get; set;}
}