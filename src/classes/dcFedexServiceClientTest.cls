/**
 * @File Name          : dcFedexServiceClientTest.cls
 * @Description        : class to test dcFedexServiceClient class
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Ibrahim Napoles
 * @Last Modified On   : 08-26-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    23/7/2020   Silvia Velazquez     Initial Version
**/
@isTest
private class dcFedexServiceClientTest {
    @isTest
    static void createShipmentOK(){
        // Call the method that invokes a callout
        Map<String,Object> params = TestDataFactory.generateShipmentInfo(true,true); //true value is for getting valid params
        dcFedexServiceClient.ShipServicePort service = new dcFedexServiceClient.ShipServicePort();
        service.endpoint_x = 'https://wsbeta.fedex.com:443/web-services/ship';
        Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new dcFedexServiceCalloutMock('SUCCESS')); //generating a success response    
        Map<String,Object> result = service.createShipment(params);
        Test.stopTest();
        // Verify that a fake result is returned
        dcFedexServiceClient.ProcessShipmentReply reply = (dcFedexServiceClient.ProcessShipmentReply) result.get('response');
        System.assertEquals('SUCCESS',reply.HighestSeverity, ''); 
    }

    @isTest
    static void createShipmentInvalidDate(){
        // Call the method that invokes a callout
        Map<String,Object> params = TestDataFactory.generateShipmentInfo(false,true); //true value is for getting valid params
        dcFedexServiceClient.ShipServicePort service = new dcFedexServiceClient.ShipServicePort();
        service.endpoint_x = 'https://wsbeta.fedex.com:443/web-services/ship';
        Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new dcFedexServiceCalloutMock('WARNING')); //generating a success response    
        Map<String,Object> result = service.createShipment(params);
        Test.stopTest();
        // Verify that a fake result is returned
        dcFedexServiceClient.ProcessShipmentReply reply = (dcFedexServiceClient.ProcessShipmentReply) result.get('response');
        System.assertEquals('WARNING',reply.HighestSeverity, '');
    }
}