/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 10-29-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   09-18-2020   Elizabeth Betancourt Herrera   Initial Version
**/
public with sharing class dcEmployeeTriggerHandler {

    Boolean isBefore;
    Boolean isAfter;
    Boolean isInsert;
    Boolean isDelete;
    Boolean isUpdate;
    List<User_Agency_RelationShip__c> newList {get; set;}
    List<User_Agency_RelationShip__c> oldList {get; set;}
    Map<Id, User_Agency_RelationShip__c> oldMap {get; set;}

    public dcEmployeeTriggerHandler() {
        newList = trigger.new;
        oldList = trigger.old;
        oldMap = (Map<Id, User_Agency_RelationShip__c>)trigger.oldMap;
        this.isBefore = trigger.isBefore;
        this.isAfter = trigger.isAfter;
        this.isInsert = trigger.isInsert;
        this.isUpdate = trigger.isUpdate;
        this.isDelete = trigger.isDelete;
    }
    public void updateCountAgencyUserEmployee (){
        Map<Id, Id> userIdToUpdate = getUserToUpdate();                                      
        
        updateUserRollUpSummary(userIdToUpdate);      
                                          
    }

    private Map<Id, Id> getUserToUpdate (){
        Map<Id, Id> userIdToUpdate = new Map<Id,Id>();

        if (newList == null)    {
            for(User_Agency_RelationShip__c employee: oldMap.values()){  
                if( employee.User__c != null &&
                    !userIdToUpdate.containsKey(employee.User__c) ){
                        userIdToUpdate.put(employee.User__c, employee.User__c);
                    }
            }
        } 
        else {
            for(User_Agency_RelationShip__c employee: newList){
                if( employee.User__c != null &&
                    !userIdToUpdate.containsKey(employee.User__c) &&
                    ( oldMap == null || employee.User__c != oldMap.get(employee.Id).User__c )
                ){
                    userIdToUpdate.put(employee.User__c, employee.User__c);
                    //agregar calculo para el user anterior
                    if(oldMap != null &&  !userIdToUpdate.containsKey(oldMap.get(employee.Id).User__c)){
                        userIdToUpdate.put(oldMap.get(employee.Id).User__c, oldMap.get(employee.Id).User__c);
                    }
                }
            }
        }
        return userIdToUpdate;
    }
    private void updateUserRollUpSummary(Map<Id, Id> userIdToUpdate){
        
        if( userIdToUpdate.size() > 0){
            
            if(!(Schema.sObjectType.User.isAccessible() &&
                Schema.sObjectType.User.fields.Id.isAccessible() &&
                Schema.sObjectType.User.fields.Number_Account_Employee__c.isAccessible() &&
                Schema.sObjectType.User_Agency_RelationShip__c.isAccessible() &&
                Schema.sObjectType.User_Agency_RelationShip__c.fields.Agency__c.isAccessible() &&
                Schema.sObjectType.User_Agency_RelationShip__c.fields.User__c.isAccessible() &&
                Schema.sObjectType.User_Agency_RelationShip__c.fields.Id.isAccessible())) {
                DCException.throwPermiException('dcEmployeeTriggerHandler.updateUserRollUpSummary');
            }

            List<User> userListToUpdate = [SELECT Id, Number_Account_Employee__c
                                           FROM User 
                                           WHERE Id in: userIdToUpdate.values() Order By Id];

            AggregateResult[] rollUpSummaries= [SELECT User__c User__c, COUNT(Id) cantAgency
                                                FROM User_Agency_RelationShip__c  
                                                WHERE User__c in: userIdToUpdate.values() 
                                                GROUP BY User__c Order By User__c];
            Integer j=0;
            for(Integer i=0; i < userListToUpdate.size(); i++){
                System.debug(userListToUpdate[i].Id);
                if (j < rollUpSummaries.size() && userListToUpdate[i].Id == (string)rollUpSummaries[j].get('User__c')){
                   System.debug(rollUpSummaries[j].get('User__c'));
                   System.debug(rollUpSummaries[j].get('cantAgency'));
                   userListToUpdate[i].Number_Account_Employee__c = (decimal)rollUpSummaries[j].get('cantAgency');
                   j++;
                }
                else{
                    userListToUpdate[i].Number_Account_Employee__c = 0;
                }
            }
            
            userListToUpdate.sort(); //Sorting by Sobject Type to prevent Error: "System.TypeException: Cannot have more than 10 chunks in a single operation."
            try {
                CRUDEnforce.dmlUpdate(userListToUpdate, 'dcEmployeeTriggerHandler.updateUserRollUpSummary');
            } catch (DmlException e) {
                DCException.throwException(e.getMessage());
            } 
        }
    }
}