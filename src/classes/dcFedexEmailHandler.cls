/**
 * @File Name          : dcFedexEmailHandler.cls
 * @Description        : Class for managing incoming emails from FedEx
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Elizabeth Betancourt Herrera
 * @Last Modified On   : 10-15-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    8/7/2020   Silvia Velazquez     Initial Version
 * 1.1   08-21-2020   Ibrahim Napoles          Validate CRUD permission before SOQL/DML operation.
**/
public without sharing class dcFedexEmailHandler implements Messaging.InboundEmailHandler {

     public Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        if(!(Schema.sObjectType.Tracking__c.isAccessible() &&
             Schema.sObjectType.Tracking__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Tracking__c.fields.Status__c.isAccessible() &&
             Schema.sObjectType.Tracking__c.fields.Name.isAccessible())) {
            DCException.throwPermiException('dcFedexEmailHandler.handleInboundEmail');
        }

        // Create an InboundEmailResult object for returning the result of the Apex Email Service
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
      
        //String myPlainText= '';
        
        // Store the email plain text into the local variable 
        //String plainTextBody = email.plainTextBody;
        String htmlBody = email.htmlBody;
        String eSubject = email.subject;
        String language = 'ES'; //Get values from mail in this language. (Valid values: EN,ES)
        String logMessage = 'Email Subject: '+eSubject+'\n';
        String logSubject = 'Email Notification';
        System.debug('Raw Email Subject: '+ eSubject);
        System.debug('PlainText Email Body:' + htmlBody.stripHtmlTags());
        System.debug('HTML Email Body: ' +htmlBody);

        Map<String,String> values = getValuesFromHTMLMail(eSubject,htmlBody,language);
        if(values !=null){
           String trackingNUmber = values.get('trackingNumber').trim();
           logMessage+= 'Subject after treatement: '+ values.get('description')+'\n';
           logMessage+= 'Message: '+values.get('message')+'\n';
           logMessage+= 'Tracking Number: '+ trackingNUmber+'\n';
           logMessage+= 'Tracking Status: '+ values.get('status')+'\n';
           logSubject+= ' '+trackingNUmber;
           List<Tracking__c> trackings = [SELECT Id, Status__c FROM Tracking__c WHERE Name=:trackingNUmber AND Carrier__c = 'FedEx'];
           if(trackings.size() > 0){
                //Tracking__c tracking = trackings[0];
                Date historyDate = Date.Today();
                Time historyTime = DateTime.now().time();
                String description;
                String message;
                String location = ' ';
                if(eSubject.containsIgnoreCase('Envío de FedEx') || eSubject.containsIgnoreCase('Envío FedEx')){
                   message  = values.get('description') + ' - '+values.get('message');
                   description = String.isNotBlank(values.get('status')) ? values.get('status') : values.get('description');
                }
                else {
                    Datetime dt = getValidDate(values.get('statusDate'));
                    historyDate = dt.date();
                    historyTime = dt.time();
                    description = values.get('status');
                    message = description;
                    location = values.get('location');
                }
                List<Tracking_History_Item__c> trackHistoryList = new List<Tracking_History_Item__c>();
                
                //tracking.Status__c = description;
                for(Tracking__c tracking: trackings){
                    tracking.Status__c = description;
                    Tracking_History_Item__c trackHistory = new Tracking_History_Item__c(Tracking__c = tracking.Id,Date__c = historyDate,
                                                                                   Time__c = historyTime,Description__c = message, 
                                                                                   Location__c = location);
                    trackHistoryList.add(trackHistory);
                }
                //insert trackHistoryList;
                CRUDEnforce.dmlInsert(trackHistoryList, 'dcFedexEmailHandler.handleInboundEmail');
                //update trackingList; 
                CRUDEnforce.dmlUpdate(trackings, 'dcFedexEmailHandler.handleInboundEmail');
           }
           else {
            System.debug('The Tracking Number '+trackingNUmber+' is not in Salesforce.');
            result.success = false;
            result.message = 'The Tracking Number '+trackingNUmber+' is not in Salesforce.';

            logMessage+='Parsing Result Status: '+ String.valueOf(result.success)+'\n';
            logMessage+='Parsing Result Message: '+ result.message+'\n';
            logMessage+='HTML Mail Body:\n'+ htmlBody;
            generateEmailLog(logMessage,logSubject);
            return result; 
            }
        }
        else {
            System.debug('No "Tracking Number xxxxxxxxxxxx" pattern was encountered in this email body.');
            result.success = false;
            result.message = 'No "Tracking Number xxxxxxxxxxxx" pattern was encountered in this email body:';
            
            logMessage+='Parsing Result Status: '+ String.valueOf(result.success)+'\n';
            logMessage+='Parsing Result Message: '+ result.message+'\n';
            logMessage+='HTML Mail Body:\n'+ htmlBody;
            generateEmailLog(logMessage,logSubject);
            return result; 
        }
      
       // Set the result to true. No need to send an email back to the user with an error message
       result.success = true;
       result.message = 'The following email message has been successfully parsed.';
       //Generate email log
       logMessage+='Parsing Result Status: '+ String.valueOf(result.success)+'\n';
       logMessage+='Parsing Result Message: '+ result.message+'\n';
       logMessage+='HTML Mail Body:\n'+ htmlBody;
       generateEmailLog(logMessage,logSubject);
       // Return the result for the Apex Email Service
       return result;
    }

   /* public Map<String,String> getValuesFromTXTMail(String subject,String mailBody,String language){
        Map<String,String> values;
        String trackNumber = (language=='ES')?'Número de rastreo':'Tracking number';
        String statusDate = (language=='ES')?'Día/Hora':'Date/Time';
        String activity = (language=='ES')?'Actividad/Ubicación':'Activity/Location';
        String delivery = (language=='ES')?'Entrega programada':'Scheduled delivery';
        String pending = (language=='ES')?'Pendiente':'Pending';
        Integer i = 0;
        Boolean isEnvio = (subject.containsIgnoreCase('Envío de FedEx') || subject.containsIgnoreCase('Envío FedEx'));

        Integer trackingPos = mailBody.indexOfIgnoreCase(trackNumber);
        if(trackingPos != -1 ){
            values = new Map<String,String>();
            List<String> parts = mailBody.split('\n');

            if(isEnvio){ //Fedex Shipment expression in subject
                //get the Email subject
                String validSubject = subject.substring(subject.indexOfIgnoreCase('Envío')); //RV: Envío de FedEx 770618101415: Tu paquete ha sido entregado.
                if(validSubject.contains(':')){
                    validSubject = validSubject.substringAfterLast(':');
                }

                values.put('description',validSubject);
                System.debug('Email Subject Refined from Mail Content: '+ values.get('description'));

                //get the main message inside the Email body
                Boolean enc=false;
                if(mailBody.containsIgnoreCase('Asunto:')){
                    while(enc == false && i < parts.size()){
                        if(parts[i].containsIgnoreCase('Asunto:')){
                            enc = true;
                        }
                        i++;
                    }   
                }
                enc=false;
                while (enc == false && i < parts.size()) { //get the first text before several empty lines
                    if(String.isNotBlank(parts[i])){
                        enc=true;
                        values.put('message',parts[i]);
                        System.debug('Mail Message from Mail Content: '+ values.get('message'));
                    }
                    i++;
                }                
            }

            for(Integer j=i;j<parts.size();j++){
                String item = parts[j];
                if(String.isNotBlank(item)){
                  System.debug('Printing Parts: '+ item);
                    if(item.containsIgnoreCase(trackNumber) && item.length() < 20 && String.isBlank(values.get('trackingNumber'))){
                        String trackValue = String.isNotBlank(parts[j+1])?parts[j+1]:parts[j+2];
                        values.put('trackingNumber', trackValue); //Tracking Number Label in a string line, tracking value at the next string line(next item in the List)
                        System.debug('Tracking Number from Mail Content: '+ trackValue);
                    }

                    if(!isEnvio){ //If email is not an online Tracking Message
                        if(item.containsIgnoreCase(statusDate)){
                            values.put('statusDate',parts[j+2]);
                            System.debug('Date/Time from Mail Content: '+ parts[j+2]);
                        }
                        else if(item.containsIgnoreCase(activity)){
                            values.put('status', parts[j+2]);
                            System.debug('Status from Mail Content: '+ parts[j+2]);
                            values.put('location', (String.isNotBlank(parts[j+4]))?parts[j+4]:'Pendiente');
                            System.debug('Location from Mail Content: '+ values.get('location'));  
                        }
                        else if(item.containsIgnoreCase(delivery) && item.length() < 30 && String.isBlank(values.get('schedule_Delivery'))){
                            values.put('schedule_Delivery',parts[j+1]);
                            System.debug('Scheduled delivery from Mail Content: '+parts[j+1]);  
                        }
                    } 
                } 
            }
        }
        return values;
    }*/

    public Map<String,String> getValuesFromHTMLMail(String subject,String mailBody,String language){
        Map<String,String> values;
        String trackNumber = (language=='ES')?'>Número de rastreo':'>Tracking number';
        String statusDate = (language=='ES')?'>Día/Hora':'>Date/Time';
        String activity = (language=='ES')?'>Actividad/Ubicación':'>Activity/Location';
        String delivery = (language=='ES')?'>Entrega programada':'>Scheduled delivery';
        String pending = (language=='ES')?'Pendiente':'Pending';
        Integer i = 0;
        subject = getValidChars(subject);
        mailBody = getValidChars(mailBody);
        Boolean isEnvio = (subject.containsIgnoreCase('Envío de FedEx') || subject.containsIgnoreCase('Envío FedEx'));

        if(mailBody.indexOfIgnoreCase(trackNumber) != -1){
            values = new Map<String,String>();
            List<String> parts = mailBody.split('\n');

            if(isEnvio){ //Fedex Shipment expression in subject
                //get the Email subject
                String validSubject = subject.substring(subject.indexOfIgnoreCase('Envío')); //RV: Envío de FedEx 770618101415: Tu paquete ha sido entregado.
                
                if(validSubject.contains(':')){       //Envío de FedEx 770618101415: Tu paquete ha sido entregado.
                    validSubject = validSubject.substringAfterLast(':'); 
                }
                else{                                  //Envío FedEx 395256776191 Notificación
                    validSubject = getValidSubject(validSubject); 
                }
                values.put('description',validSubject);
                System.debug('Email Subject Refined from Mail Content: '+ values.get('description'));

                //get the main message inside the Email body
                String message;
                while (String.isBlank(message) && i < parts.size()) { //get the first text line inside the mail body
                    if(parts[i]!='</td>' && parts[i].endsWith('</td>') && parts[i].right(6) !='></td>'){
                        message = parts[i].substringBetween('>', '</td>');
                    }
                    i++;
                }
                if(String.isBlank(message)){
                    message = validSubject;
                }
                values.put('message',message);
                System.debug('Mail Message from Mail Content: '+ message);             
            }

            //Integer paramsCount = (isEnvio == true)?2:2;  //Amount of parameters to read inside the html
            Integer paramsCount = 2;
            if(isEnvio == true && !mailBody.containsIgnoreCase('alt="Delivery progress bar"')) {paramsCount = 1;} //If message body not contains progress bar, only one parameter is reading.
    
            Integer j = i;
            While(paramsCount > 0 && j<parts.size()){
                String item = parts[j];
                if(String.isNotBlank(item)){
                    System.debug('Printing Parts: '+ item);
                    if((item.containsIgnoreCase(trackNumber)) && String.isBlank(values.get('trackingNumber'))){
                        String trackValue = getValueFromHTML('tracking',parts,j);
                        values.put('trackingNumber', trackValue); //Tracking Number Label in a string line, tracking value at the next string line(next item in the List)
                        System.debug('Tracking Number from Mail Content: '+ trackValue);
                        paramsCount--;
                    }
                    else if(item.containsIgnoreCase('alt="Delivery progress bar"')){  //image with status progress bar
                        String trackStatus = getValueFromHTML('status',parts,j); 
                        values.put('status',trackStatus);
                        System.debug('Status from Mail Content: '+ trackStatus);
                        paramsCount--;
                    }
                    else  if(item.containsIgnoreCase(statusDate) && item.containsIgnoreCase(activity)){
                        String resValue = getValueFromHTML('date-activity',parts,j+1);
                        String dateValue = resValue.substringBefore('-').stripHtmlTags();
                        String actValue = resValue.substringAfter('-').stripHtmlTags();
                        String locValue = pending;
                        if(actValue.contains('-')){ //resValue maight have the pattern: xxxx-xxxx-xxxx
                            locValue = actValue.substringAfter('-');
                            actValue = actValue.substringBefore('-'); 
                        }
                        
                        values.put('statusDate',dateValue);
                        System.debug('Date/Time from Mail Content: '+ dateValue);

                        values.put('status',actValue);
                        System.debug('Status from Mail Content: '+ actValue);

                        values.put('location',locValue);
                        System.debug('Location from Mail Content: '+ locValue);
                        paramsCount--;
                    }
                }
                j++;  
            }
        }   
        return values;
    }
    
    public String getValueFromHTML(String tag,List<String> lines,Integer pos){
        String result;
        String item = lines[pos];
        Boolean enc = false;
        Integer i = pos;
        if(tag == 'tracking'){
            String labelHtml = '<b>Número de rastreo</b></td>';
            if(item.indexOfIgnoreCase(labelHtml) == -1){
                labelHtml = '<b>Número de rastreo:</b></td>';
            }
            String sameLine = item.substringAfter(labelHtml);
            if(String.isBlank(sameLine)){
                i=pos+1;
            }
            while(enc == false && i< lines.size()){
                item = lines[i];
                Integer posA = item.indexOf('<a');
                if(posA != -1){
                    enc = true;
                    result = item.substring(posA);
                    result = result.substringBetween('>', '</a>');               
                }
                else {
                    i++;
                }
            }
            if(enc == false){
                result = 'Track Number not Found.';
            }
        }
        else if (tag == 'status'){
            while(enc == false && i< lines.size()){
                item = lines[i];
                if(item != '</td>' && item.endsWith('</td>') && item.right(6) !='></td>'){
                    result = item.substringBetween('>', '</td>');
                    enc = true;
                }
                i++;     
            }
            if(enc ==false){
                result = 'Status not Found';
            }
        }
        else if(tag == 'date-activity'){
            while(enc == false && i< lines.size()){
                if(lines[i] == '<tr>'){
                   enc = true;
                   item = lines[i+1];
                }
                i++;   
            }
            if(enc == true){
                result = item.substringBetween('>', '</td>'); // get the date value
                item = item.substringAfter('</td>');
                result+= '-'+item.substringBetween('>', '</td>'); //get  the activity
                if(lines[i+2] == '<tr>'){ //if location has any value
                    item = lines[i+3];
                    item = item.substringAfter('</td>');
                    result+= '-'+item.substringBetween('>', '</td>'); //get the location
                }
            }
            else {
                result = 'Date/Activity values not Found';
            }
        }
        /* else if(tag == 'location'){ 
        } */

        return result;
    }

    public Datetime getValidDate(String value){
        Datetime result;
        //value = '6/25/2020 11:10 am';
        if(value.countMatches('/') == 2){
            Integer blankPos = value.indexOf(' ');
            if(blankPos != -1){
                String validDateTime = value.substringBefore(' ') + ', '+value.substringAfter(' '); //'6/25/2020, 11:10 am'
                result = DateTime.parse(validDateTime);
            }
        }
        return result;
    }

    public String getValidChars(String param){
        String result = param;
        if(param.contains('&aacute;')){
            result = result.replace('&aacute;', 'á');
        }

        if(param.contains('&eacute;')){
            result = result.replace('&eacute;', 'é');
        }

        if(param.contains('&iacute;')){
            result = result.replace('&iacute;', 'í');
        }

        if(param.contains('&oacute;')){
            result = result.replace('&oacute;', 'ó');
        }

        if(param.contains('&uacute;')){
            result = result.replace('&uacute;', 'ú');
        }
       return result;         
    }

    public void generateEmailLog(String message,String subject){
        if(message.length() > 50000){
            message = message.substring(0, 49999);
        }
        
        Tracking_Error_Log__c errorLog = new Tracking_Error_Log__c(Source__c='Email Service',Error_Code__c = subject,Error_Message__c= message);
        //insert(errorLog);
        CRUDEnforce.dmlInsert(new List<Tracking_Error_Log__c>{errorLog}, 'dcFedexEmailHandler.generateEmailLog');
    }

    public String getValidSubject(String subject){ //Envío FedEx 395256776191 Notificación --> Notificación
        Boolean enc = false;
        Integer i= 0;
        String result;
        String tempResult;

        if(subject.containsIgnoreCase('Envío FedEx')){
            result = subject.removeStartIgnoreCase('Envío FedEx'); //Envío FedEx 395256776191 Notificación -->  395256776191 Notificación 
        }
        else if(subject.containsIgnoreCase('Envío de FedEx')){
            result = subject.removeStartIgnoreCase('Envío de FedEx');
        }

        while (enc ==false && i< result.length()) {
           tempResult = result.left(i+1);
           if(!tempResult.isNumericSpace()){
            enc = true; //1rst letter found
           }
           else{ 
               i++;
            }
        }
        
        if(enc == true){  
            result = result.substring(i);
        }
        else{
            result = subject;
        }
        
        return result; 
    }
}