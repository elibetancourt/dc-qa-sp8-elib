/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-03-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   10-26-2020   Elizabeth Betancourt Herrera   Initial Version
**/
public with sharing class dcTrackingTriggerHandler {
    Boolean isBefore;
    Boolean isAfter;
    Boolean isInsert;
    Boolean isDelete;
    Boolean isUpdate;
    List<Tracking__c> newList {get; set;}
    List<Tracking__c> oldList {get; set;}
    Map<Id, Tracking__c> oldMap {get; set;}
    
    public dcTrackingTriggerHandler() {
        newList = trigger.new;
        oldList = trigger.old;
        oldMap = (Map<Id, Tracking__c>)trigger.oldMap;
        this.isBefore = trigger.isBefore;
        this.isAfter = trigger.isAfter;
        this.isInsert = trigger.isInsert;
        this.isUpdate = trigger.isUpdate;
        this.isDelete = trigger.isDelete;    }

    
    public void updateTotalEnvioAmount (){
        Map<Id, Id> procedureIdToUpdate = getTrackingToUpdate();                                      
        
        updateProcedureRollUpSummary(procedureIdToUpdate);                                           
    }

    private Map<Id, Id> getTrackingToUpdate (){
        Map<Id, Id> procedureIdToUpdate = new Map<Id,Id>();

        if (newList == null)    {
            for(Tracking__c track: oldMap.values()){  
                if( track.Procedure__c != null &&
                    track.Type__c != null &&
                    track.Type__c != 'Tramite completado' &&
                    !procedureIdToUpdate.containsKey(track.Procedure__c) ){
                        procedureIdToUpdate.put(track.Procedure__c, track.Procedure__c);
                    }
            }
        } 
        else {
            for(Tracking__c track: newList){
                if( track.Procedure__c != null &&
                    track.Type__c != null &&
                    track.Type__c != 'Tramite completado' &&
                    !procedureIdToUpdate.containsKey(track.Procedure__c) &&
                    ( oldMap == null ||
                      track.Rate__c != oldMap.get(track.Id).Rate__c ||
                      track.Procedure__c != oldMap.get(track.Id).Procedure__c )
                ){
                    procedureIdToUpdate.put(track.Procedure__c, track.Procedure__c);
                }
                else if (track.Procedure__c == null &&
                         oldMap != null && 
                         oldMap.get(track.Id).Procedure__c != null &&
                         oldMap.get(track.Id).Type__c != null &&
                         oldMap.get(track.Id).Type__c != 'Tramite completado' &&
                         !procedureIdToUpdate.containsKey(oldMap.get(track.Id).Procedure__c)){
                    procedureIdToUpdate.put(oldMap.get(track.Id).Procedure__c, 
                                              oldMap.get(track.Id).Procedure__c);
                }
            }
        }
        return procedureIdToUpdate;
    }
    private void updateProcedureRollUpSummary(Map<Id, Id> procedureIdToUpdate){
        
        if( procedureIdToUpdate.size() > 0){
            
            if(!(Schema.sObjectType.Tracking__c.isAccessible() &&
                Schema.sObjectType.Tracking__c.fields.Id.isAccessible() &&
                Schema.sObjectType.Tracking__c.fields.Rate__c.isAccessible() &&
                Schema.sObjectType.Tracking__c.fields.Procedure__c.isAccessible() &&
                Schema.sObjectType.Procedure__c.isAccessible() &&
                Schema.sObjectType.Procedure__c.fields.Delivery_Fee__c.isAccessible() &&
                Schema.sObjectType.Procedure__c.fields.Id.isAccessible())) {
                DCException.throwPermiException('dcTrackingTriggerHandler.updateProcedureRollUpSummary');
            }

            List<Procedure__c> prodListToUpdate = [SELECT Id, Delivery_Fee__c
                                                            FROM Procedure__c 
                                                            WHERE Id in: procedureIdToUpdate.values() Order By Id];

            AggregateResult[] rollUpSummaries= [SELECT Procedure__c Procedure__c, SUM(Rate__c) totalEnvio
                                                FROM Tracking__c  
                                                WHERE Procedure__c in: procedureIdToUpdate.values() AND
                                                Type__c!='Tramite completado' 
                                                GROUP BY Procedure__c Order By Procedure__c];
            Integer j=0;
            for(Integer i=0; i < prodListToUpdate.size(); i++){
                System.debug(prodListToUpdate[i].Id);
                if (j < rollUpSummaries.size() && prodListToUpdate[i].Id == (string)rollUpSummaries[j].get('Procedure__c')){
                   System.debug(rollUpSummaries[j].get('Procedure__c'));
                   System.debug(rollUpSummaries[j].get('totalEnvio'));
                   prodListToUpdate[i].Delivery_Fee__c = (decimal)rollUpSummaries[j].get('totalEnvio');
                   j++;
                }
                else{
                    prodListToUpdate[i].Delivery_Fee__c = 0;
                }
            }

            prodListToUpdate.sort(); //Sorting by Sobject Type to prevent Error: "System.TypeException: Cannot have more than 10 chunks in a single operation."
            try {
                CRUDEnforce.dmlUpdate(prodListToUpdate, 'dcTrackingTriggerHandler.updateProcedureRollUpSummary');
            } catch (DmlException e) {
                DCException.throwException(e.getMessage());
            } 
        }
    }

    public void TrackingTriggerActions (){
        
        if(isAfter && isInsert) {
            //share account whith user in public group
            shareRecord();
        }   
        updateTotalEnvioAmount();                        
    }

    public void shareRecord (){
        
       Map<String, List<sObject>> groupTrackingList = getGroupListToShare();
       System.debug('Lista tarifa de envio a compartir');
       for(String groupId: groupTrackingList.keySet()){
           System.debug('groupId --> '+groupId) ;
           System.debug('total a compratir --> '+groupTrackingList.get(groupId).size()) ;
           dcSharingManager.shareRecords(groupTrackingList.get(groupId), 
                                         'Tracking__Share', 'Edit', groupId, 'Manual');
       }
    }

    public Map<String, List<sObject>> getGroupListToShare (){
        
        Map<String, List<sObject>> groupMap = new Map<String, List<sObject>>();
        
        String groupIdAgencyMayorista = dcPublicGroupUtils.getGroupAgencyMayorista();
        
        if(String.isNotEmpty(groupIdAgencyMayorista)) groupMap.put(groupIdAgencyMayorista, newList);

        return groupMap;
    }  
}