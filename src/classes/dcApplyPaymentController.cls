/**
 * @File Name          : dcApplyPaymentController.cls
 * @Description        : Apex Controller for Aura component to Apply Payments
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Elizabeth Betancourt Herrera
 * @Last Modified On   : 11-03-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    20/8/2020   Silvia Velazquez     Initial Version
 * 1.1    16/9/2020   Elizabeth Betancourt     Include functionality to remove applied payment
**/
public with sharing class dcApplyPaymentController {

    public static ItemsToApply_Wrapper decodeJson(String itemsPattern){
        ItemsToApply_Wrapper objConverted = (ItemsToApply_Wrapper) JSON.deserialize(itemsPattern, ItemsToApply_Wrapper.class);
        return objConverted;
    }

    /**
    * @description Retrieves Batch data from a given ID
    * @author Silvia Velazquez | 20/8/2020 
    * @param batchId 
    * @return Batch_Wrapper 
    **/
    @AuraEnabled
    public static Batch_Wrapper initData(String batchId){
        return dcApplyPaymentUtility.getBatchData(batchId);
    }


    /**
    * @description Apply payments and generate debit and credit transactions
    * @author Silvia Velazquez | 08-21-2020 
    * @param itemsPattern 
    * @return String 
    **/
    @AuraEnabled
    public static String applyPayments(String itemsPattern){
        return dcApplyPaymentUtility.applyPayment(decodeJson(itemsPattern));
    }
    
    /**
    * @description remove Applied Payments and generate corresponding debit and credit transactions 
    * to anulate corresponding transactions in apply payment process
    * @author Elizabeth Betancourt | 09-16-2020 
    * @param itemsPattern 
    * @return String 
    **/
    @AuraEnabled
    public static String removeAppliedPayment(String itemsPattern){
        return dcApplyPaymentUtility.removeAppliedPayment(decodeJson(itemsPattern));
    }

    public class Batch_Wrapper{  //Wrapper class to handle batch info.
        @AuraEnabled
        public String batch_Id {get; set;}
        @AuraEnabled
        public String batch_Name {get; set;}
        @AuraEnabled
        public Decimal amount_Paid {get; set;}  //Monto Pagado
        @AuraEnabled
        public Decimal amount_Due {get; set;} //Pendiente x Pagar
        @AuraEnabled
        public Decimal amount_Applied {get; set;} //Monto Aplicado
        @AuraEnabled
        public Decimal amount_Pending_to_Apply {get; set;} //Monto Pendiente a Aplicar
        @AuraEnabled
        public Integer procedure_count_to_Apply {get; set;} //cantidad de tramites pendientes a aplicar
        @AuraEnabled
        public Boolean have_Payment {get; set;} //false si no tiene pago asociado
        @AuraEnabled
        public Boolean is_Closed {get; set;}  //false si no cerrado
        @AuraEnabled
        public Boolean is_Recived {get; set;}  //false si no esta recibido
        @AuraEnabled
        public List<Batch_Item_Wrapper> batch_items {get; set;}
    }

    public class Batch_Item_Wrapper{
        @AuraEnabled
        public String batch_item_id {get; set;} //Id del Articulo de Lote
        @AuraEnabled
        public String batch_item_Name {get; set;} //Nombre del Articulo de Lote
        @AuraEnabled
        public String procedure_id {get; set;} //Id del Tramite
        @AuraEnabled
        public String procedureNameLink {get; set;} //Id del tramite para el link
        @AuraEnabled
        public String procedure_Name {get; set;} //Nombre del Tramite
        @AuraEnabled
        public String procedure_Contact {get; set;} // Nombre del Cliente
        @AuraEnabled
        public String procedure_type {get; set;} // Tipo de tramite
        @AuraEnabled 
        public String procedure_date {get; set;} //Fecha del tramite
        @AuraEnabled
        public Decimal procedure_consulate_payment {get; set;} //Pago al conuslado
        @AuraEnabled
        public Decimal procedure_agency_comission {get; set;} //Comision de Agencia
        @AuraEnabled
        public Decimal procedure_distributor_payment {get; set;} //Pago mayorista
        @AuraEnabled
        public Boolean procedure_Applied_Payment {get; set;} //Pago aplicado
    }

    public class ItemsToApply_Wrapper{  //Class to receive the batch items to apply payments
        @AuraEnabled
        public String batchId {get; set;} //Id del Lote
        @AuraEnabled
        public List<String> itemsToApply {get; set;}  //Ids de Articulos de lote seleccionados para aplicar pago
    }
        
}