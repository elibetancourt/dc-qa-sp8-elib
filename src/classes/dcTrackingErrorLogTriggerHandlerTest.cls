/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-03-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   11-02-2020   Elizabeth Betancourt Herrera   Initial Version
**/
@isTest
public with sharing class dcTrackingErrorLogTriggerHandlerTest {
    
    /**
    * Testing all after insert trigger
    **/
    @isTest
    static void trackingErrorLogCreateTest() {
        Test.startTest();
        
        Tracking_Error_Log__c errorLog = new Tracking_Error_Log__c(Source__c='Email Service',Error_Code__c = 'Testing error',Error_Message__c= 'Testing Message');
        insert errorLog;

        List<Tracking_Error_Log__Share> sharedRecords = [SELECT Id, ParentId, UserOrGroupId FROM Tracking_Error_Log__Share WHERE parentId=:errorLog.Id];
        System.debug(sharedRecords.size());
        System.assert(sharedRecords.size()>0, 'La tarifa de envio no fue compartida con el grupo de la agencia.');
   
        Test.stopTest();
    }    
}