/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-02-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   11-02-2020   Elizabeth Betancourt Herrera   Initial Version
**/
@isTest
public with sharing class dcBatchTriggerHandlerTest {
    /**
    * Testing all before insert trigger and after insert trigger
    **/
    @isTest
    static void batchCreateTest() {
        Test.startTest();
        
        String batchId = TestDataFactory.getLoteParam(true);
        System.debug('batch id --> '+batchId);
        List<Batch__Share> sharedRecords = [SELECT Id, ParentId, UserOrGroupId FROM Batch__Share WHERE parentId=:batchId];
        System.debug(sharedRecords.size());
         System.debug(sharedRecords);
        System.assert(sharedRecords.size()>0, 'El lote no fue compartida con el grupo de la agencia.');
   
        Test.stopTest();
    }
}