/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-03-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   09-22-2020   Elizabeth Betancourt Herrera   Initial Version
**/
@isTest
public with sharing class dcPaymentsTriggerHandlerTest {
    public dcPaymentsTriggerHandlerTest() {

    }
    @TestSetup
    static void makeData(){
        
        Account acc = new Account(Active__c = 'Yes',Name = 'District Cuba',Phone='2023211169',Type='Mayorista',Sigla__c = 'DCW');
        insert acc;
            
        Account accR = new Account(Active__c = 'Yes',Name = 'District Cuba R',Phone='2023211169',Type='Minorista',Sigla__c = 'DCR', Distributor__c=acc.Id);
        insert accR;
        
        User userAux = new User(FirstName = 'sky test', 
                                LastName = 'dc',
                                Username ='dcskytest@theskyplanner.com',
                                Email = 'dcskytest@theskyplanner.com',
                                Alias = 'sky',
                                TimeZoneSidKey='America/Los_Angeles',
                                LocaleSidKey='en_US',
                                EmailEncodingKey='ISO-8859-1',
                                ProfileId=userinfo.getProfileId(),
                                LanguageLocaleKey='en_US',
                                IsActive = true);
        insert userAux;
        
        User_Agency_RelationShip__c employee = new User_Agency_RelationShip__c(User__c=userAux.Id, Agency__c=acc.Id);        
        insert employee;
        
        Contact c = new Contact(FirstName='Luis',LastName='Suarez',AccountId=acc.Id,Eyes_Color__c='Negros');
        insert c;
        
        Date d = date.today();
        //two records for each RecordType, one complete and one incomplete
        
        //pasaporte 1ra vez incompleto
        Id pas1raVezRT =  DCUtils.getRecTypeIdByDevName('Procedure__c', 'Pasaporte_1ra_Vez');
        Procedure__c p = new Procedure__c(Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId=pas1raVezRT,
                                          First_Name__c='Luis', Last_Name__c='Suarez', Street__c='calle 10 entre 1ra y 3ra',
                                          Eyes_Color__c='Negros', Consulate_Payment__c = 375);
        insert p;
       
        Batch__c lote = new Batch__c(Status__c='Abierto',
                                     Agency__c = acc.Id,
                                     Recipient__c = accR.Id);
        insert lote;
    }

    @isTest
    static void updateProcedurePaymentsTest(){

        User userToRun = [SELECT Id FROM User WHERE FirstName = 'sky test' LIMIT 1];
        
        System.runAs (userToRun) {
            Procedure__c proc = [SELECT Id, Amount_Paid__c FROM Procedure__c LIMIT 1]; 
            Account acc = [SELECT Id FROM Account LIMIT 1];
            /*  introducir un pago  */
            Id rpRT = DCUtils.getRecTypeIdByDevName('Payments__c', 'Pago_Regular');
            List<Payments__c> pagoList = new List<Payments__c>();
            Payments__c pago = new Payments__c(Agencia__c = acc.Id,
                                              Payment_Method__c = 'Efectivo',
                                              Procedure__c = proc.Id,
                                              RecordTypeId = rpRT,
                                              Amount__c = 50);
            pagoList.add(pago) ;                                
            pago = new Payments__c(Agencia__c = acc.Id,
                                   Payment_Method__c = 'Efectivo',
                                   Procedure__c = proc.Id,
                                   RecordTypeId = rpRT,
                                   Amount__c = 150);
            pagoList.add(pago) ;
    
            Test.startTest();
            // valor incial antes de insertar transaccion
            System.assert( proc.Amount_Paid__c == null ||
                           proc.Amount_Paid__c == 0, 
                           'Amount_Paid__c valor inicial inesperado');
            insert pagoList;
            proc = [SELECT Id, Amount_Paid__c FROM Procedure__c LIMIT 1]; 
            System.assert(proc.Amount_Paid__c == 200, 'Amount_Paid__c no fue actualizado');
            
            /*  al actualizar el monto del pago se actualiza el monto de total del procedure */
            pagoList[0].Amount__c = 150;
            update pagoList[0];
            proc = [SELECT Id, Amount_Paid__c FROM Procedure__c LIMIT 1]; 
            System.assert(  proc.Amount_Paid__c == 300, 'Amount_Paid__c no fue actualizado');
            
            /*verificar sharing manual */
            String gId = dcPublicGroupUtils.getGroupAgencyMayorista();
            List<Payments__Share> sharedRecords = [SELECT Id, ParentId, UserOrGroupId 
                                                      FROM Payments__Share 
                                                      WHERE UserOrGroupId=:gId AND
                                                            RowCause='Manual' AND
                                                           (parentId=:pagoList[0].Id OR 
                                                            parentId=:pagoList[1].Id)];
            System.debug(sharedRecords.size());
            System.assert(sharedRecords.size()>=2, 'los pagos no fueron compartidos con el grupo de la agencia mayorista.');
  
            delete pagoList[0];
            proc = [SELECT Id, Amount_Paid__c FROM Procedure__c LIMIT 1]; 
            System.debug(proc.Amount_Paid__c);
            System.assert(proc.Amount_Paid__c == 150, 'Amount_Paid__c no fue actualizado');
            Test.stopTest();
        }
    }

    @isTest
    static void updateBatchPaymentsTest(){

        User userToRun = [SELECT Id FROM User WHERE FirstName = 'sky test' LIMIT 1];
        
        System.runAs (userToRun) {
            Batch__c batch = [SELECT Id, Amount_Paid__c FROM Batch__c LIMIT 1]; 
            Account acc = [SELECT Id FROM Account LIMIT 1];
            /*  introducir un pago  */
            Id rpRT = DCUtils.getRecTypeIdByDevName('Payments__c', 'Pago_Regular');
            List<Payments__c> pagoList = new List<Payments__c>();
            Payments__c pago = new Payments__c(Agencia__c = acc.Id,
                                              Payment_Method__c = 'Efectivo',
                                              Batch__c = batch.Id,
                                              RecordTypeId = rpRT,
                                              Amount__c = 50);
            pagoList.add(pago) ;                                
            pago = new Payments__c(Agencia__c = acc.Id,
                                   Payment_Method__c = 'Efectivo',
                                   Batch__c = batch.Id,
                                   RecordTypeId = rpRT,
                                   Amount__c = 150);
            pagoList.add(pago) ;
    
            Test.startTest();
            // valor incial antes de insertar transaccion
            System.assert( batch.Amount_Paid__c == null ||
                           batch.Amount_Paid__c == 0, 
                           'Amount_Paid__c valor inicial inesperado');
            insert pagoList;
            batch = [SELECT Id, Amount_Paid__c FROM Batch__c LIMIT 1]; 
            System.assert(batch.Amount_Paid__c == 200, 'Amount_Paid__c no fue actualizado');
            
            /*  al actualizar el monto del pago se actualiza el monto de total del procedure */
            pagoList[0].Amount__c = 150;
            update pagoList[0];
            batch = [SELECT Id, Amount_Paid__c FROM Batch__c LIMIT 1]; 
            System.assert(batch.Amount_Paid__c == 300, 'Amount_Paid__c no fue actualizado');
            
            /*verificar sharing manual */
            String gId = dcPublicGroupUtils.getGroupAgencyMayorista();
            List<Payments__Share> sharedRecords = [SELECT Id, ParentId, UserOrGroupId 
                                                      FROM Payments__Share 
                                                      WHERE UserOrGroupId=:gId AND
                                                   			RowCause='Manual' AND
                                                           (parentId=:pagoList[0].Id OR 
                                                            parentId=:pagoList[1].Id)];
            System.debug(sharedRecords.size());
            System.assert(sharedRecords.size()>=2, 'los pagos no fueron compartidos con el grupo de la agencia mayorista.');
  
            delete pagoList[0];
            batch = [SELECT Id, Amount_Paid__c FROM Batch__c LIMIT 1]; 
            System.debug(batch.Amount_Paid__c);
            System.assert(batch.Amount_Paid__c == 150, 'Amount_Paid__c no fue actualizado');
    
            Test.stopTest();
        }
        
    }
}