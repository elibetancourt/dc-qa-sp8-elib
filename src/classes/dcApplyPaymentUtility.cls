/**
 * @File Name          : dcApplyPaymentUtility.cls
 * @Description        :
 * @Author             : Silvia Velazquez
 * @Group              :
 * @Last Modified By   : Elizabeth Betancourt Herrera
 * @Last Modified On   : 11-04-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    20/8/2020   Silvia Velazquez     Initial Version
 * 1.1   08-24-2020   Ibrahim Napoles          Validate CRUD permission before SOQL/DML operation.
 **/
public with sharing class dcApplyPaymentUtility {

    /**
     * @description Retrieves a Batch records info by a given Id.
     * @author Silvia Velazquez | 20/8/2020
     * @param batchId
     * @return dcApplyPaymentController.Batch_Wrapper
     **/
    public static dcApplyPaymentController.Batch_Wrapper getBatchData(String batchId){
        if(!(Schema.sObjectType.Batch__c.isAccessible() &&
                Schema.sObjectType.Batch__c.fields.Id.isAccessible() &&
                Schema.sObjectType.Batch__c.fields.Name.isAccessible() &&
                Schema.sObjectType.Batch__c.fields.Amount_Paid__c.isAccessible() &&
                Schema.sObjectType.Batch__c.fields.Amount_Due__c.isAccessible() &&
                Schema.sObjectType.Batch__c.fields.Monto_Aplicado__c.isAccessible() &&
                Schema.sObjectType.Batch__c.fields.Amount_Pending_to_Apply__c.isAccessible() &&
                Schema.sObjectType.Batch__c.fields.Recieved_By__c.isAccessible() &&
                Schema.sObjectType.Batch__c.fields.Status__c.isAccessible() &&
                Schema.sObjectType.Batch_Item__c.isAccessible() &&
                Schema.sObjectType.Batch_Item__c.fields.Id.isAccessible() &&
                Schema.sObjectType.Batch_Item__c.fields.Batch__c.isAccessible() &&
                Schema.sObjectType.Batch_Item__c.fields.Procedure__c.isAccessible() &&
                Schema.sObjectType.Batch_Item__c.fields.Payment_Date__c.isAccessible() &&
                Schema.sObjectType.Batch_Item__c.fields.Applied_Cash__c.isAccessible() &&
                Schema.sObjectType.Procedure__c.isAccessible() &&
                Schema.sObjectType.Procedure__c.fields.Id.isAccessible() &&
                Schema.sObjectType.Contact.isAccessible() &&
                Schema.sObjectType.Contact.fields.Name.isAccessible() &&
                Schema.sObjectType.Procedure__c.fields.RecordTypeId.isAccessible() &&
                Schema.sObjectType.RecordType.isAccessible() &&
                Schema.sObjectType.RecordType.fields.Name.isAccessible() &&
                Schema.sObjectType.Procedure__c.fields.Customer__c.isAccessible() &&
                Schema.sObjectType.Procedure__c.fields.CreatedDate.isAccessible() &&
                Schema.sObjectType.Procedure__c.fields.Consulate_Payment__c.isAccessible() &&
                Schema.sObjectType.Procedure__c.fields.Agency_Commission__c.isAccessible() &&
                Schema.sObjectType.Procedure__c.fields.Wholesaler_Payment__c.isAccessible() &&
                Schema.sObjectType.Procedure__c.fields.Name.isAccessible())) {
            DCException.throwPermiException('dcApplyPaymentUtility.getBatchData');
        }
        
        List<Batch__c> batchs = [SELECT Name, Amount_Paid__c, Amount_Due__c, Monto_Aplicado__c, Amount_Pending_to_Apply__c,
                                        Recieved_By__c, Status__c,
                                 (SELECT Id, Name, Applied_Cash__c, Procedure__r.Id, Procedure__r.Name, Procedure__r.Customer__r.Name, 
                                 Procedure__r.RecordType.Name, Procedure__r.CreatedDate, Procedure__r.Consulate_Payment__c, 
                                 Procedure__r.Agency_Commission__c, Procedure__r.Wholesaler_Payment__c
                                  FROM Batch_Items__r)
                                 FROM Batch__c WHERE Id = :batchId];

        dcApplyPaymentController.Batch_Wrapper result;

        if(batchs.size() > 0) {
            Batch__c batch = batchs[0];
            result = new dcApplyPaymentController.Batch_Wrapper();
            result.batch_Id = batchId;
            result.batch_Name = batch.Name;
            result.amount_Paid = batch.Amount_Paid__c; //Monto Pagado
            result.amount_Due = batch.Amount_Due__c;  //Monto Pendiente por Pagar
            result.amount_Applied = batch.Monto_Aplicado__c; //Monto Aplicado
            result.amount_Pending_to_Apply = batch.Amount_Pending_to_Apply__c; //Monto pendiente por aplicar
            result.procedure_count_to_Apply = batch.Batch_Items__r.size(); //Cantidad de Tramites pendientes por aplicar
            result.have_Payment = true;
            result.is_Recived = (batch.Recieved_By__c !=null);
            result.is_Closed = (batch.Status__c == 'Cerrado');
            result.batch_items = new List<dcApplyPaymentController.Batch_Item_Wrapper>();
            /* si no hay monto pagado comprobar si no hay ningun pago asociado */
            if (batch.Amount_Paid__c==0){
                result.have_Payment = paymentsExists(batchId);
            }
            for(Batch_Item__c item: batch.Batch_Items__r) {
                dcApplyPaymentController.Batch_Item_Wrapper itemWrapper = new dcApplyPaymentController.Batch_Item_Wrapper();
                itemWrapper.batch_item_id = item.Id;
                itemWrapper.batch_item_Name = item.Name;
                itemWrapper.procedure_id = item.Procedure__r.Id;
                itemWrapper.procedure_Name = item.Procedure__r.Name;
                itemWrapper.procedureNameLink = '/'+item.Procedure__r.Id;
                itemWrapper.procedure_Contact = item.Procedure__r.Customer__r.Name;
                itemWrapper.procedure_type = item.Procedure__r.RecordType.Name;
                itemWrapper.procedure_date = item.Procedure__r.CreatedDate.format('MM/dd/yyyy HH:mm');
                itemWrapper.procedure_consulate_payment = item.Procedure__r.Consulate_Payment__c;
                itemWrapper.procedure_agency_comission = item.procedure__r.Agency_Commission__c;
                itemWrapper.procedure_distributor_payment = item.Procedure__r.Wholesaler_Payment__c;
                itemWrapper.procedure_Applied_Payment = (item.Applied_Cash__c== null || item.Applied_Cash__c == 0 );
                result.batch_items.add(itemWrapper);
            }    
        }
                
        return result;
    }

    /**
    * @description Apply Payments to selected batch_items
    * @author Elizabeth Betancourt | 11-03-2020 
    * @param batchId 
    * @return Boolean: true if have payment asociated, false  if not
    **/
    private static Boolean paymentsExists(String batchId){
        if(!(Schema.sObjectType.Payments__c.isAccessible() && 
             Schema.sObjectType.Payments__c.fields.Id.isAccessible() && 
             Schema.sObjectType.Payments__c.fields.Batch__c.isAccessible())) {
            DCException.throwPermiException('dcApplyPaymentUtility.paymentsExists');
        }

        List<Payments__c> payList = [SELECT Id FROM Payments__c WHERE Batch__c<>null AND Batch__c =: batchId];
        return payList.size()>0;
    }

    /**
    * @description Apply Payments to selected batch_items
    * @author Silvia Velazquez | 08-27-2020 
    * @param items 
    * @return String 
    **/
    public static String applyPayment(dcApplyPaymentController.ItemsToApply_Wrapper items){
        if(!(Schema.sObjectType.Batch__c.isAccessible() &&
             Schema.sObjectType.Batch__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Batch__c.fields.Recipient__c.isAccessible() &&
             Schema.sObjectType.Account.fields.Name.isAccessible() &&
             Schema.sObjectType.Account.fields.Id.isAccessible() &&
             Schema.sObjectType.Batch__c.fields.Amount_Pending_to_Apply__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Batch__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Name.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Applied_Cash__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Payment_Date__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Procedure__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Consulate_Payment__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Agency_Payment_Date__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Agency_Commission__c.isAccessible())) {
            DCException.throwPermiException('CustomMetadataUploadController.loadCustomMetadataMetadata');
        }
        
        List<Batch__c> batches = [SELECT Id, Recipient__r.Name, Recipient__r.Id, Amount_Pending_to_Apply__c,
                                  (SELECT Id, Name, Applied_Cash__c, Payment_Date__c, Procedure__c,
                                   Procedure__r.Consulate_Payment__c, Procedure__r.Agency_Commission__c,Procedure__r.Agency_Payment_Date__c
                                   FROM Batch_Items__r WHERE Id IN :items.itemsToApply AND (Applied_Cash__c=0 OR Applied_Cash__c = null))
                                  FROM Batch__c WHERE Id = :items.batchId];

        String result;
        System.debug('Lote: '+ items.batchId);
        System.debug('Articulos de Lote: '+ items.itemsToApply);

        if(batches.size() > 0) {
            Batch__c batch = batches[0];
            if(batch.Batch_Items__r.size() > 0 && batch.Amount_Pending_to_Apply__c > 0) {  //Validating if exists an amount pending to apply
                List<SObject> records = new List<SObject>(); // List to Update Batch_Items and Procedures and to Create Transactions
                //List<Transaction__c> transactions = new List<Transaction__c>(); //List to Insert new Transactions
                String agencyId = (batch.Recipient__r.Name == 'District Cuba') ? batch.Recipient__r.Id : null;

                for(Batch_Item__c item : batch.Batch_Items__r) {  //For each item, 4 elements are inserted in records list to finally make a DML statement over the list.
                    Decimal amount = (item.Procedure__r.Agency_Commission__c != null) ? item.Procedure__r.Agency_Commission__c : 0;
                    Transaction__c creditTransaction = new Transaction__c(Agency__c = agencyId,
                                                                          GL_Account__c = 'Operaciones',
                                                                          Description__c = 'Transacción automática para la aplicación de un pago.',
                                                                          Date__c = Datetime.now(),
                                                                          Lote__c = batch.Id,
                                                                          Batch_Item__c = item.Id,
                                                                          Monto__c = amount,
                                                                          Procedure__c = item.Procedure__c);
                    records.add(creditTransaction);
                    Transaction__c debitTransaction = new Transaction__c(Agency__c = agencyId,
                                                                         GL_Account__c = 'Escrow',
                                                                         Description__c = 'Transacción automática para la aplicación de un pago.',
                                                                         Date__c = Datetime.now(),
                                                                         Lote__c = batch.Id,
                                                                         Batch_Item__c = item.Id,
                                                                         Monto__c = -1 * amount);
                    records.add(debitTransaction);
                    Transaction__c debitEscrowTransaction = new Transaction__c(Agency__c = agencyId,
                                                                         GL_Account__c = 'Escrow',
                                                                         Description__c = 'Transacción automática para la aplicación de un pago.',
                                                                         Date__c = Datetime.now(),
                                                                         Lote__c = batch.Id,
                                                                         Batch_Item__c = item.Id,
                                                                         Monto__c = -1 * item.Procedure__r.Consulate_Payment__c);
                    records.add(debitEscrowTransaction);
                    Transaction__c creditEscrowTransaction = new Transaction__c(Agency__c = agencyId,
                                                                         GL_Account__c = 'Escrow',
                                                                         Description__c = 'Transacción automática para la aplicación de un pago.',
                                                                         Date__c = Datetime.now(),
                                                                         Lote__c = batch.Id,
                                                                         Batch_Item__c = item.Id,
                                                                         Monto__c = item.Procedure__r.Consulate_Payment__c,
                                                                         Procedure__c = item.Procedure__c);
                    records.add(creditEscrowTransaction);

                    item.Applied_Cash__c = amount + item.Procedure__r.Consulate_Payment__c;
                    item.Payment_Date__c = Datetime.now();
                    item.Procedure__r.Agency_Payment_Date__c = Date.today();
                    records.add(item);
                    records.add(item.Procedure__r);
                }

                records.sort(); //Sorting by Sobject Type to prevent Error: "System.TypeException: Cannot have more than 10 chunks in a single operation."
                try {
                    //upsert records;
                    CRUDEnforce.dmlUpsert(records, 'dcApplyPaymentUtility.applyPayment');
                    result = 'OK';
                } catch (DmlException e) {
                    return 'Error: A DML exception has occurred: ' + e.getMessage();
                }
            }
            else{
                result = 'Error: No hay Articulos de Lote pendientes por aplicar pagos ó El Lote no dispone de un monto pendiente por aplicar.';
            }
        }
        else{
            result = 'Error: No se encontró un Lote con el valor del Id proporcionado.';
        }

        return result;
     
    }

    /**
    * @description remove Applied Payments and generate corresponding debit and credit transactions 
    * to anulate corresponding transactions in apply payment process
    * @author Elizabeth Betancourt | 09-16-2020 
    * @param items 
    * @return String 
    **/
    public static String removeAppliedPayment(dcApplyPaymentController.ItemsToApply_Wrapper items){
        if(!(Schema.sObjectType.Batch__c.isAccessible() &&
             Schema.sObjectType.Batch__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Batch__c.fields.Recipient__c.isAccessible() &&
             Schema.sObjectType.Account.fields.Name.isAccessible() &&
             Schema.sObjectType.Account.fields.Id.isAccessible() &&
             Schema.sObjectType.Batch__c.fields.Amount_Pending_to_Apply__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Batch__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Name.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Applied_Cash__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Payment_Date__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Procedure__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Consulate_Payment__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Agency_Payment_Date__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Agency_Commission__c.isAccessible())) {
            DCException.throwPermiException('CustomMetadataUploadController.loadCustomMetadataMetadata');
        }
        
        List<Batch_Item__c> batchItems = [SELECT Id, Name, Applied_Cash__c, Payment_Date__c, 
                                                 Batch__c, Batch__r.Recipient__r.Name, Batch__r.Recipient__r.Id, Batch__r.Amount_Pending_to_Apply__c, 
                                                 Procedure__c, Procedure__r.Consulate_Payment__c, 
                                                 Procedure__r.Agency_Commission__c, Procedure__r.Agency_Payment_Date__c
                                          FROM Batch_Item__c 
                                          WHERE Id IN :items.itemsToApply and Batch__c = :items.batchId];

        String result;
        System.debug('Lote: '+ items.batchId);
        System.debug('Articulos de Lote: '+ items.itemsToApply);

        if(batchItems.size() == 0) {
            return 'Error: No se encontraron datos de Lote o Artículos de Lote.';
        }
        List<SObject> records = new List<SObject>(); // List to Update Batch_Items and Procedures and to Create Transactions
        for(Batch_Item__c item : batchItems) {

            if(item.Applied_Cash__c== null || item.Applied_Cash__c == 0) {  //Validate if exists payment applied
                return 'Error: El Artículo de Lote no tiene pago aplicado.';
            }
                        
            String agencyId = (item.Batch__r.Recipient__r.Name == 'District Cuba') ? item.Batch__r.Recipient__r.Id : null;

            //For each item, 4 elements are inserted in records list to finally make a DML statement over the list.
            Decimal amount = (item.Procedure__r.Agency_Commission__c != null) ? item.Procedure__r.Agency_Commission__c : 0;
            Transaction__c creditTransaction = new Transaction__c(Agency__c = agencyId,
                                                                GL_Account__c = 'Operaciones',
                                                                Description__c = 'Transacción automática para cancelar pago de Artículo de Lote',
                                                                Date__c = Datetime.now(),
                                                                Lote__c = item.Batch__c,
                                                                Batch_Item__c = item.Id,
                                                                Monto__c = -1 * amount,
                                                                Procedure__c = item.Procedure__c);
            records.add(creditTransaction);
            Transaction__c debitTransaction = new Transaction__c(Agency__c = agencyId,
                                                                GL_Account__c = 'Escrow',
                                                                Description__c = 'Transacción automática para cancelar pago de Artículo de Lote',
                                                                Date__c = Datetime.now(),
                                                                Lote__c = item.Batch__c,
                                                                Batch_Item__c = item.Id,
                                                                Monto__c = amount);
            records.add(debitTransaction);
            Transaction__c debitEscrowTransaction = new Transaction__c(Agency__c = agencyId,
                                                                GL_Account__c = 'Escrow',
                                                                Description__c = 'Transacción automática para cancelar pago de Artículo de Lote',
                                                                Date__c = Datetime.now(),
                                                                Lote__c = item.Batch__c,
                                                                Batch_Item__c = item.Id,
                                                                Monto__c = -1 * item.Procedure__r.Consulate_Payment__c,
                                                                Procedure__c = item.Procedure__c);
            records.add(debitEscrowTransaction);
            Transaction__c creditEscrowTransaction = new Transaction__c(Agency__c = agencyId,
                                                                GL_Account__c = 'Escrow',
                                                                Description__c = 'Transacción automática para cancelar pago de Artículo de Lote',
                                                                Date__c = Datetime.now(),
                                                                Lote__c = item.Batch__c,
                                                                Batch_Item__c = item.Id,
                                                                Monto__c = item.Procedure__r.Consulate_Payment__c);
            records.add(creditEscrowTransaction);

            item.Applied_Cash__c = 0;
            item.Payment_Date__c = null;
            item.Procedure__r.Agency_Payment_Date__c = null;
            records.add(item);
            records.add(item.Procedure__r);
        }   
        records.sort(); //Sorting by Sobject Type to prevent Error: "System.TypeException: Cannot have more than 10 chunks in a single operation."
        try {
            //upsert records;
            CRUDEnforce.dmlUpsert(records, 'dcApplyPaymentUtility.removeAppliedPayment');
            return 'OK';
        } catch (DmlException e) {
            return 'Error: A DML exception has occurred: ' + e.getMessage();
        }
    }
}