/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-02-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   11-02-2020   Elizabeth Betancourt Herrera   Initial Version
**/
public without sharing class dcManifestTriggerHandler {
    Boolean isBefore;
    Boolean isAfter;
    Boolean isInsert;
    Boolean isDelete;
    Boolean isUpdate;
    List<Manifest__c> newList {get; set;}
    List<Manifest__c> oldList {get; set;}
    Map<Id, Manifest__c> oldMap {get; set;}
    
    public dcManifestTriggerHandler() {
        newList = trigger.new;
        oldList = trigger.old;
        oldMap = (Map<Id, Manifest__c>)trigger.oldMap;
        this.isBefore = trigger.isBefore;
        this.isAfter = trigger.isAfter;
        this.isInsert = trigger.isInsert;
        this.isUpdate = trigger.isUpdate;
        this.isDelete = trigger.isDelete;
    }

    public void ManifestTriggerActions (){
        
        if(isAfter && isInsert) {
            //share account whith user in public group
            shareRecord();
        }                           
    }

    public void shareRecord (){
        
       Map<String, List<sObject>> groupManifestList = getGroupListToShare();
       System.debug('Lista tarifa de envio a compartir') ;
       for(String groupId: groupManifestList.keySet()){
           System.debug('groupId --> '+groupId) ;
           System.debug('total a compratir --> '+groupManifestList.get(groupId).size()) ;
           dcSharingManager.shareRecords(groupManifestList.get(groupId), 
                                         'Manifest__Share', 'Edit', groupId, 'Manual');
       }
    }

    public Map<String, List<sObject>> getGroupListToShare (){
        
        Map<String, List<sObject>> groupMap = new Map<String, List<sObject>>();
        
        String groupIdAgencyMayorista = dcPublicGroupUtils.getGroupAgencyMayorista();
        
        if(String.isNotEmpty(groupIdAgencyMayorista)) groupMap.put(groupIdAgencyMayorista, newList);

        return groupMap;
    }
}