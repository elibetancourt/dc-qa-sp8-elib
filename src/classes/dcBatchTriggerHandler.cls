/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-02-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   11-02-2020   Elizabeth Betancourt Herrera   Initial Version
**/
public without sharing class dcBatchTriggerHandler {
    Boolean isBefore;
    Boolean isAfter;
    Boolean isInsert;
    Boolean isDelete;
    Boolean isUpdate;
    List<Batch__c> newList {get; set;}
    List<Batch__c> oldList {get; set;}
    Map<Id, Batch__c> oldMap {get; set;}
    
    public dcBatchTriggerHandler() {
        newList = trigger.new;
        oldList = trigger.old;
        oldMap = (Map<Id, Batch__c>)trigger.oldMap;
        this.isBefore = trigger.isBefore;
        this.isAfter = trigger.isAfter;
        this.isInsert = trigger.isInsert;
        this.isUpdate = trigger.isUpdate;
        this.isDelete = trigger.isDelete;
    }

    public void BatchTriggerActions (){
        
        if(isAfter && isInsert) {
            //share account whith user in public group
            shareRecord();
        }                           
    }

    public void shareRecord (){
        
       Map<String, List<sObject>> groupLoteList = getGroupListToShare();
       System.debug('Lista lote a compartir') ;
       for(String groupId: groupLoteList.keySet()){
           System.debug('groupId --> '+groupId) ;
           System.debug('total a compratir --> '+groupLoteList.get(groupId).size()) ;
           dcSharingManager.shareRecords(groupLoteList.get(groupId), 
                                         'Batch__Share', 'Edit', groupId, 'Manual');
       }
    }

    public Map<String, List<sObject>> getGroupListToShare (){
        Map<String, String> agencyGroupMap;
        System.debug('list size --> '+newList.size()) ;
        if(newList.size() == 1){
            agencyGroupMap = dcPublicGroupUtils.getAgencyGroupMap(newList[0].Agency__c);
        } else {
            agencyGroupMap = dcPublicGroupUtils.getAgencyGroupMap('');
        }

        Map<String, List<sObject>> groupLoteList = new Map<String, List<sObject>>();
        
        for( Batch__c newBatch: newList){
            List<sObject> batchToShareList = new List<sObject>{newBatch};
            if(agencyGroupMap != null && agencyGroupMap.containsKey(newBatch.Agency__c)){
                String groupId = agencyGroupMap.get(newBatch.Agency__c);
                groupLoteList.put(groupId, batchToShareList);
            }   
        }

        return groupLoteList;
    }
}