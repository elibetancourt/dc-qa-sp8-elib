/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-02-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   11-02-2020   Elizabeth Betancourt Herrera   Initial Version
**/
trigger dcTrackingErrorLogTrigger on Tracking_Error_Log__c (after insert) {
    new dcTrackingErrorLogTriggerHandler().TrackingErrorLogTriggerActions();
}