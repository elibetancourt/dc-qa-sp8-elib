/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-03-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   10-26-2020   Elizabeth Betancourt Herrera   Initial Version
**/
trigger dcTrackingTrigger on Tracking__c (after insert, after update, after delete) {
    new dcTrackingTriggerHandler().TrackingTriggerActions();
}