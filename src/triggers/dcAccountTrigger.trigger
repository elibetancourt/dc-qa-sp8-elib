/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 10-29-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   10-29-2020   Elizabeth Betancourt Herrera   Initial Version
**/
trigger dcAccountTrigger on Account (before insert, after insert) {
    new dcAccountTriggerHandler().accountTriggerActions();  
}