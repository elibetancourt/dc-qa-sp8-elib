/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 11-03-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   09-18-2020   Elizabeth Betancourt Herrera   Initial Version
**/
trigger dcTransactionTrigger on Transaction__c (after insert, after update, after delete) {
    new dcTransactionTriggerHandler().TransactionTriggerActions();   
}