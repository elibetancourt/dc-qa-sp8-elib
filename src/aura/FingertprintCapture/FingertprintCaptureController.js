({
	doInit: function(component, event, helper) {
		var action;
		action = component.get("c.getFingerprint");
		action.setParams({ 
			recordId: component.get("v.recordId")
		});
		action.setCallback(this, function(response) {
			switch (response.getState()) {
				case "SUCCESS":
					component.set("v.currentUrl", response.getReturnValue());
					break;
				case "ERROR":
					component.set("v.currentUrl", null);
					break;
			}
		});
		$A.enqueueAction(action);
	},
	getFingerprints: function(component, event, helper) {
		component.set("v.stage", "waiting");
		$.ajax({
			method: "POST",
			url: $("#scannerUrl").val(),
			data: { 
				"Quality": "50",
				"time": "2pm",
				"licstr": "",
				"templateFormat": "ISO",
				"imageWSQRate": "0.75"
			},
			dataType: "json"
		})
		.done(function(result) {
			if (result.ErrorCode)
				component.set("v.stage", "error");
			else {
				console.log(result);
				component.set("v.stage", "done");
				component.set("v.imageData", result.BMPBase64);
				component.set("v.imageSrc", 
					"data:image/bmp;base64," + result.BMPBase64);
			}
		})	
		.fail(function(error) {
			component.set("v.stage", "error");
		});
	},
	saveFingerprints: function(component, event, helper) {
		var action;
		component.set("v.stage", "saving");
		action = component.get("c.saveFingerprint");
		action.setParams({ 
			recordId: component.get("v.recordId"),
			imageData: component.get("v.imageData")
		});
		action.setCallback(this, function(response) {
			switch (response.getState()) {
				case "SUCCESS":
					component.set("v.stage", "saved");
					component.set("v.currentUrl", response.getReturnValue());
					component.set("v.imageSrc", null);
					component.set("v.imageData", null);
					setTimeout(function() {
						component.set("v.stage", "idle");
					}, 5000);
					break;
				case "ERROR":
					component.set("v.stage", "saveError");
					break;
			}
		});
		$A.enqueueAction(action);
	},
	cancelFingerprints: function(component, event, helper) {
		component.set("v.stage", "idle");
	}
})