/**
 * @File Name          : SP_Object_DataTableController.js
 * @Description        :
 * @Author             : Ibrahim Napoles
 * @Group              :
 * @Last Modified By   : Ibrahim Napoles
 * @Last Modified On   : 12/19/2019, 9:58:21 AM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    12/6/2019   Ibrahim Napoles     Initial Version
 **/
({
  init: function(component, event, helper) {
    if (!component.get("v.noRecords"))
      component.find("dataList").set("v.isLoading", false);
      //alert(JSON.stringify(component.get("v.data")));
      //alert(JSON.stringify(component.get("v.columns")));
      
  },
  updateLoadMoreStatus: function(component, event, helper) {
    component.find("dataList").set("v.isLoading", false);
  },
  updateColumnSorting: function(component, event, helper) {
    var fieldName = event.getParam("fieldName");
    var sortDirection = event.getParam("sortDirection");

    component.set("v.sortBy", fieldName);
    component.set("v.sortDirection", sortDirection);

    helper.fireComponentEvent(component, "isSort");
  },
  loadMore: function(component, event, helper) {
    component.find("dataList").set("v.isLoading", true);
    helper.fireComponentEvent(component, "isLoadMore");
  },
  handleRowAction: function(component, event, helper) {
      var action = event.getParam('action');
      var row = event.getParam('row'); 
      //alert(JSON.stringify(row));
      helper.fireDataTableEventAction(component, row, action)
  }
});