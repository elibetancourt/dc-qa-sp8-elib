/**
 * @File Name          : dcBatchProceduresDataTableController.js
 * @Description        :
 * @Author             : Elizabeth Betancourt Herrera
 * @Group              :
 * @Last Modified By   : Elizabeth Betancourt Herrera
 * @Last Modified On   : 8/25/2020, 2:47:26 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    25/8/2020   Elizabeth Betancourt Herrera     Initial Version
 **/
({
      init: function(component, event, helper) {
        if (!component.get("v.noRecords"))
          component.find("dataList").set("v.isLoading", false);
          //alert(JSON.stringify(component.get("v.data")));
          //alert(JSON.stringify(component.get("v.columns")));    
      },
        
      updateSelected: function (component, event, helper) {
            
            var selectedRows = event.getParam('selectedRows');
            
            helper.fireSelectedEvent(component, selectedRows);
      },
    
       handleRowAction: function(component, event, helper) {
     	 var action = event.getParam('action');
     	 var row = event.getParam('row'); 
      	//alert(JSON.stringify(row));
      	helper.fireDataTableEventAction(component, row, action)
  }
});