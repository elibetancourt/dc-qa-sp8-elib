/**
 * @File Name          : dcBatchProceduresDataTableController.js
 * @Description        :
 * @Author             : Elizabeth Betancourt Herrera
 * @Group              :
 * @Last Modified By   : Elizabeth Betancourt Herrera
 * @Last Modified On   : 8/25/2020, 2:47:26 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    25/8/2020   Elizabeth Betancourt Herrera     Initial Version
 **/
({
    fireSelectedEvent: function(component, selectedRows) {
        var myEvent = component.getEvent("selectEvent");
        let paramRows = Object.assign({}, selectedRows); ;
        myEvent.setParams({
          rows: paramRows
        });
    
        myEvent.fire();
  },
  fireDataTableEventAction: function(component, row, action) {
    var myEvent = component.getEvent("actionEvent");
    let paramRow = Object.assign({}, row); ;
      
    myEvent.setParams({
      row: paramRow
    });

    myEvent.fire();
  }
});