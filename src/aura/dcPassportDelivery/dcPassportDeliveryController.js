({
	invoke : function(component, event, helper) {
        //alert("invoke");
        
        var pdfPage = component.get("v.pdfPage");
        var idProcedure = component.get("v.idProcedure");
        
        //alert(pdfPage);
        //alert(idProcedure);
        
		var redirect = "/apex/dcDigitalSignature?recordId="+idProcedure;
        redirect = redirect + "&obn=Procedure__c&afn=Signature_Attachment_Id__c&fln=Entrega&nurl="
        redirect = redirect + pdfPage+"?id="+idProcedure;
        //alert(redirect);
        
        //helper.redirect(component, helper);
        var urlEvent = $A.get("e.force:navigateToURL");
        //alert (urlEvent);
        //var redirect = "/apex/dcDigitalSignature?recordId=a003h000005qSgdAAE&obn=Procedure__c&afn=Signature_Attachment_Id__c&fln=Recibo&nurl=/apex/dcPassportDelivery?id=a003h000005qSgdAAE";
        urlEvent.setParams({
       				 "url": redirect,
                     "isredirect": "true"
       			});
        
    	urlEvent.fire();
	}
})