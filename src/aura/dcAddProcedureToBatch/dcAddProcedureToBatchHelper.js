({
    serverAction: function(cmp, method, params) {
            var self = this;
        
            return new Promise(function(resolve, reject) {
            var action = cmp.get("c." + method);
        
            if (params != null) action.setParams(params);
        
            action.setCallback(self, function(response) {
                console.log("Begin Callback" + Date(Date.now()));
                var state = response.getState();
        
                if (state == "SUCCESS") resolve.call(this, response.getReturnValue());
                else if (state == "ERROR") {
                var errors = response.getError();
                var title = cmp.get("v.title");
                this.showToast(cmp, title, errors[0].message, "error");
                
                reject.call(this, JSON.stringify(errors));
                }
            });
        
            $A.enqueueAction(action);
            console.log("Finish EnqueueAction" + Date(Date.now()));
            });
      },

      isValidDate: function(date){
          var d = new Date(date);
          var msc = d.getMilliseconds();
          
          return (!isNaN(msc));
      },

      formatDate: function(date) {  
               
        if (!this.isValidDate(date))  return '';
        
        var d = new Date(date);
        //alert(d);   
        var month = '' + (d.getUTCMonth() + 1),
            day = '' + d.getUTCDate(),
            year = d.getUTCFullYear();
        
        //alert(month); alert(day); alert(year);
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
        
        return [year, month, day].join('-'); 
        
    },
    
    updateSearchParamsWrapper: function(component) {
            var dateFrom = component.get("v.dateFrom");
            //alert(dateFrom);
            dateFrom = this.formatDate(dateFrom);
            //alert(dateFrom);
            var dateTo = component.get("v.dateTo");
            if (dateTo == null) dateTo = new Date();
            //alert(dateTo);
            dateTo = this.formatDate(dateTo);
            //alert(dateTo);
            var searchPattern = {
                status: component.get("v.status"),
                agency: component.get("v.agency"),
                filter: component.get("v.filter"),
                noBatch: component.get("v.noBatch"),
                noDistributor: component.get("v.noDistributor"),
                dateFrom: dateFrom,
                dateTo: dateTo,
                procedureTypes: component.get("v.procedureTypes"),
                pageSize: component.get("v.pageSize"),
                offset: component.get("v.offset"),
                sortBy: component.get("v.sortBy"),
                isDescending: component.get("v.isDescending")
            };
        
            component.set("v.searchPattern", searchPattern);
      },

      searchAction: function(component, concat) {
            var self = this;
        
            var search = component.get("v.searchPattern");
            //alert(JSON.stringify(search)); 
            return self
            .serverAction(component, "search", {
                searchPattern: JSON.stringify(search)
            })
            .then(function(results) {
                if (results.errorShow == true) {
                    component.set("v.errorShow", results.errorShow);
                    component.set("v.errorTitle", results.errorTitle);
                    component.set("v.errorMessage", results.errorMessage);
                    //alert(results.errorMessage);
                    self.showToast(component,results.errorTitle, results.errorMessage, "error");
                } else {
                    //component.set("v.recordCount", results.count);
                    //alert(JSON.stringify(results.procedureResults));
                    self.updateDataList(component, results.procedureResults, concat);
                    component.set("v.emptyList", results.noResult);
                }
                component.set("v.loading", false);
                var filterEnter = component.find("filterEnter") ;
                filterEnter.focus(); 
            });
      },
      searchAutomaticAction: function(component, event, helper) {
        component.set("v.loading", true);
        component.set("v.showDataTable", true);
        component.set("v.offset", 0);
    
        helper.updateSearchParamsWrapper(component);
    
        console.log("Init Search" + Date(Date.now()));
        var search = component.get("v.searchPattern");
        // alert(JSON.stringify(search));  
        helper
          .serverAction(component, "count", {
            searchPattern: JSON.stringify(search)
          })
          .then(function(results) {
            //alert(JSON.stringify(results));   
            if (results.errorShow == true) {
              component.set("v.errorShow", results.errorShow);
              component.set("v.errorTitle", results.errorTitle);
              component.set("v.errorMessage", results.errorMessage);
              helper.showToast(component, results.errorTitle, results.errorMessage, "error");
            } else {
              
              component.set("v.recordCount", results.count);
              helper.updateDataList(component, results.procedureResults, false);
              component.set("v.emptyList", results.noResult);
              //alert(JSON.stringify(results.procedureResults));
              //alert(JSON.stringify(results.noResult));
              //alert(JSON.stringify(component.get("v.columnFields"))); 
                
              console.log("Finish Count Server request" + Date(Date.now()));
              component.set("v.loading", false);
               
            }
          });
          
      },
      showToast: function(component, title, message, type) {
          
            var toastEvent = $A.get("e.force:showToast");
            //alert(toastEvent);
            if (toastEvent){
                toastEvent.setParams({
                            "title": title,
                            "message": message,
                            "type": type,
                            "duration": 1500,
                        });
                toastEvent.fire();
            }
            else{
                    var cmp = component.find('notifLib');
                    component.find('notifLib').showToast({
                        "variant": type,
                        "header":  title,
                        "message": message
                    });
            }
      },
      updateDataList: function(component, items, concat) {
            var self = this;
            var newList = items || [];
            if (newList.length == 0) {
            return;
            }
        
            if (concat) {
                
                var current = component.get("v.resultList") || [];
                newList = current.concat(newList);
                component.set("v.resultList", newList);
                var childCmp = component.find("childObjectDataTable");
                childCmp.finishLoadMore();
            } else {
                
                component.set("v.resultList", newList);
            }
            
            //var list = component.get("v.resultList");
            //alert(JSON.stringify(list));  
            
            var newOffset = newList.length;
        
            component.set("v.offset", newOffset);
            var recordCount = component.get("v.recordCount");
        
            var maxRows = component.get("v.maxRows");
        
            var maxLenght =
            recordCount > 0 && recordCount < maxRows ? recordCount : maxRows;
        
            if (newOffset >= maxLenght) {
                component.set("v.infLoading", false);
            }
      },
      setColumns: function(component){
        
            component.set("v.columnFields", [
            {
            label: "Numero de Tramite",
            fieldName: "procedureName",
            type: "text",
            sortable: true
            },
            {
            label: "Fecha del Tramite",
            fieldName: "procedureDate",
            type: "date",
            sortable: true,
            typeAttributes:{
                    year: "numeric",
                    month: "2-digit",
                    day: "2-digit"
                }
            },
            {
            label: "Tipo de Tramite",
            fieldName: "procedureType",
            type: "text",
            sortable: true
            },
            {
            label: "Nombre y Apellidos",
            fieldName: "fullName",
            type: "text"
            },
            {
            label: "Telefono Cliente",
            fieldName: "phone",
            type: "text"
            }
        ]);
    },
        
    changeSelected: function(component, event, helper){
         
        var rows = event.getParam('rows');
        //alert(JSON.stringify(rows));
        
        var selectedProcedureId = '';
        var selectedCount = 0;
        Object.values(rows).forEach(row => {
            selectedProcedureId += row.procedureId + ','; 
            ++selectedCount;
        });
        
        //alert(selectedProcedureId);
        component.set("v.selectedProcedureId", selectedProcedureId); 
        //alert(selectedCount);     
        component.set("v.selectedCount", selectedCount);    
    },
    
    reset: function(component, helper) {
        component.set("v.loading", true);
    
        component.set("v.filter", "");
        component.set("v.dateFrom", null);
        component.set("v.dateTo", null);
        var procedureTypes = [];
        component.set("v.procedureTypes", procedureTypes);
        
        component.set("v.pageSize", 25);
        component.set("v.offset", 0);
        component.set("v.sortBy", "Name");
        component.set("v.isDescending", true);
        
        var selectedRows=[];
        component.set("v.selectedRows", selectedRows);
        component.set("v.resultList", []);
        component.set("v.emptyList", true);
        component.set("v.selectedCount", 0);
        component.set("v.recordCount", 0);
        component.set("v.infLoading", true);
    },

    initData: function(component, helper) {
        component.set("v.loading", true);
    
        component.set("v.pageSize", 25);
        component.set("v.offset", 0);
        component.set("v.sortBy", "Name");
        component.set("v.isDescending", true);
    
        //component.set("v.showDataTable", false);
        var selectedRows=[];
        component.set("v.selectedRows", selectedRows);
        component.set("v.resultList", []);
        component.set("v.emptyList", true);
        component.set("v.selectedCount", 0);
        component.set("v.recordCount", 0);
        component.set("v.infLoading", true);
    }
})