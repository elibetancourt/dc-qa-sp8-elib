({
    init: function(component, event, helper) {
        
        helper.reset(component, helper);    
        helper.setColumns(component);    
        helper.searchAutomaticAction(component, event, helper);  
        component.set("v.showDataTable", true);
        component.set("v.loading", false);
     },
    
    handleComponentEvent: function(component, event, helper) {
      if (event.getParam("isSort")) {
        var sortFieldName = "";
        //alert(event.getParam("sortBy"));
        switch (event.getParam("sortBy")) {
          
          case "procedureName": 
            sortFieldName = "Name";
            break; 
          case "procedureDate":  
            sortFieldName = "CreatedDate";
            break;
          case "procedureType":  
            sortFieldName = "RecordType.Name";
            break;  
          default:
            sortFieldName = "Name";
            break;
        }
  
        var searchPattern = {
          sortBy: sortFieldName,
          sortDirection: event.getParam("sortDirection")
        };
        component.set("v.sortBy", searchPattern.sortBy);
  
        component.set("v.sortDirection", searchPattern.sortDirection);
        component.set(
          "v.isDescending",
          searchPattern.sortDirection == "desc" ? true : false
        );
        component.set("v.offset", 0);
        helper.updateSearchParamsWrapper(component, {});
        helper.searchAction(component, false);
      }
      if (event.getParam("isLoadMore")) {
        helper.updateSearchParamsWrapper(component, {});
        helper.searchAction(component, true);
      }
    },

    handleDataTableSelect: function(component, event, helper) {
        
        helper.changeSelected(component, event, helper);
    },
     
    resetAction: function (component, event, helper) {
        helper.reset(component, helper);
        helper.updateSearchParamsWrapper(component, {});
        helper.searchAutomaticAction(component, event, helper);       
    },

    searchAction: function(component, event, helper) {
        helper.initData(component, helper);
        helper.updateSearchParamsWrapper(component, {});
        helper.searchAutomaticAction(component, event, helper); 
    }
})