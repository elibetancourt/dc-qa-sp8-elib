({
    fireComponentEvent: function(component, action) {
      var myEvent = component.getEvent("searchEvent");
      myEvent.setParams({
        sortBy: component.get("v.sortBy"),
        sortDirection: component.get("v.sortDirection"),
        isSort: action == "isSort" ? true : false,
        isLoadMore: action == "isLoadMore" ? true : false
      });
  
      myEvent.fire();
    },
    
    fireSelectedEvent: function(component, selectedRows) {
      var myEvent = component.getEvent("selectEvent");
      let paramRows = Object.assign({}, selectedRows); ;
      myEvent.setParams({
        rows: paramRows
      });
  
      myEvent.fire();
    }

  });