({
    init: function(component, event, helper) {
      //alert(component.get("v.noRecords"));
      if (!component.get("v.noRecords"))
        component.find("dataList").set("v.isLoading", false);
        //alert(JSON.stringify(component.get("v.data")));
        //alert(JSON.stringify(component.get("v.columns")));
        
    },
    
    updateLoadMoreStatus: function(component, event, helper) {
      component.find("dataList").set("v.isLoading", false);
    },

    updateColumnSorting: function(component, event, helper) {
      var fieldName = event.getParam("fieldName");
      var sortDirection = event.getParam("sortDirection");
  
      component.set("v.sortBy", fieldName);
      component.set("v.sortDirection", sortDirection);
  
      helper.fireComponentEvent(component, "isSort");
    },

    loadMore: function(component, event, helper) {
      component.find("dataList").set("v.isLoading", true);
      helper.fireComponentEvent(component, "isLoadMore");
    },
    
    updateSelected: function (component, event, helper) {
          
          var selectedRows = event.getParam('selectedRows');
          
          helper.fireSelectedEvent(component, selectedRows);
    }
  });