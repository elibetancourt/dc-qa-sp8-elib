({
    reset: function(component, helper){
            component.set("v.loading", true);
            component.set("v.showDataTable", false);
            component.set("v.canApplyPayment", true);
            var id = component.get("v.id");
            if (! id){
                var myPageRef = component.get("v.pageReference");
                //alert(JSON.stringify(myPageRef));
                var id = myPageRef.state.c__id;
                component.set("v.id", id);
            }
            helper.initColumns(component, helper);
            helper.getBatchInfo(component, helper); 
            component.set("v.loading", false);
    },
    
    runServerAction : function(cmp, helper, method, params) {
        var self = this;
        return new Promise(function(resolve, reject) {
            var action = cmp.get('c.' + method);
            if (params) {
                action.setParams(params);
            }
			action.setCallback(self, function(response) {
				var state = response.getState();
                if (state == 'SUCCESS')
					resolve.call(helper, response.getReturnValue());
				else if (state == 'ERROR') {
					var errors = response.getError();
					reject.call(helper, errors);
				}
			});
			$A.enqueueAction(action);
		});
    },
    
    getBatchInfo: function(component, helper){
        var recordId = component.get("v.id");
        //alert(recordId);
        helper
        .runServerAction(component, helper, "initData", {batchId: recordId})
        .then(function(result) {
      
              //alert(JSON.stringify(result));   
              if (!result) {
                var serverErrorMessage = component.get("v.serverErrorMessage");
                helper.showToast(component, "Error", serverErrorMessage, "error");
              }
     
              component.set("v.loading", false);
              component.set("v.showConfirmation", false);
              var appplyPaymentOFF = !(result.is_Closed && result.is_Recived && result.have_Payment);
              //alert('applyPaymentOFF'+appplyPaymentOFF);
              //alert(result.is_Closed && result.is_Recived && result.have_Payment);
              component.set("v.applyPaymentOFF", appplyPaymentOFF);
              component.set("v.pendingAmount", result.amount_Pending_to_Apply);
              component.set("v.cantPendingProcedures", result.procedure_count_to_Apply);
              component.set("v.procedureList", result.batch_items);  
              component.set("v.emptyList", result.batch_items.length == 0);  
              component.set("v.showDataTable", true);
            });
    },
    
    initColumns: function(component, helper){
    	component.set("v.columnFields", [              
            {
              type: "button",  
              typeAttributes: {
                    label: "Cancelar Pago",
                    name:  "Cancelar Pago",
                    title: "Cancelar Pago",
                    disabled: { fieldName: "procedure_Applied_Payment" },
                    value: "Cancelar Pago"
              }
            },
            {
                  label: "Trámite",
                  fieldName: "procedureNameLink",
                  type: "url",
                  typeAttributes: {
                        label: { fieldName: "procedure_Name" },
                        target: "_self"
                  }      
            },
            {
              label: "Cliente",
              fieldName: "procedure_Contact",
              type: "text"      
            },
              {
              label: "Tipo de Trámite",
              fieldName: "procedure_type",
              type: "text"        
            },
            {
              label: "Fecha de Creación",
              fieldName: "procedure_date",
              type: "date"
            },
            {
              label: "Pago al Consulado",
              fieldName: "procedure_consulate_payment",
              type: "currency",
            },
            {
              label: "Comisión de Agencia",
              fieldName: "procedure_agency_comission",
              type: "currency"
            },
            {
              label: "Pago al Mayorista",
              fieldName: "procedure_distributor_payment",
              type: "currency"
            }           
          ]);
	},
    
    showToast: function(component, title, message, type) {
        
        var toastEvent = $A.get("e.force:showToast");
        //alert(toastEvent);
        if (toastEvent){
            toastEvent.setParams({
                        "title": title,
                        "message": message,
                        "type": type,
                        "duration": 1500,
	                });
    		toastEvent.fire();
        }
        else{
             	var cmp = component.find('notifLib');
                component.find('notifLib').showToast({
                    "variant": type,
                    "header":  title,
                    "message": message
                });
        }
    },
    
    verifySelectedRows: function(component, event, helper){
        var rows = event.getParam('rows');
        var newRows = Object.values(rows);
        var newRowsOK = [];
        var procedureName = '';
        var iPos = -1;
        for (var i =0; i < newRows.length; i++){
            var row = newRows[i];
            
            //alert(JSON.stringify(row));
            //alert(row['procedure_Applied_Payment']);
            if (!row.procedure_Applied_Payment) {
                procedureName = row.procedure_Name;
                iPos = i;
            }
            else{
                //alert(row.procedure_id);
                newRowsOK.push(row.procedure_id);
            }
        }
        //alert(newRowsOK);
        /*Object.values(rows).forEach(row => {
            if (row.procedure_Applied_Payment) {
               procedureName = row.procedure_Name;
               iPos = i;
            }
            else{
              newRows.push(row);
            }
            i++;
        });*/
        

        if (procedureName != '' && iPos >= 0) {
            var message = 'El artículo de lote '+ procedureName + ' ya fue pagado y no puede ser seleccionado.';
            var title = 'Artículo de Lote Pagado';
            helper.showToast(component, message, title, 'info');   
        }
        component.set("v.selectedRows", newRowsOK);
        //alert(JSON.stringify(rows));
    },
    
    changeSelected: function(component, event, helper){
         /* update amount selected and proceduresSelected */
        
        var rows = event.getParam('rows');
        //alert(JSON.stringify(rows));
        var selectedAmount = 0;
        var cantSelectedProcedures = 0;
        var newRows = Object.values(rows);
        for (var i=0; i < newRows.length; i++){
            var row = newRows[i];
            if (row.procedure_Applied_Payment) {
              selectedAmount += row.procedure_distributor_payment;
              cantSelectedProcedures++;
            }
        }
        /*Object.values(rows).forEach(row => {
          	selectedAmount += row.procedure_distributor_payment;
            //alert(row.procedure_distributor_payment);
            cantSelectedProcedures++;
        });*/
                
        var pendingAmount = component.get("v.pendingAmount");
        var canApplyPayment = !(selectedAmount > 0 && pendingAmount >= selectedAmount);
        /*alert('selectedAmount'+selectedAmount);
        alert('cantSelectedProcedures'+cantSelectedProcedures);
        alert('canApplyPayment' + canApplyPayment);*/
        component.set("v.selectedAmount", selectedAmount); 
        component.set("v.cantSelectedProcedures", cantSelectedProcedures); 
        component.set("v.canApplyPayment", canApplyPayment); 
        component.set("v.selectedProc", newRows); 
        
    },
    
    applyPaymentConfirm: function(component, event, helper){
           
            var itemsToApply = [];
            var rows = component.get("v.selectedProc");
            //alert('estoy en confirmacion'); 
            //alert(JSON.stringify(rows));
            /*Object.values(rows).forEach(row => {   
                if (row.procedure_Applied_Payment) {            
                  itemsToApply.push(row.batch_item_id);
                  alert(row.batch_item_id);
                }
            });*/
            for (var i=0; i < rows.length; i++){
                var row = rows[i];
                if (row.procedure_Applied_Payment) {
                  itemsToApply.push(row.batch_item_id);
                    //alert(row.batch_item_id);
                }
            }    
            var recordId = component.get("v.id");
            var itemsToApplyJSON = JSON.stringify(itemsToApply);
            //alert(itemsToApplyJSON);   
            var item = {batchId: recordId, itemsToApply: itemsToApply};
            var itemsPattern = {itemsPattern: JSON.stringify(item)};
            //alert(JSON.stringify(itemsPattern)) ;                          
            helper
            .runServerAction(component, helper, "applyPayments", itemsPattern)
            .then(function(result) {
                 //alert(result);
               
                 if (result === 'OK') {
                     //alert('estoy en el if de toast message');
                     var message = component.get("v.successmessage");
                     var title = component.get("v.title");
                     //alert(message);
                     helper.showToast(component, title, message, "info");
                    //hacer un reset de la pagina 
                     helper.reset(component, helper);                        
                 }
                 else{
                     var message = component.get("v.serverErrorMessage");
                     var title = component.get("v.title");
                     helper.showToast(component, title, message, "error");                  
                 }                  
            });   
    },
           
    removeAppliedPayment: function(component, event, helper){
            var itemsToApply = [];
            var idToRemovePayment = component.get("v.idToRemovePayment"); 
            //alert(JSON.stringify(rows));
            itemsToApply.push(idToRemovePayment);
            
            var recordId = component.get("v.id");
            var itemsToApplyJSON = JSON.stringify(itemsToApply);
            //alert(itemsToApplyJSON);   
            var item = {batchId: recordId, itemsToApply: itemsToApply};
            var itemsPattern = {itemsPattern: JSON.stringify(item)};
            //alert(JSON.stringify(itemsPattern)) ;                          
            helper
            .runServerAction(component, helper, "removeAppliedPayment", itemsPattern)
            .then(function(result) {
                 //alert(result);
               
                 if (result === 'OK') {
                     //alert('estoy en el if de toast message');
                     var message = component.get("v.successmessage1");
                     var title = component.get("v.title");
                     //alert(message);
                     helper.showToast(component, title, message, "info");
                    //hacer un reset de la pagina 
                     helper.reset(component, helper);                        
                 }
                 else{
                     var message = component.get("v.serverErrorMessage");
                     var title = component.get("v.title");
                     helper.showToast(component, title, message, "error");                  
                 }                  
            }); 
    }
})