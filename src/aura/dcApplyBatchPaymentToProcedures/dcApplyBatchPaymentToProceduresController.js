({
	init: function(component, event, helper) {
       // alert('estoy en inicio');     
       helper.reset(component, helper);
    },
    
    handlePageChange: function(component, event, helper) {
        //alert('estoy en page change');
        component.set("v.id", "");
        helper.reset(component, helper);
    },
 
    handleDataTableSelect: function(component, event, helper) {
        
        helper.verifySelectedRows(component, event, helper);
        helper.changeSelected(component, event, helper);
    },
    
    handleConfirmationEvent : function(component, event, helper) {
        //alert('entre al evento confirmacion');
        var action = event.getParam("action");
        //alert('@@@ ==> ' + action);
        var showModal = false;
        component.set("v.showConfirmation", showModal);
        component.set("v.showConfirmation1", showModal);
        
        var confirmationType = component.get("v.confirmationType");
        
        //if action = 2 user selecciono OK, ejecutar Pagar Tramite
        if (action == 2){
            
            if (confirmationType == 1){ //confirmacion de pago
                helper.applyPaymentConfirm(component, event, helper);
            } else if (confirmationType == 2){ //confirmacion de cancelar pago
                helper.removeAppliedPayment(component, event, helper);
            }             
        }  
        confirmationType = 0;
        component.set("v.confirmationType", confirmationType);
    },
    
    applyPayment: function(component, event, helper) {
        //  activar el modal
        var confirmationType = 1;
        var showModal = true;
    
        component.set("v.confirmationType", confirmationType);
        component.set("v.showConfirmation", showModal);
       
    },
        
    handleDataTableAction: function(component, event, helper) {
        var row = event.getParam('row');
        //guardar en el componente los valores de fila seleccionada
        component.set("v.idToRemovePayment", row.batch_item_id);
        //alert(JSON.stringify(row));
        // setaer el mensaje y activar el modal
        
        var message = component.get("v.confirmationMsg1");
        message = message.replace("[0]", row.procedure_Name);
        component.set("v.confirmationMsg1", message);
        
        var showModal = true;
        var confirmationType = 2;
        
        component.set("v.confirmationType", confirmationType);
        component.set("v.showConfirmation1", showModal);
    }
})