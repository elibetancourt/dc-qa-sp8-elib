<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Prorroga Regular</label>
    <protected>false</protected>
    <values>
        <field>Procedure_Type__c</field>
        <value xsi:type="xsd:string">Prorroga</value>
    </values>
    <values>
        <field>Product_Key__c</field>
        <value xsi:type="xsd:string">price_1HeiFBAC0FDJF1JzYgaDKtoI</value>
    </values>
    <values>
        <field>Product_Price__c</field>
        <value xsi:type="xsd:double">270.0</value>
    </values>
    <values>
        <field>Subtype__c</field>
        <value xsi:type="xsd:string">Regular</value>
    </values>
</CustomMetadata>
