<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Municipio de Referencia</label>
    <protected>false</protected>
    <values>
        <field>ProcedureFieldApiName__c</field>
        <value xsi:type="xsd:string">References_Municipality__c</value>
    </values>
    <values>
        <field>ProcedureRecordType__c</field>
        <value xsi:type="xsd:string">Prorroga</value>
    </values>
</CustomMetadata>
