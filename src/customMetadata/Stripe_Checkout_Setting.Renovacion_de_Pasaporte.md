<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Renovacion de Pasaporte</label>
    <protected>false</protected>
    <values>
        <field>Procedure_Type__c</field>
        <value xsi:type="xsd:string">Renovacion de Pasaporte</value>
    </values>
    <values>
        <field>Product_Key__c</field>
        <value xsi:type="xsd:string">price_1Heie8AC0FDJF1JzOVjJPEok</value>
    </values>
    <values>
        <field>Product_Price__c</field>
        <value xsi:type="xsd:double">470.0</value>
    </values>
    <values>
        <field>Subtype__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
