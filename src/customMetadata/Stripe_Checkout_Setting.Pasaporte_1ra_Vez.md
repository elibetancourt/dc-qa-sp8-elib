<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Pasaporte 1ra Vez</label>
    <protected>false</protected>
    <values>
        <field>Procedure_Type__c</field>
        <value xsi:type="xsd:string">Pasaporte 1ra Vez</value>
    </values>
    <values>
        <field>Product_Key__c</field>
        <value xsi:type="xsd:string">price_1HZgMkAC0FDJF1JzgfZvL2tm</value>
    </values>
    <values>
        <field>Product_Price__c</field>
        <value xsi:type="xsd:double">455.0</value>
    </values>
    <values>
        <field>Subtype__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
